<div class="footerWrap">
  <div class="container">
    <div class="col-md-3">
      <img src="<?php echo base_url('public/images/logo.png');?>" alt="USA jobs website" />
      <br><br>
      
      <!--Social-->
        
        <div class="social">
        <a href="http://www.twitter.com" target="_blank"><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
        <a href="http://www.plus.google.com" target="_blank"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a>
        <a href="http://www.facebook.com" target="_blank"> <i class="fa fa-facebook-square" aria-hidden="true"></i></a>
        <a href="https://www.pinterest.com" target="_blank"><i class="fa fa-pinterest-square" aria-hidden="true"></i></a>
        <a href="https://www.youtube.com" target="_blank"><i class="fa fa-youtube-square" aria-hidden="true"></i></a>
        <a href="https://www.linkedin.com" target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
        </div>
        
      
    </div>
    <div class="col-md-2">
      <h5>Quick Links</h5>
      <ul class="quicklinks">
        <li><a href="<?php echo base_url('about-us.html');?>" title="About Us">About Us</a></li>
        <li><a href="<?php echo base_url('how-to-get-job.html');?>" title="How to get Job">How to get Job</a></li>
        <li><a href="<?php echo base_url('search-jobs');?>" title="New Job Openings">New Job Openings</a></li>
        <li><a href="<?php echo base_url('search-resume');?>" title="New Job Openings">Resume Search</a></li>
        <li><a href="<?php echo base_url('contact-us');?>" title="Contact Us">Contact Us</a></li>
      </ul>
    </div>
    
    <div class="col-md-3">
      <h5>Popular Industries</h5>
      <ul class="quicklinks">
        <?php
			$res_inds = $this->industries_model->get_top_industries();
			if($res_inds):
				foreach($res_inds as $row_inds):
		?>
        <li><a href="<?php echo base_url('industry/'.$row_inds->slug);?>" title="<?php echo $row_inds->industry_name;?> Jobs"><?php echo $row_inds->industry_name;?> Jobs</a></li>
        <?php 

		  		endforeach;

		  	endif;

		  ?>
      </ul>
    </div>
    <!-- <div class="col-md-4">
      <h5>Popular Cities</h5>
      <ul class="citiesList">
        <li><a href="<?php echo base_url('search/jharkhand');?>" title="Jobs in Jharkhand">Jobs in Jharkhand</a></li>
        <li><a href="<?php echo base_url('search/dhanbad');?>" title="Jobs in Dhanbad">Jobs in Dhanbad</a></li>
        <li><a href="<?php echo base_url('search/ranchi');?>" title="Jobs in Ranchi">Jobs in Ranchi</a></li>
        <li><a href="<?php echo base_url('search/kolkata');?>" title="Jobs in Kolkata">Jobs in Kolkata</a></li>
        <li><a href="<?php echo base_url('search/new-delhi');?>" title="Jobs in New Delhi">Jobs in New Delhi</a></li>
        <li><a href="<?php echo base_url('search/mumbai');?>" title="Jobs in Mumbai">Jobs in Mumbai</a></li>
        <li><a href="<?php echo base_url('search/ahmedabad');?>" title="Jobs in Ahmedabad">Jobs in Ahmedabad</a></li>
        <li><a href="<?php echo base_url('search/bangalore');?>" title="Jobs in Bangalore">Jobs in Bangalore</a></li>
      </ul>
      <div class="clear"></div>
    </div> -->
    <!--Google map-->
    <div class="col-md-4">
      <div id="map-container-google-8" class="z-depth-1-half map-container-5" style="height: 300px">
          <<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14600.170360958173!2d86.4276645!3d23.8170845!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x6bc55b07ef8baaba!2sCSIR%20-%20Central%20Institute%20of%20Mining%20and%20Fuel%20Research!5e0!3m2!1sen!2sin!4v1598850397700!5m2!1sen!2sin" width="350" height="220" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
        </div>
    </div>
    <div class="clear"></div>
    <div class="copyright">
    <!-- <a href="<?php echo base_url('interview.html');?>" title="Preparing for Interview">Preparing for Interview</a> | 
    <a href="<?php echo base_url('cv-writing.html');?>" title="CV Writing">CV Writing</a> | |
    <a href="<?php echo base_url('privacy-policy.html');?>" title="Policy">Policy</a> -->
    
    
    
      <div class="bttxt">Copyright <?php echo date('Y');?>. Job Portal. Powered By <a href="http:www.cimfr.nic.in" target="_blank">CSIR-CIMFR, Dhanbad</a></div>
    </div>
  </div>
</div>
</div>
<?php echo $ads_row->google_analytics;?> 