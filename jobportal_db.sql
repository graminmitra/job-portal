-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 07, 2020 at 02:00 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jobportal_db`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `count_active_opened_jobs` ()  BEGIN
	#Routine body goes here...
	SELECT COUNT(ID) as total
	FROM `pp_post_jobs` AS pj
	WHERE pj.sts='active' AND CURRENT_DATE < pj.last_date;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `count_active_opened_jobs_by_company_id` (IN `comp_id` INT(11))  BEGIN
	#Routine body goes here...
	SELECT COUNT(ID) as total
	FROM `pp_post_jobs` AS pj
	WHERE pj.company_ID=comp_id AND pj.sts='active';
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `count_active_records_by_city_front_end` (IN `city` VARCHAR(40))  BEGIN
	#Routine body goes here...
	SELECT COUNT(pj.ID) AS total
	FROM `pp_post_jobs` AS pj
	INNER JOIN pp_companies AS pc ON pj.company_ID=pc.ID
	WHERE pj.city=city AND pj.sts='active' AND pc.sts = 'active';
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `count_active_records_by_industry_front_end` (IN `industry_id` INT(11))  BEGIN
	SELECT COUNT(pj.ID) AS total
	FROM `pp_post_jobs` AS pj
	INNER JOIN pp_companies AS pc ON pj.company_ID=pc.ID
	INNER JOIN pp_job_industries AS ji ON pj.industry_ID=ji.ID
	WHERE pj.industry_ID=industry_id AND pj.sts='active' AND pc.sts = 'active';
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `count_all_posted_jobs_by_company_id_frontend` (IN `comp_id` INT(11))  BEGIN
	#Routine body goes here...
	SELECT COUNT(pj.ID) AS total
	FROM `pp_post_jobs` AS pj
	INNER JOIN pp_companies AS pc ON pj.company_ID=pc.ID
	WHERE pj.company_ID=comp_id AND pj.sts='active' AND pc.sts = 'active';
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `count_applied_jobs_by_employer_id` (IN `employer_id` INT(11))  BEGIN
	SELECT COUNT(pp_seeker_applied_for_job.ID) AS total
	FROM `pp_seeker_applied_for_job`
	INNER JOIN pp_post_jobs ON pp_post_jobs.ID=pp_seeker_applied_for_job.job_ID
	INNER JOIN pp_job_seekers ON pp_job_seekers.ID=pp_seeker_applied_for_job.seeker_ID
	WHERE pp_post_jobs.employer_ID=employer_id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `count_applied_jobs_by_jobseeker_id` (IN `jobseeker_id` INT(11))  BEGIN
	SELECT COUNT(pp_seeker_applied_for_job.ID) AS total
	FROM `pp_seeker_applied_for_job`
	WHERE pp_seeker_applied_for_job.seeker_ID=jobseeker_id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `count_ft_job_search_filter_3` (IN `param_city` VARCHAR(255), `param_company_slug` VARCHAR(255), `param_title` VARCHAR(255))  BEGIN
	SELECT COUNT(pj.ID) as total
	FROM pp_post_jobs pj
	INNER JOIN pp_companies pc ON pc.ID = pj.company_ID 
	WHERE (pj.job_title like CONCAT("%",param,"%") OR pj.job_description like CONCAT("%",param,"%") OR pj.required_skills like CONCAT("%",param,"%"))
AND pc.company_slug = param_company_slug AND pj.city = param_city AND pj.sts = 'active' AND pc.sts = 'active';
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `count_ft_search_job` (IN `param` VARCHAR(255), `param2` VARCHAR(255))  BEGIN
	SELECT COUNT(pc.ID) as total
	FROM `pp_post_jobs` pj 
	INNER JOIN pp_companies AS pc ON pj.company_ID=pc.ID
	WHERE pj.sts = 'active' AND pc.sts = 'active'
AND (pj.job_title like CONCAT("%",param,"%") OR pj.job_description like CONCAT("%",param,"%") OR pj.required_skills like CONCAT("%",param,"%"))
AND pj.city like CONCAT("%",param2,"%");
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `count_ft_search_resume` (IN `param` VARCHAR(255))  BEGIN
	SELECT COUNT(DISTINCT ss.ID) as total
	FROM `pp_job_seekers` js 
	INNER JOIN pp_seeker_skills AS ss ON js.ID=ss.seeker_ID
	WHERE js.sts = 'active' 
AND ss.skill_name like CONCAT('%',param,'%');
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `count_search_posted_jobs` (IN `where_condition` VARCHAR(255))  BEGIN
	#Routine body goes here...
SET @query = "SELECT COUNT(pj.ID) as total
	FROM `pp_post_jobs` pj 
	LEFT JOIN pp_companies AS pc ON pj.company_ID=pc.ID 
	WHERE
";

SET @where_clause = CONCAT(where_condition);
SET @query = CONCAT(@query, @where_clause);

PREPARE stmt FROM @query;
EXECUTE stmt;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ft_job_search_filter_3` (IN `param_city` VARCHAR(255), `param_company_slug` VARCHAR(255), `param_title` VARCHAR(255), `from_limit` INT(5), `to_limit` INT(5))  BEGIN
	SELECT pj.ID, pj.job_title, pj.job_slug, pj.employer_ID, pj.company_ID, pj.job_description, pj.city, pj.dated, pj.last_date, pj.is_featured, pj.sts, pc.company_name, pc.company_logo, pc.company_slug, MATCH(pj.job_title, pj.job_description) AGAINST( param_title ) AS score
	FROM pp_post_jobs pj
	INNER JOIN pp_companies pc ON pc.ID = pj.company_ID 
	WHERE (pj.job_title like CONCAT("%",param_title,"%") OR pj.job_description like CONCAT("%",param_title,"%") OR pj.required_skills like CONCAT("%",param_title,"%")) 
AND pc.company_slug = param_company_slug AND pj.city = param_city AND pj.sts = 'active' AND pc.sts = 'active'

ORDER BY score DESC
	LIMIT from_limit, to_limit;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ft_search_job` (IN `param` VARCHAR(255), IN `param2` VARCHAR(255), IN `from_limit` INT(5), IN `to_limit` INT(5))  BEGIN

	SELECT pj.ID, pj.job_title, pj.job_slug, pj.employer_ID, pj.company_ID, pj.job_description, pj.city, pj.dated, pj.last_date, pj.is_featured, pj.sts, pc.company_name, pc.company_logo, pc.company_slug, MATCH(pj.job_title, pj.job_description) AGAINST(param) AS score
	FROM `pp_post_jobs` pj 
	INNER JOIN pp_companies AS pc ON pj.company_ID=pc.ID
	WHERE pj.sts = 'active' AND pc.sts = 'active' 
	AND (
			pj.job_title like CONCAT("%",param,"%") 
			OR pj.job_description like CONCAT("%",param,"%") 
			OR pj.required_skills like CONCAT("%",param,"%") 
			OR pj.pay like CONCAT("%",REPLACE(param,' ','-'),"%")
			OR pj.city like CONCAT("%",param,"%")
            OR pj.category like CONCAT("%",param,"%")
  
		)
		AND (pj.city) like CONCAT("%",param2,"%")
ORDER BY pj.ID DESC
	LIMIT from_limit, to_limit;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ft_search_jobs_group_by_category` (IN `param` VARCHAR(255))  BEGIN
	SELECT pc.category, COUNT(pc.category) as score
	FROM pp_category as pc,pp_post_jobs as pj 
	WHERE 
    	pc.category like CONCAT("%",param,"%")
	AND 
    	pj.ID=pc.job_id AND pj.sts="active"
	GROUP BY pc.category
	ORDER BY score DESC
	LIMIT 0,5;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ft_search_jobs_group_by_city` (IN `param` VARCHAR(255))  NO SQL
BEGIN
	SELECT pp.city, COUNT(pp.city) as score
	FROM pp_post_jobs as pp
    
	WHERE pp.sts="active"
    AND
    (
    	pp.city like CONCAT("%",param,"%")
       
	)
	GROUP BY pp.city
	ORDER BY score DESC
	LIMIT 0,5;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ft_search_jobs_group_by_company` (IN `param` VARCHAR(255))  BEGIN
	SELECT  pc.company_name,pc.company_slug, COUNT(pc.company_name) as score
	FROM `pp_post_jobs` pj 
	INNER JOIN pp_companies AS pc ON pj.company_ID=pc.ID
	WHERE pj.sts = 'active' AND pc.sts = 'active' 
AND (
			pj.job_title like CONCAT("%",param,"%") 
			OR pj.job_description like CONCAT("%",param,"%") 
			OR pj.required_skills like CONCAT("%",param,"%") 
			OR pj.pay like CONCAT("%",REPLACE(param,' ','-'),"%")
			OR pj.city like CONCAT("%",param,"%")
    OR pj.category like CONCAT("%",param,"%")
    	
		)
	GROUP BY pc.company_name
	ORDER BY score DESC
	LIMIT 0,5;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ft_search_jobs_group_by_salary_range` (IN `param` VARCHAR(255))  BEGIN
	SELECT pay, COUNT(pay) as score
	FROM `pp_post_jobs` pj 
	WHERE pj.sts = 'active' 
AND (
			pj.job_title like CONCAT("%",param,"%") 
			OR pj.job_description like CONCAT("%",param,"%") 
			OR pj.required_skills like CONCAT("%",param,"%") 
			OR pj.pay like CONCAT("%",REPLACE(param,' ','-'),"%")
			OR pj.city like CONCAT("%",param,"%")
    OR pj.category like CONCAT("%",param,"%")
		)
	GROUP BY pay
	ORDER BY score DESC
	LIMIT 0,5;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ft_search_jobs_group_by_title` (IN `param` VARCHAR(255))  BEGIN
	SELECT job_title, COUNT(job_title) as score
	FROM `pp_post_jobs` pj 
	WHERE pj.sts = 'active' 
AND (
			pj.job_title like CONCAT("%",param,"%") 
			OR pj.job_description like CONCAT("%",param,"%") 
			OR pj.required_skills like CONCAT("%",param,"%") 
			OR pj.pay like CONCAT("%",REPLACE(param,' ','-'),"%")
			OR pj.city like CONCAT("%",param,"%")
    	    OR pj.category like CONCAT("%",param,"%")
    
		)

	GROUP BY job_title
	ORDER BY score DESC
	LIMIT 0,5;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ft_search_resume` (IN `param` VARCHAR(255), `from_limit` INT(5), `to_limit` INT(5))  BEGIN
  SELECT js.ID, js.first_name, js.gender, js.dob, js.city, js.photo
	FROM pp_job_seekers AS js
	INNER JOIN pp_seeker_skills AS ss ON js.ID=ss.seeker_ID
	WHERE js.sts = 'active' AND ss.skill_name like CONCAT("%",param,"%")
  GROUP BY ss.seeker_ID
	ORDER BY js.ID DESC
	LIMIT from_limit, to_limit;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_active_deactive_posted_job_by_company_id` (IN `comp_id` INT(11), `from_limit` INT(4), `to_limit` INT(4))  BEGIN
	#Routine body goes here...
	SELECT pj.ID, pj.job_title, pj.job_slug, pj.job_description, pj.employer_ID, pj.last_date, pj.dated, pj.city, pj.is_featured, pj.sts, pc.company_name, pc.company_logo
	FROM `pp_post_jobs` AS pj
	INNER JOIN pp_companies AS pc ON pj.company_ID=pc.ID
	WHERE pj.company_ID=comp_id AND pj.sts IN ('active', 'inactive', 'pending') AND pc.sts = 'active'
	ORDER BY ID DESC
	LIMIT from_limit, to_limit;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_active_featured_job` (IN `from_limit` INT(5), `to_limit` INT(5))  BEGIN
	SELECT pj.ID, pj.job_title, pj.job_slug, pj.employer_ID, pj.company_ID, pj.city, pj.dated, pj.last_date, pj.is_featured, pj.sts, pc.company_name, pc.company_logo, pc.company_slug 
	FROM `pp_post_jobs` pj 
	LEFT JOIN pp_companies AS pc ON pj.company_ID=pc.ID 
	WHERE pj.is_featured='yes' AND pj.sts='active' AND pc.sts = 'active'
	ORDER BY ID DESC 
	LIMIT from_limit, to_limit;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_active_posted_job_by_company_id` (IN `comp_id` INT(11), `from_limit` INT(4), `to_limit` INT(4))  BEGIN
	#Routine body goes here...
	SELECT pj.ID, pj.job_title, pj.job_slug, pj.job_description, pj.employer_ID, pj.last_date, pj.dated, pj.city, pj.is_featured, pj.sts, pc.company_name, pc.company_logo
	FROM `pp_post_jobs` AS pj
	INNER JOIN pp_companies AS pc ON pj.company_ID=pc.ID
	WHERE pj.company_ID=comp_id AND pj.sts='active' AND pc.sts = 'active'
	ORDER BY ID DESC
	LIMIT from_limit, to_limit;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_active_posted_job_by_id` (IN `job_id` INT(11))  BEGIN
	SELECT pp_post_jobs.*, pc.ID AS CID, emp.first_name, emp.email AS employer_email, pp_job_industries.industry_name, pc.company_name, pc.company_email, pc.company_ceo, pc.company_description, pc.company_logo, pc.company_phone, pc.company_website, pc.company_fax,pc.no_of_offices, pc.no_of_employees, pc.established_in, pc.industry_ID AS cat_ID, pc.company_location, pc.company_slug
,emp.city as emp_city, emp.country as emp_country	
FROM `pp_post_jobs` 
	INNER JOIN pp_companies AS pc ON pp_post_jobs.company_ID=pc.ID
	INNER JOIN pp_employers AS emp ON pc.ID=emp.company_ID
	INNER JOIN pp_job_industries ON pp_post_jobs.industry_ID=pp_job_industries.ID
	WHERE pp_post_jobs.ID=job_id AND pp_post_jobs.sts='active' AND pc.sts = 'active';
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_all_active_employers` (IN `from_limit` INT(5), `to_limit` INT(5))  BEGIN
	SELECT pc.ID AS CID, pc.company_name, pc.company_logo, pc.company_slug
	FROM `pp_employers` emp 
	INNER JOIN pp_companies AS pc ON emp.company_ID=pc.ID
	WHERE emp.sts = 'active'
	ORDER BY emp.ID DESC 
	LIMIT from_limit, to_limit;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_all_active_top_employers` (IN `from_limit` INT(5), `to_limit` INT(5))  BEGIN
	SELECT pc.ID AS CID, pc.company_name, pc.company_logo, pc.company_slug
	FROM `pp_employers` emp 
	INNER JOIN pp_companies AS pc ON emp.company_ID=pc.ID
	WHERE emp.sts = 'active' AND emp.top_employer = 'yes'
	ORDER BY emp.ID DESC 
	LIMIT from_limit, to_limit;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_all_opened_jobs` (IN `from_limit` INT(5), `to_limit` INT(5))  BEGIN
	SELECT pj.ID, pj.job_title, pj.job_slug, pj.employer_ID, pj.company_ID, pj.job_description, pj.city, pj.dated, pj.last_date, pj.is_featured, pj.sts, pc.company_name, pc.company_logo, pc.company_slug, ji.industry_name 
	FROM `pp_post_jobs` pj 
	INNER JOIN pp_companies AS pc ON pj.company_ID=pc.ID
	INNER JOIN pp_job_industries AS ji ON pj.industry_ID=ji.ID
	WHERE pj.sts = 'active' AND pc.sts='active'
	ORDER BY pj.ID DESC 
	LIMIT from_limit, to_limit;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_all_posted_jobs` (IN `from_limit` INT(5), `to_limit` INT(5))  BEGIN
	#Routine body goes here...
	SELECT pj.ID, pj.job_title, pj.job_slug, pj.employer_ID, pj.company_ID, pj.job_description, pj.city, pj.dated, pj.last_date, pj.is_featured, pj.sts, pc.company_name, pc.company_logo, pc.company_slug, pj.ip_address 
	FROM `pp_post_jobs` pj 
	INNER JOIN pp_companies AS pc ON pj.company_ID=pc.ID 
	ORDER BY ID DESC 
	LIMIT from_limit, to_limit;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_all_posted_jobs_by_company_id_frontend` (IN `comp_id` INT(11), `from_limit` INT(4), `to_limit` INT(4))  BEGIN
	#Routine body goes here...
	SELECT pj.ID, pj.job_title, pj.job_slug, pj.job_description, pj.employer_ID, pj.last_date, pj.dated, pj.city, pj.is_featured, pj.sts, pc.company_name, pc.company_logo
	FROM `pp_post_jobs` AS pj
	INNER JOIN pp_companies AS pc ON pj.company_ID=pc.ID
	WHERE pj.company_ID=comp_id AND pj.sts='active' AND pc.sts = 'active'
	ORDER BY ID DESC
	LIMIT from_limit, to_limit;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_all_posted_jobs_by_status` (IN `job_status` VARCHAR(10), `from_limit` INT(5), `to_limit` INT(5))  BEGIN
	#Routine body goes here...
	SELECT pj.ID, pj.job_title, pj.job_slug, pj.employer_ID, pj.company_ID, pj.job_description, pj.city, pj.dated, pj.last_date, pj.is_featured, pj.sts, pc.company_name, pc.company_logo, pc.company_slug 
	FROM `pp_post_jobs` pj 
	INNER JOIN pp_companies AS pc ON pj.company_ID=pc.ID
	WHERE pj.sts = job_status
	ORDER BY ID DESC 
	LIMIT from_limit, to_limit;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_applied_jobs_by_employer_id` (IN `employer_id` INT(11), `from_limit` INT(5), `to_limit` INT(5))  BEGIN
	SELECT pp_seeker_applied_for_job.dated AS applied_date, pp_post_jobs.ID, pp_post_jobs.job_title, pp_job_seekers.ID AS job_seeker_ID, pp_post_jobs.job_slug, pp_job_seekers.first_name, pp_job_seekers.last_name, pp_job_seekers.slug
	FROM `pp_seeker_applied_for_job`
	INNER JOIN pp_post_jobs ON pp_post_jobs.ID=pp_seeker_applied_for_job.job_ID
	INNER JOIN pp_job_seekers ON pp_job_seekers.ID=pp_seeker_applied_for_job.seeker_ID
	WHERE pp_post_jobs.employer_ID=employer_id 
	ORDER BY pp_seeker_applied_for_job.ID DESC 
	LIMIT from_limit, to_limit;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_applied_jobs_by_jobseeker_id` (IN `jobseeker_id` INT(11), `from_limit` INT(5), `to_limit` INT(5))  BEGIN
	SELECT pp_seeker_applied_for_job.ID as applied_id, pp_seeker_applied_for_job.dated AS applied_date, pp_post_jobs.ID, pp_post_jobs.job_title, pp_post_jobs.job_slug, pp_companies.company_name, pp_companies.company_slug, pp_companies.company_logo 
	FROM `pp_seeker_applied_for_job`
	INNER JOIN pp_post_jobs ON pp_post_jobs.ID=pp_seeker_applied_for_job.job_ID
	INNER JOIN pp_employers ON pp_employers.ID=pp_post_jobs.employer_ID
	INNER JOIN pp_companies ON pp_companies.ID=pp_employers.company_ID
	WHERE pp_seeker_applied_for_job.seeker_ID=jobseeker_id 
	ORDER BY pp_seeker_applied_for_job.ID DESC 
	LIMIT from_limit, to_limit;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_applied_jobs_by_seeker_id` (IN `applicant_id` INT(11), `from_limit` INT(5), `to_limit` INT(5))  BEGIN
	#Routine body goes here...
	SELECT aj.*, pp_post_jobs.ID AS posted_job_id, pp_post_jobs.employer_ID, pp_post_jobs.job_title, pp_post_jobs.job_slug, pp_post_jobs.city, pp_post_jobs.is_featured, pp_post_jobs.sts, pp_companies.company_name, pp_companies.company_logo, pp_job_seekers.first_name, pp_job_seekers.last_name, pp_job_seekers.photo
	FROM `pp_seeker_applied_for_job` aj
	INNER JOIN pp_job_seekers ON aj.seeker_ID=pp_job_seekers.ID
	INNER JOIN pp_post_jobs ON aj.job_ID=pp_post_jobs.ID
	INNER JOIN pp_companies ON pp_post_jobs.company_ID=pp_companies.ID
	WHERE aj.seeker_ID=applicant_id
	ORDER BY ID DESC
	LIMIT from_limit, to_limit;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_company_by_slug` (IN `slug` VARCHAR(70))  BEGIN
	SELECT emp.ID AS empID, pc.ID, emp.country, emp.city, pc.company_name, pc.company_description, pc.company_location, pc.company_website, pc.no_of_employees, pc.established_in, pc.company_logo, pc.company_slug
	FROM `pp_employers` AS emp 
	INNER JOIN pp_companies AS pc ON emp.company_ID=pc.ID
	WHERE pc.company_slug=slug AND emp.sts='active';
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_experience_by_jobseeker_id` (IN `jobseeker_id` INT(11))  BEGIN
	SELECT pp_seeker_experience.* 
	FROM `pp_seeker_experience`
	WHERE pp_seeker_experience.seeker_ID=jobseeker_id 
	ORDER BY pp_seeker_experience.start_date DESC;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_featured_job` (IN `from_limit` INT(5), `to_limit` INT(5))  BEGIN
	#Routine body goes here...
	SELECT pj.ID, pj.job_title, pj.job_slug, pj.employer_ID, pj.company_ID, pj.city, pj.dated, pj.last_date, pj.is_featured, pj.sts, pc.company_name, pc.company_logo, pc.company_slug 
	FROM `pp_post_jobs` pj 
	LEFT JOIN pp_companies AS pc ON pj.company_ID=pc.ID 
	WHERE pj.is_featured='yes'
	ORDER BY ID DESC 
	LIMIT from_limit, to_limit;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_latest_posted_job_by_employer_ID` (IN `emp_id` INT(11), `from_limit` INT(4), `to_limit` INT(4))  BEGIN
	#Routine body goes here...
	SELECT pp_post_jobs.ID, pp_post_jobs.job_title, pp_post_jobs.job_slug, pp_post_jobs.employer_ID, pp_post_jobs.last_date, pp_post_jobs.dated, pp_post_jobs.city, pp_post_jobs.is_featured, pp_post_jobs.sts, pp_job_industries.industry_name, pc.company_name, pc.company_logo
	FROM `pp_post_jobs` 
	INNER JOIN pp_companies AS pc ON pp_post_jobs.company_ID=pc.ID
	INNER JOIN pp_employers AS emp ON pp_post_jobs.employer_ID=emp.ID
	INNER JOIN pp_job_industries ON pp_post_jobs.industry_ID=pp_job_industries.ID
	WHERE pp_post_jobs.employer_ID=emp_id
	ORDER BY ID DESC
	LIMIT from_limit, to_limit;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_opened_jobs_home_page` (IN `from_limit` INT(5), `to_limit` INT(5))  BEGIN
set @prev := 0, @rownum := '';
SELECT ID, job_title, job_slug, employer_ID, company_ID, job_description, city, dated, last_date, is_featured, sts, company_name, company_logo, company_slug, industry_name 
FROM (
  SELECT ID, job_title, job_slug, employer_ID, company_ID, job_description, city, dated, last_date, is_featured, sts, company_name, company_logo, company_slug, industry_name, 
         IF( @prev <> company_ID, 
             @rownum := 1, 
             @rownum := @rownum+1 
         ) AS rank, 
         @prev := company_ID, 
         @rownum  
			FROM (
					SELECT pj.ID, pj.job_title, pj.job_slug, pj.employer_ID, pj.company_ID, pj.job_description, pj.city, pj.dated, pj.last_date, pj.is_featured, pj.sts, company_name, company_logo, company_slug, industry_name 
					FROM pp_post_jobs AS pj
					INNER JOIN pp_companies AS pc ON pj.company_ID=pc.ID
					INNER JOIN pp_job_industries AS ji ON pj.industry_ID=ji.ID	
					WHERE pj.sts = 'active' AND pc.sts='active'
					ORDER BY company_ID DESC, ID DESC
			) pj
) jobs_ranked 
WHERE jobs_ranked.rank <= 2
ORDER BY jobs_ranked.ID DESC 
LIMIT from_limit,to_limit;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_posted_job_by_company_id` (IN `comp_id` INT(11), `from_limit` INT(4), `to_limit` INT(4))  BEGIN
	#Routine body goes here...
	SELECT pp_post_jobs.ID, pp_post_jobs.job_title, pp_post_jobs.job_slug, pp_post_jobs.employer_ID, pp_post_jobs.last_date, pp_post_jobs.dated, pp_post_jobs.city, pp_post_jobs.job_description, pp_post_jobs.is_featured, pp_post_jobs.sts, pp_job_industries.industry_name, pc.company_name, pc.company_logo
	FROM `pp_post_jobs` 
	INNER JOIN pp_companies AS pc ON pp_post_jobs.company_ID=pc.ID
	INNER JOIN pp_job_industries ON pp_post_jobs.industry_ID=pp_job_industries.ID
	WHERE pp_post_jobs.company_ID=comp_id
	ORDER BY ID DESC
	LIMIT from_limit, to_limit;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_posted_job_by_employer_id` (IN `emp_id` INT(11), `from_limit` INT(4), `to_limit` INT(4))  BEGIN
	#Routine body goes here...
	SELECT pp_post_jobs.ID, pp_post_jobs.job_title, pp_post_jobs.job_slug, pp_post_jobs.job_description, pp_post_jobs.contact_person, pp_post_jobs.contact_email, pp_post_jobs.contact_phone, pp_post_jobs.employer_ID, pp_post_jobs.last_date, pp_post_jobs.dated, pp_post_jobs.city, pp_post_jobs.is_featured, pp_post_jobs.sts, pp_job_industries.industry_name, pc.company_name, pc.company_logo
	FROM `pp_post_jobs` 
	INNER JOIN pp_companies AS pc ON pp_post_jobs.company_ID=pc.ID
	INNER JOIN pp_employers AS emp ON pp_post_jobs.employer_ID=emp.ID
	INNER JOIN pp_job_industries ON pp_post_jobs.industry_ID=pp_job_industries.ID
	WHERE pp_post_jobs.employer_ID=emp_id
	ORDER BY ID DESC
	LIMIT from_limit, to_limit;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_posted_job_by_id` (IN `job_id` INT(11))  BEGIN
	#Routine body goes here...
	SELECT pp_post_jobs.*, pc.ID AS CID, pp_job_industries.industry_name, pc.company_name, pc.company_email, pc.company_ceo, pc.company_description, pc.company_logo, pc.company_phone, pc.company_website, pc.company_fax,pc.no_of_offices, pc.no_of_employees, pc.established_in, pc.industry_ID AS cat_ID, pc.company_location, pc.company_slug
,em.city as emp_city, em.country as emp_country
	FROM `pp_post_jobs` 
	INNER JOIN pp_companies AS pc ON pp_post_jobs.company_ID=pc.ID
  INNER JOIN pp_employers AS em ON pc.ID=em.company_ID
	INNER JOIN pp_job_industries ON pp_post_jobs.industry_ID=pp_job_industries.ID
	WHERE pp_post_jobs.ID=job_id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_posted_job_by_id_employer_id` (IN `job_id` INT(11), `emp_id` INT(11))  BEGIN
	SELECT pp_post_jobs.*, pc.ID AS CID, pp_job_industries.industry_name, pc.company_name, pc.company_email, pc.company_ceo, pc.company_description, pc.company_logo, pc.company_phone, pc.company_website, pc.company_fax,pc.no_of_offices, pc.no_of_employees, pc.established_in, pc.industry_ID AS cat_ID, pc.company_location, pc.company_slug
	FROM `pp_post_jobs` 
	INNER JOIN pp_companies AS pc ON pp_post_jobs.company_ID=pc.ID
	INNER JOIN pp_employers AS emp ON pp_post_jobs.employer_ID=emp.ID
	INNER JOIN pp_job_industries ON pp_post_jobs.industry_ID=pp_job_industries.ID
	WHERE pp_post_jobs.ID=job_id AND pp_post_jobs.employer_ID=emp_id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_qualification_by_jobseeker_id` (IN `jobseeker_id` INT(11))  BEGIN
	SELECT pp_seeker_academic.* 
	FROM `pp_seeker_academic`
	WHERE pp_seeker_academic.seeker_ID=jobseeker_id 
	ORDER BY pp_seeker_academic.completion_year DESC;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `job_search_by_category` (IN `param_category` VARCHAR(255), IN `from_limit` INT(5), IN `to_limit` INT(5))  BEGIN
	SELECT pj.ID, pj.job_title, pj.job_slug, pj.employer_ID, pj.company_ID, pj.job_description, pj.city, pj.dated, pj.last_date, pj.is_featured, pj.sts, pc.company_name, pc.company_logo, pc.company_slug
	FROM pp_post_jobs pj
	INNER JOIN pp_companies pc ON pc.ID = pj.company_ID 
	WHERE pj.category = param_category AND pj.sts = 'active' AND pc.sts = 'active'
	ORDER BY pc.ID DESC
	LIMIT from_limit, to_limit;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `job_search_by_city` (IN `param_city` VARCHAR(255), IN `from_limit` INT(5), IN `to_limit` INT(5))  BEGIN
	SELECT pj.ID, pj.job_title, pj.job_slug, pj.employer_ID, pj.company_ID, pj.job_description, pj.city, pj.dated, pj.last_date, pj.is_featured, pj.sts, pc.company_name, pc.company_logo, pc.company_slug
	FROM pp_post_jobs pj
	INNER JOIN pp_companies pc ON pc.ID = pj.company_ID 
	WHERE pj.city = param_city AND pj.sts = 'active' AND pc.sts = 'active'
	ORDER BY pj.dated DESC
	LIMIT from_limit, to_limit;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `job_search_by_industry` (IN `param` VARCHAR(255), `from_limit` INT(5), `to_limit` INT(5))  BEGIN
	SELECT pj.ID, pj.job_title, pj.job_slug, pj.employer_ID, pj.company_ID, pj.job_description, pj.city, pj.dated, pj.last_date, pj.is_featured, pj.sts, pc.company_name, pc.company_logo, pc.company_slug
	FROM pp_post_jobs pj
	INNER JOIN pp_companies pc ON pc.ID = pj.company_ID 
	WHERE pj.industry_ID = param AND pj.sts = 'active' AND pc.sts = 'active'
	ORDER BY pj.dated DESC
	LIMIT from_limit, to_limit;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `search_posted_jobs` (IN `where_condition` VARCHAR(255), `from_limit` INT(11), `to_limit` INT(11))  BEGIN
	#Routine body goes here...
SET @query = "SELECT pj.ID, pj.job_title,  pj.job_slug, pj.employer_ID, pj.company_ID, pj.city, pj.dated, pj.last_date, pj.is_featured, pj.sts, pc.company_name, pc.company_logo 
	FROM `pp_post_jobs` pj 
	LEFT JOIN pp_companies AS pc ON pj.company_ID=pc.ID 
	WHERE
";

SET @where_clause = CONCAT(where_condition);
SET @after_where_clause = CONCAT("ORDER BY ID DESC LIMIT ",from_limit,", ",to_limit,"");
SET @full_search_clause = CONCAT(@where_clause, @after_where_clause);
SET @query = CONCAT(@query, @full_search_clause);

PREPARE stmt FROM @query;
EXECUTE stmt;

END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `general` int(1) NOT NULL,
  `Obc` int(1) NOT NULL,
  `Sc/St` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pp_admin`
--

CREATE TABLE `pp_admin` (
  `id` int(8) NOT NULL,
  `admin_username` varchar(80) DEFAULT NULL,
  `admin_password` varchar(100) DEFAULT NULL,
  `type` tinyint(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pp_admin`
--

INSERT INTO `pp_admin` (`id`, `admin_username`, `admin_password`, `type`) VALUES
(1, 'admin', 'admin', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pp_ad_codes`
--

CREATE TABLE `pp_ad_codes` (
  `ID` int(4) NOT NULL,
  `bottom` text DEFAULT NULL,
  `right_side_1` text DEFAULT NULL,
  `right_side_2` text DEFAULT NULL,
  `google_analytics` text DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pp_ad_codes`
--

INSERT INTO `pp_ad_codes` (`ID`, `bottom`, `right_side_1`, `right_side_2`, `google_analytics`) VALUES
(1, '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `pp_category`
--

CREATE TABLE `pp_category` (
  `id` int(11) NOT NULL,
  `category` varchar(20) CHARACTER SET latin1 NOT NULL,
  `job_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pp_category`
--

INSERT INTO `pp_category` (`id`, `category`, `job_id`) VALUES
(1, 'General', 1),
(2, 'EWS', 1),
(3, 'OBC', 1),
(4, 'SC', 1),
(5, 'ST', 1),
(6, 'General', 2),
(7, 'General', 3),
(8, 'SC', 3),
(9, 'ST', 3),
(10, 'EWS', 4),
(11, 'SC', 4);

-- --------------------------------------------------------

--
-- Table structure for table `pp_cities`
--

CREATE TABLE `pp_cities` (
  `ID` int(11) NOT NULL,
  `show` tinyint(1) NOT NULL DEFAULT 1,
  `city_slug` varchar(150) NOT NULL,
  `city_name` varchar(150) DEFAULT NULL,
  `sort_order` int(3) NOT NULL DEFAULT 998,
  `country_ID` int(11) NOT NULL,
  `is_popular` enum('yes','no') NOT NULL DEFAULT 'no'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pp_cities`
--

INSERT INTO `pp_cities` (`ID`, `show`, `city_slug`, `city_name`, `sort_order`, `country_ID`, `is_popular`) VALUES
(27, 1, '', 'Gujarat', 998, 0, 'no'),
(31, 1, '', 'Karnataka', 998, 0, 'no'),
(30, 1, '', 'Jammu and Kashmir', 998, 0, 'no'),
(28, 1, '', 'Haryana', 998, 0, 'no'),
(29, 1, '', 'Himachal Pradesh', 998, 0, 'no'),
(22, 1, '', 'Arunchal Pradesh', 998, 0, 'no'),
(26, 1, '', 'Goa', 998, 0, 'no'),
(23, 1, '', 'Assam', 998, 0, 'no'),
(24, 1, '', 'Bihar', 998, 0, 'no'),
(25, 1, '', 'Chhattisgarh', 998, 0, 'no'),
(21, 1, '', 'Andhra Pradesh', 998, 0, 'no'),
(20, 1, '', 'Jharkhand', 998, 0, 'no'),
(32, 1, '', 'Kerala', 998, 0, 'no'),
(33, 1, '', 'Madhya Pradesh', 998, 0, 'no'),
(34, 1, '', 'Maharashtra', 998, 0, 'no'),
(35, 1, '', 'Manipur', 998, 0, 'no'),
(36, 1, '', 'Meghalya', 998, 0, 'no'),
(37, 1, '', 'Mizoram', 998, 0, 'no'),
(38, 1, '', 'Nagaland', 998, 0, 'no'),
(39, 1, '', 'Odisha', 998, 0, 'no'),
(40, 1, '', 'Panjab', 998, 0, 'no'),
(41, 1, '', 'Rajasthan', 998, 0, 'no'),
(42, 1, '', 'Sikkim', 998, 0, 'no'),
(43, 1, '', 'Tamil Nadu', 998, 0, 'no'),
(44, 1, '', 'Telangana', 998, 0, 'no'),
(45, 1, '', 'Tripura', 998, 0, 'no'),
(46, 1, '', 'Uttarakhand', 998, 0, 'no'),
(47, 1, '', 'Uttar Pradesh', 998, 0, 'no'),
(48, 1, '', 'West Bengal', 998, 0, 'no'),
(49, 1, '', 'Andaman and Nicobar Islands', 998, 0, 'no'),
(50, 1, '', 'Chandigarh', 998, 0, 'no'),
(51, 1, '', 'Dadra and Nagar Haveli', 998, 0, 'no'),
(52, 1, '', 'Daman and Diu', 998, 0, 'no'),
(53, 1, '', 'Delhi', 998, 0, 'no'),
(54, 1, '', 'Lakshadweep', 998, 0, 'no'),
(55, 1, '', 'Puducherry', 998, 0, 'no'),
(56, 1, '', 'Ladakh', 998, 0, 'no');

-- --------------------------------------------------------

--
-- Table structure for table `pp_cms`
--

CREATE TABLE `pp_cms` (
  `pageID` int(11) NOT NULL,
  `pageTitle` varchar(100) DEFAULT NULL,
  `pageSlug` varchar(100) DEFAULT NULL,
  `pageContent` text DEFAULT NULL,
  `pageImage` varchar(100) DEFAULT NULL,
  `pageParentPageID` int(11) DEFAULT 0,
  `dated` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `pageStatus` enum('Inactive','Published') DEFAULT 'Inactive',
  `seoMetaTitle` varchar(100) DEFAULT NULL,
  `seoMetaKeyword` varchar(255) DEFAULT NULL,
  `seoMetaDescription` varchar(255) DEFAULT NULL,
  `seoAllowCrawler` tinyint(1) DEFAULT 1,
  `pageCss` text DEFAULT NULL,
  `pageScript` text DEFAULT NULL,
  `menuTop` tinyint(4) DEFAULT 0,
  `menuBottom` tinyint(4) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pp_cms`
--

INSERT INTO `pp_cms` (`pageID`, `pageTitle`, `pageSlug`, `pageContent`, `pageImage`, `pageParentPageID`, `dated`, `pageStatus`, `seoMetaTitle`, `seoMetaKeyword`, `seoMetaDescription`, `seoAllowCrawler`, `pageCss`, `pageScript`, `menuTop`, `menuBottom`) VALUES
(7, 'About Us', 'about-us.html', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis fermentum, dolor non vulputate pretium, nisl enim posuere leo, vel dictum orci dolor non est. Sed lacus lorem, pulvinar sit amet hendrerit a, varius eu est. Fusce ut turpis enim. Sed vel gravida velit, vel vulputate tortor. Suspendisse ut congue sem, vitae dignissim nulla. In at neque sagittis, pulvinar risus sit amet, tincidunt enim. Proin placerat lorem nisl, a molestie sem ornare quis. Duis bibendum, lectus et rhoncus auctor, massa dolor efficitur risus, a hendrerit quam nulla ut enim. Suspendisse potenti. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.<br />\n<br />\nAliquam imperdiet cursus felis, sed posuere nunc. In sollicitudin accumsan orci, eu aliquet lectus tempus nec. Fusce facilisis metus a diam dignissim tristique. Fusce id ligula lectus. In tempor ut purus vel pharetra. Quisque ultrices justo id lectus tristique finibus. Praesent facilisis velit eu elementum tempus. In vel lectus congue, ultricies orci congue, imperdiet massa. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sollicitudin, magna ultricies vulputate feugiat, tortor arcu dignissim urna, vitae porta sem justo ut enim. Donec ullamcorper tellus vel fringilla varius. In nec felis quam. Quisque ut nunc non dui bibendum tristique quis accumsan libero.<br />\n<br />\nNunc finibus nisi id nisi scelerisque eleifend. Sed vulputate finibus vestibulum. Nulla facilisi. Etiam convallis leo nisl, et hendrerit ligula ornare ut. Nunc et enim ultrices, vehicula dui sit amet, fringilla tellus. Quisque eu elit lorem. Nunc hendrerit orci ut ex molestie, eget semper lorem cursus. Proin congue consectetur felis et cursus. Sed aliquam nunc nec odio ultricies, eget aliquet nisl porta. Phasellus consequat eleifend enim. Donec in tincidunt diam, id mattis nulla. Cras in luctus arcu, eu efficitur mi. Interdum et malesuada fames ac ante ipsum primis in faucibus. In tincidunt sapien libero, sit amet convallis tortor sollicitudin non. Sed id nulla ac nulla volutpat vehicula. Morbi lacus nunc, tristique rutrum molestie vel, tincidunt ut lectus.<br />\nAliquam imperdiet cursus<br />\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Duis fermentum, dolor non vulputate pretium, nisl enim posuere leo, vel dictum orci dolor non est. Sed lacus lorem, pulvinar sit amet hendrerit a, varius eu est. Fusce ut turpis enim. Sed vel gravida velit, vel vulputate tortor. Suspendisse ut congue sem, vitae dignissim nulla. In at neque sagittis, pulvinar risus sit amet, tincidunt enim. Proin placerat lorem nisl, a molestie sem ornare quis. Duis bibendum, lectus et rhoncus auctor, massa dolor efficitur risus, a hendrerit quam nulla ut enim. Suspendisse potenti. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.<br />\n<br />\nAliquam imperdiet cursus felis, sed posuere nunc. In sollicitudin accumsan orci, eu aliquet lectus tempus nec. Fusce facilisis metus a diam dignissim tristique. Fusce id ligula lectus. In tempor ut purus vel pharetra. Quisque ultrices justo id lectus tristique finibus. Praesent facilisis velit eu elementum tempus. In vel lectus congue, ultricies orci congue, imperdiet massa. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sollicitudin, magna ultricies vulputate feugiat, tortor arcu dignissim urna, vitae porta sem justo ut enim. Donec ullamcorper tellus vel fringilla varius. In nec felis quam. Quisque ut nunc non dui bibendum tristique quis accumsan libero.<br />\n<br />\nNunc finibus nisi id nisi scelerisque eleifend. Sed vulputate finibus vestibulum. Nulla facilisi. Etiam convallis leo nisl, et hendrerit ligula ornare ut. Nunc et enim ultrices, vehicula dui sit amet, fringilla tellus. Quisque eu elit lorem. Nunc hendrerit orci ut ex molestie, eget semper lorem cursus. Proin congue consectetur felis et cursus. Sed aliquam nunc nec odio ultricies, eget aliquet nisl porta. Phasellus consequat eleifend enim. Donec in tincidunt diam, id mattis nulla. Cras in luctus arcu, eu efficitur mi. Interdum et malesuada fames ac ante ipsum primis in faucibus. In tincidunt sapien libero, sit amet convallis tortor sollicitudin non. Sed id nulla ac nulla volutpat vehicula. Morbi lacus nunc, tristique rutrum molestie vel, tincidunt ut lectus.<br />\nSuspendisse quis mi commodo, eleifend massa ut, dapibus ligula.<br />\nMaecenas sagittis sem sed sapien blandit venenatis.<br />\nPraesent eleifend ligula id ex condimentum, eu finibus lorem hendrerit.<br />\nVestibulum consequat nunc a elit faucibus lacinia.<br />\nProin quis libero eget lorem vulputate imperdiet.<br />\nVivamus iaculis arcu eget libero imperdiet, sit amet posuere ante consectetur.', 'about-company1.jpg', 0, '2016-11-26 18:03:43', 'Published', 'About Us', 'About Job Portal, Jobs, IT', 'The leading online job portal', 1, NULL, NULL, 1, 1),
(13, 'How To Get Job', 'how-to-get-job.html', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis fermentum, dolor non vulputate pretium, nisl enim posuere leo, vel dictum orci dolor non est. Sed lacus lorem, pulvinar sit amet hendrerit a, varius eu est. Fusce ut turpis enim. Sed vel gravida velit, vel vulputate tortor. Suspendisse ut congue sem, vitae dignissim nulla. In at neque sagittis, pulvinar risus sit amet, tincidunt enim. Proin placerat lorem nisl, a molestie sem ornare quis. Duis bibendum, lectus et rhoncus auctor, massa dolor efficitur risus, a hendrerit quam nulla ut enim. Suspendisse potenti. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.<br />\n<br />\nAliquam imperdiet cursus felis, sed posuere nunc. In sollicitudin accumsan orci, eu aliquet lectus tempus nec. Fusce facilisis metus a diam dignissim tristique. Fusce id ligula lectus. In tempor ut purus vel pharetra. Quisque ultrices justo id lectus tristique finibus. Praesent facilisis velit eu elementum tempus. In vel lectus congue, ultricies orci congue, imperdiet massa. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sollicitudin, magna ultricies vulputate feugiat, tortor arcu dignissim urna, vitae porta sem justo ut enim. Donec ullamcorper tellus vel fringilla varius. In nec felis quam. Quisque ut nunc non dui bibendum tristique quis accumsan libero.<br />\n<br />\nNunc finibus nisi id nisi scelerisque eleifend. Sed vulputate finibus vestibulum. Nulla facilisi. Etiam convallis leo nisl, et hendrerit ligula ornare ut. Nunc et enim ultrices, vehicula dui sit amet, fringilla tellus. Quisque eu elit lorem. Nunc hendrerit orci ut ex molestie, eget semper lorem cursus. Proin congue consectetur felis et cursus. Sed aliquam nunc nec odio ultricies, eget aliquet nisl porta. Phasellus consequat eleifend enim. Donec in tincidunt diam, id mattis nulla. Cras in luctus arcu, eu efficitur mi. Interdum et malesuada fames ac ante ipsum primis in faucibus. In tincidunt sapien libero, sit amet convallis tortor sollicitudin non. Sed id nulla ac nulla volutpat vehicula. Morbi lacus nunc, tristique rutrum molestie vel, tincidunt ut lectus.<br />\nAliquam imperdiet cursus<br />\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Duis fermentum, dolor non vulputate pretium, nisl enim posuere leo, vel dictum orci dolor non est. Sed lacus lorem, pulvinar sit amet hendrerit a, varius eu est. Fusce ut turpis enim. Sed vel gravida velit, vel vulputate tortor. Suspendisse ut congue sem, vitae dignissim nulla. In at neque sagittis, pulvinar risus sit amet, tincidunt enim. Proin placerat lorem nisl, a molestie sem ornare quis. Duis bibendum, lectus et rhoncus auctor, massa dolor efficitur risus, a hendrerit quam nulla ut enim. Suspendisse potenti. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.', NULL, 0, '2016-11-26 18:09:56', 'Published', 'How To Get Job', 'Tips, Job, Online', 'How to get job includes tips and tricks to crack interview', 1, NULL, NULL, 0, 0),
(14, 'Interview', 'interview.html', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis fermentum, dolor non vulputate pretium, nisl enim posuere leo, vel dictum orci dolor non est. Sed lacus lorem, pulvinar sit amet hendrerit a, varius eu est. Fusce ut turpis enim. Sed vel gravida velit, vel vulputate tortor. Suspendisse ut congue sem, vitae dignissim nulla. In at neque sagittis, pulvinar risus sit amet, tincidunt enim. Proin placerat lorem nisl, a molestie sem ornare quis. Duis bibendum, lectus et rhoncus auctor, massa dolor efficitur risus, a hendrerit quam nulla ut enim. Suspendisse potenti. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.<br />\n<br />\nAliquam imperdiet cursus felis, sed posuere nunc. In sollicitudin accumsan orci, eu aliquet lectus tempus nec. Fusce facilisis metus a diam dignissim tristique. Fusce id ligula lectus. In tempor ut purus vel pharetra. Quisque ultrices justo id lectus tristique finibus. Praesent facilisis velit eu elementum tempus. In vel lectus congue, ultricies orci congue, imperdiet massa. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sollicitudin, magna ultricies vulputate feugiat, tortor arcu dignissim urna, vitae porta sem justo ut enim. Donec ullamcorper tellus vel fringilla varius. In nec felis quam. Quisque ut nunc non dui bibendum tristique quis accumsan libero.<br />\n<br />\nNunc finibus nisi id nisi scelerisque eleifend. Sed vulputate finibus vestibulum. Nulla facilisi. Etiam convallis leo nisl, et hendrerit ligula ornare ut. Nunc et enim ultrices, vehicula dui sit amet, fringilla tellus. Quisque eu elit lorem. Nunc hendrerit orci ut ex molestie, eget semper lorem cursus. Proin congue consectetur felis et cursus. Sed aliquam nunc nec odio ultricies, eget aliquet nisl porta. Phasellus consequat eleifend enim. Donec in tincidunt diam, id mattis nulla. Cras in luctus arcu, eu efficitur mi. Interdum et malesuada fames ac ante ipsum primis in faucibus. In tincidunt sapien libero, sit amet convallis tortor sollicitudin non. Sed id nulla ac nulla volutpat vehicula. Morbi lacus nunc, tristique rutrum molestie vel, tincidunt ut lectus.<br />\nAliquam imperdiet cursus<br />\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Duis fermentum, dolor non vulputate pretium, nisl enim posuere leo, vel dictum orci dolor non est. Sed lacus lorem, pulvinar sit amet hendrerit a, varius eu est. Fusce ut turpis enim. Sed vel gravida velit, vel vulputate tortor. Suspendisse ut congue sem, vitae dignissim nulla. In at neque sagittis, pulvinar risus sit amet, tincidunt enim. Proin placerat lorem nisl, a molestie sem ornare quis. Duis bibendum, lectus et rhoncus auctor, massa dolor efficitur risus, a hendrerit quam nulla ut enim. Suspendisse potenti. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.', NULL, 0, '2016-11-26 18:12:22', 'Published', 'Interview', 'job, jobs, interview, tips', 'How to take interview', 1, NULL, NULL, 0, 0),
(15, 'CV Writing', 'cv-writing.html', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis fermentum, dolor non vulputate pretium, nisl enim posuere leo, vel dictum orci dolor non est. Sed lacus lorem, pulvinar sit amet hendrerit a, varius eu est. Fusce ut turpis enim. Sed vel gravida velit, vel vulputate tortor. Suspendisse ut congue sem, vitae dignissim nulla. In at neque sagittis, pulvinar risus sit amet, tincidunt enim. Proin placerat lorem nisl, a molestie sem ornare quis. Duis bibendum, lectus et rhoncus auctor, massa dolor efficitur risus, a hendrerit quam nulla ut enim. Suspendisse potenti. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.<br />\n<br />\nAliquam imperdiet cursus felis, sed posuere nunc. In sollicitudin accumsan orci, eu aliquet lectus tempus nec. Fusce facilisis metus a diam dignissim tristique. Fusce id ligula lectus. In tempor ut purus vel pharetra. Quisque ultrices justo id lectus tristique finibus. Praesent facilisis velit eu elementum tempus. In vel lectus congue, ultricies orci congue, imperdiet massa. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sollicitudin, magna ultricies vulputate feugiat, tortor arcu dignissim urna, vitae porta sem justo ut enim. Donec ullamcorper tellus vel fringilla varius. In nec felis quam. Quisque ut nunc non dui bibendum tristique quis accumsan libero.<br />\n<br />\nNunc finibus nisi id nisi scelerisque eleifend. Sed vulputate finibus vestibulum. Nulla facilisi. Etiam convallis leo nisl, et hendrerit ligula ornare ut. Nunc et enim ultrices, vehicula dui sit amet, fringilla tellus. Quisque eu elit lorem. Nunc hendrerit orci ut ex molestie, eget semper lorem cursus. Proin congue consectetur felis et cursus. Sed aliquam nunc nec odio ultricies, eget aliquet nisl porta. Phasellus consequat eleifend enim. Donec in tincidunt diam, id mattis nulla. Cras in luctus arcu, eu efficitur mi. Interdum et malesuada fames ac ante ipsum primis in faucibus. In tincidunt sapien libero, sit amet convallis tortor sollicitudin non. Sed id nulla ac nulla volutpat vehicula. Morbi lacus nunc, tristique rutrum molestie vel, tincidunt ut lectus.<br />\nAliquam imperdiet cursus<br />\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Duis fermentum, dolor non vulputate pretium, nisl enim posuere leo, vel dictum orci dolor non est. Sed lacus lorem, pulvinar sit amet hendrerit a, varius eu est. Fusce ut turpis enim. Sed vel gravida velit, vel vulputate tortor. Suspendisse ut congue sem, vitae dignissim nulla. In at neque sagittis, pulvinar risus sit amet, tincidunt enim. Proin placerat lorem nisl, a molestie sem ornare quis. Duis bibendum, lectus et rhoncus auctor, massa dolor efficitur risus, a hendrerit quam nulla ut enim. Suspendisse potenti. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.', NULL, 0, '2016-11-26 18:14:04', 'Published', 'CV writing tips and tricks', 'CV, resume', 'How to write a professional CV.', 1, NULL, NULL, 0, 0),
(16, 'Privacy Policy', 'privacy-policy.html', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis fermentum, dolor non vulputate pretium, nisl enim posuere leo, vel dictum orci dolor non est. Sed lacus lorem, pulvinar sit amet hendrerit a, varius eu est. Fusce ut turpis enim. Sed vel gravida velit, vel vulputate tortor. Suspendisse ut congue sem, vitae dignissim nulla. In at neque sagittis, pulvinar risus sit amet, tincidunt enim. Proin placerat lorem nisl, a molestie sem ornare quis. Duis bibendum, lectus et rhoncus auctor, massa dolor efficitur risus, a hendrerit quam nulla ut enim. Suspendisse potenti. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.<br />\n<br />\nAliquam imperdiet cursus felis, sed posuere nunc. In sollicitudin accumsan orci, eu aliquet lectus tempus nec. Fusce facilisis metus a diam dignissim tristique. Fusce id ligula lectus. In tempor ut purus vel pharetra. Quisque ultrices justo id lectus tristique finibus. Praesent facilisis velit eu elementum tempus. In vel lectus congue, ultricies orci congue, imperdiet massa. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sollicitudin, magna ultricies vulputate feugiat, tortor arcu dignissim urna, vitae porta sem justo ut enim. Donec ullamcorper tellus vel fringilla varius. In nec felis quam. Quisque ut nunc non dui bibendum tristique quis accumsan libero.<br />\n<br />\nNunc finibus nisi id nisi scelerisque eleifend. Sed vulputate finibus vestibulum. Nulla facilisi. Etiam convallis leo nisl, et hendrerit ligula ornare ut. Nunc et enim ultrices, vehicula dui sit amet, fringilla tellus. Quisque eu elit lorem. Nunc hendrerit orci ut ex molestie, eget semper lorem cursus. Proin congue consectetur felis et cursus. Sed aliquam nunc nec odio ultricies, eget aliquet nisl porta. Phasellus consequat eleifend enim. Donec in tincidunt diam, id mattis nulla. Cras in luctus arcu, eu efficitur mi. Interdum et malesuada fames ac ante ipsum primis in faucibus. In tincidunt sapien libero, sit amet convallis tortor sollicitudin non. Sed id nulla ac nulla volutpat vehicula. Morbi lacus nunc, tristique rutrum molestie vel, tincidunt ut lectus.<br />\nAliquam imperdiet cursus<br />\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Duis fermentum, dolor non vulputate pretium, nisl enim posuere leo, vel dictum orci dolor non est. Sed lacus lorem, pulvinar sit amet hendrerit a, varius eu est. Fusce ut turpis enim. Sed vel gravida velit, vel vulputate tortor. Suspendisse ut congue sem, vitae dignissim nulla. In at neque sagittis, pulvinar risus sit amet, tincidunt enim. Proin placerat lorem nisl, a molestie sem ornare quis. Duis bibendum, lectus et rhoncus auctor, massa dolor efficitur risus, a hendrerit quam nulla ut enim. Suspendisse potenti. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.', NULL, 0, '2016-11-26 09:42:48', 'Published', 'Privacy Policy', 'Privacy, policies', 'Job portal privacy policies', 1, NULL, NULL, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pp_cms_previous`
--

CREATE TABLE `pp_cms_previous` (
  `ID` int(11) NOT NULL,
  `page` varchar(60) DEFAULT NULL,
  `heading` varchar(155) DEFAULT NULL,
  `content` text DEFAULT NULL,
  `page_slug` varchar(100) DEFAULT NULL,
  `sts` enum('blocked','active') DEFAULT 'active',
  `dated` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pp_cms_previous`
--

INSERT INTO `pp_cms_previous` (`ID`, `page`, `heading`, `content`, `page_slug`, `sts`, `dated`) VALUES
(4, NULL, 'About Us', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi a velit sed risus pulvinar faucibus. Nulla facilisi. Nullam vehicula nec ligula eu vulputate. Nunc id ultrices mi, ac tristique lectus. Suspendisse porta ultrices ultricies. Sed quis nisi vel magna maximus aliquam a vel nisl. Cras non rutrum diam. Nulla sed ipsum a felis posuere pharetra ut sit amet augue. Sed id nisl sodales, vulputate mi eu, viverra neque. Fusce fermentum, est ut accumsan accumsan, risus ante varius diam, non venenatis eros ligula fermentum leo. Etiam consectetur imperdiet volutpat. Donec ut pharetra nisi, eget pellentesque tortor. Integer eleifend dolor eu ex lobortis, ac gravida augue tristique. Proin placerat consectetur tincidunt. Nullam sollicitudin, neque eget iaculis ultricies, est justo pulvinar turpis, vulputate convallis leo orci at sapien.\n<br /><br />\nQuisque ac scelerisque libero, nec blandit neque. Nullam felis nisl, elementum eu sapien ut, convallis interdum felis. In turpis odio, fermentum non pulvinar gravida, posuere quis magna. Ut mollis eget neque at euismod. Interdum et malesuada fames ac ante ipsum primis in faucibus. Integer faucibus orci a pulvinar malesuada. Aenean at felis vitae lorem venenatis consequat. Nam non nunc euismod, consequat ligula non, tristique odio. Ut leo sapien, aliquet sed ultricies et, scelerisque quis nulla. Aenean non sapien maximus, convallis eros vitae, iaculis massa. In fringilla hendrerit nisi, eu pellentesque massa faucibus molestie. Etiam laoreet eros quis faucibus rutrum. Quisque eleifend purus justo, eget tempus quam interdum non.\n<br /><br />\nNullam enim ex, vulputate at ultricies bibendum, interdum sit amet tortor. Fusce semper augue ac ipsum ultricies interdum. Cras maximus faucibus sapien, et lacinia leo efficitur id. Nullam laoreet pulvinar nibh et ullamcorper. Etiam a lorem rhoncus, rutrum felis sed, blandit orci. Nulla vel tellus gravida, pretium neque a, fringilla lectus. Morbi et leo mi. Aliquam interdum ex ipsum. Vivamus eu ultrices ante, eget volutpat massa. Nulla nisi purus, sollicitudin euismod eleifend pulvinar, dictum rutrum lacus. Nam hendrerit sed arcu a pellentesque. Vestibulum maximus ligula tellus, a euismod dui feugiat et. Aliquam viverra blandit est nec ultricies.\n<br /><br />\nNullam et sem a dui accumsan ornare. Praesent faucibus ultricies orci. Maecenas hendrerit tincidunt rutrum. Phasellus eget libero eget ante interdum venenatis. Cras sodales finibus vulputate. Aenean aliquet velit eget felis pellentesque, et blandit ex facilisis. Vivamus sit amet euismod diam, at rhoncus ex. Nullam consectetur, erat ut maximus dignissim, ex eros pellentesque ex, at dictum odio dui in urna. Nulla rutrum nisi eget risus accumsan, sit amet iaculis risus interdum. Curabitur accumsan eu purus nec condimentum. Fusce pulvinar ex id sagittis sodales. Donec hendrerit scelerisque est, in viverra nibh lobortis et.\n<br /><br />\n<ul>\n<li>Quisque facilisis purus vel sem laoreet posuere.</li>\n<li>Proin eleifend velit ut elit sollicitudin scelerisque.</li>\n<li>Nulla aliquet urna in magna congue, ac hendrerit velit lacinia.</li>\n<li>Aliquam id urna ut lorem porta vulputate.</li>\n<li>Sed ultrices sem quis risus tincidunt, ut lacinia nunc aliquet.</li>\n<li>Phasellus in est suscipit, feugiat tortor ac, iaculis enim.</li>\n</ul>', 'about_us.html', 'active', '2014-05-16 13:47:11'),
(12, NULL, 'First Day of New Job', '<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<br />\r\n<br />\r\n<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<br />\r\n<br />\r\n<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<br />\r\n&nbsp;', 'first_day_job.html', 'active', '2014-05-16 14:46:14'),
(13, NULL, 'Privacy Policy', '<strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<br />\n<br />\n<strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<br />\n<br />\n<strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'privacy-policy.html', 'active', '2015-05-20 23:38:56'),
(15, NULL, 'Why Job', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi a velit sed risus pulvinar faucibus. Nulla facilisi. Nullam vehicula nec ligula eu vulputate. Nunc id ultrices mi, ac tristique lectus. Suspendisse porta ultrices ultricies. Sed quis nisi vel magna maximus aliquam a vel nisl. Cras non rutrum diam. Nulla sed ipsum a felis posuere pharetra ut sit amet augue. Sed id nisl sodales, vulputate mi eu, viverra neque. Fusce fermentum, est ut accumsan accumsan, risus ante varius diam, non venenatis eros ligula fermentum leo. Etiam consectetur imperdiet volutpat. Donec ut pharetra nisi, eget pellentesque tortor. Integer eleifend dolor eu ex lobortis, ac gravida augue tristique. Proin placerat consectetur tincidunt. Nullam sollicitudin, neque eget iaculis ultricies, est justo pulvinar turpis, vulputate convallis leo orci at sapien.<br />\n<br />\nQuisque ac scelerisque libero, nec blandit neque. Nullam felis nisl, elementum eu sapien ut, convallis interdum felis. In turpis odio, fermentum non pulvinar gravida, posuere quis magna. Ut mollis eget neque at euismod. Interdum et malesuada fames ac ante ipsum primis in faucibus. Integer faucibus orci a pulvinar malesuada. Aenean at felis vitae lorem venenatis consequat. Nam non nunc euismod, consequat ligula non, tristique odio. Ut leo sapien, aliquet sed ultricies et, scelerisque quis nulla. Aenean non sapien maximus, convallis eros vitae, iaculis massa. In fringilla hendrerit nisi, eu pellentesque massa faucibus molestie. Etiam laoreet eros quis faucibus rutrum. Quisque eleifend purus justo, eget tempus quam interdum non.<br />\n<br />\nNullam enim ex, vulputate at ultricies bibendum, interdum sit amet tortor. Fusce semper augue ac ipsum ultricies interdum. Cras maximus faucibus sapien, et lacinia leo efficitur id. Nullam laoreet pulvinar nibh et ullamcorper. Etiam a lorem rhoncus, rutrum felis sed, blandit orci. Nulla vel tellus gravida, pretium neque a, fringilla lectus. Morbi et leo mi. Aliquam interdum ex ipsum. Vivamus eu ultrices ante, eget volutpat massa. Nulla nisi purus, sollicitudin euismod eleifend pulvinar, dictum rutrum lacus. Nam hendrerit sed arcu a pellentesque. Vestibulum maximus ligula tellus, a euismod dui feugiat et. Aliquam viverra blandit est nec ultricies.<br />\n<br />\nNullam et sem a dui accumsan ornare. Praesent faucibus ultricies orci. Maecenas hendrerit tincidunt rutrum. Phasellus eget libero eget ante interdum venenatis. Cras sodales finibus vulputate. Aenean aliquet velit eget felis pellentesque, et blandit ex facilisis. Vivamus sit amet euismod diam, at rhoncus ex. Nullam consectetur, erat ut maximus dignissim, ex eros pellentesque ex, at dictum odio dui in urna. Nulla rutrum nisi eget risus accumsan, sit amet iaculis risus interdum. Curabitur accumsan eu purus nec condimentum. Fusce pulvinar ex id sagittis sodales. Donec hendrerit scelerisque est, in viverra nibh lobortis et.', 'why_job.html', 'active', '2016-03-12 16:12:11'),
(16, NULL, 'Preparing for Interview', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi a velit sed risus pulvinar faucibus. Nulla facilisi. Nullam vehicula nec ligula eu vulputate. Nunc id ultrices mi, ac tristique lectus. Suspendisse porta ultrices ultricies. Sed quis nisi vel magna maximus aliquam a vel nisl. Cras non rutrum diam. Nulla sed ipsum a felis posuere pharetra ut sit amet augue. Sed id nisl sodales, vulputate mi eu, viverra neque. Fusce fermentum, est ut accumsan accumsan, risus ante varius diam, non venenatis eros ligula fermentum leo. Etiam consectetur imperdiet volutpat. Donec ut pharetra nisi, eget pellentesque tortor. Integer eleifend dolor eu ex lobortis, ac gravida augue tristique. Proin placerat consectetur tincidunt. Nullam sollicitudin, neque eget iaculis ultricies, est justo pulvinar turpis, vulputate convallis leo orci at sapien.\n<br /><br />\nQuisque ac scelerisque libero, nec blandit neque. Nullam felis nisl, elementum eu sapien ut, convallis interdum felis. In turpis odio, fermentum non pulvinar gravida, posuere quis magna. Ut mollis eget neque at euismod. Interdum et malesuada fames ac ante ipsum primis in faucibus. Integer faucibus orci a pulvinar malesuada. Aenean at felis vitae lorem venenatis consequat. Nam non nunc euismod, consequat ligula non, tristique odio. Ut leo sapien, aliquet sed ultricies et, scelerisque quis nulla. Aenean non sapien maximus, convallis eros vitae, iaculis massa. In fringilla hendrerit nisi, eu pellentesque massa faucibus molestie. Etiam laoreet eros quis faucibus rutrum. Quisque eleifend purus justo, eget tempus quam interdum non.\n<br /><br />\nNullam enim ex, vulputate at ultricies bibendum, interdum sit amet tortor. Fusce semper augue ac ipsum ultricies interdum. Cras maximus faucibus sapien, et lacinia leo efficitur id. Nullam laoreet pulvinar nibh et ullamcorper. Etiam a lorem rhoncus, rutrum felis sed, blandit orci. Nulla vel tellus gravida, pretium neque a, fringilla lectus. Morbi et leo mi. Aliquam interdum ex ipsum. Vivamus eu ultrices ante, eget volutpat massa. Nulla nisi purus, sollicitudin euismod eleifend pulvinar, dictum rutrum lacus. Nam hendrerit sed arcu a pellentesque. Vestibulum maximus ligula tellus, a euismod dui feugiat et. Aliquam viverra blandit est nec ultricies.\n<br /><br />\nNullam et sem a dui accumsan ornare. Praesent faucibus ultricies orci. Maecenas hendrerit tincidunt rutrum. Phasellus eget libero eget ante interdum venenatis. Cras sodales finibus vulputate. Aenean aliquet velit eget felis pellentesque, et blandit ex facilisis. Vivamus sit amet euismod diam, at rhoncus ex. Nullam consectetur, erat ut maximus dignissim, ex eros pellentesque ex, at dictum odio dui in urna. Nulla rutrum nisi eget risus accumsan, sit amet iaculis risus interdum. Curabitur accumsan eu purus nec condimentum. Fusce pulvinar ex id sagittis sodales. Donec hendrerit scelerisque est, in viverra nibh lobortis et.\n<br /><br />\n<ul>\n<li>Quisque facilisis purus vel sem laoreet posuere.</li>\n<li>Proin eleifend velit ut elit sollicitudin scelerisque.</li>\n<li>Nulla aliquet urna in magna congue, ac hendrerit velit lacinia.</li>\n<li>Aliquam id urna ut lorem porta vulputate.</li>\n<li>Sed ultrices sem quis risus tincidunt, ut lacinia nunc aliquet.</li>\n<li>Phasellus in est suscipit, feugiat tortor ac, iaculis enim.</li>\n</ul>', 'interview.html', 'active', '2016-03-12 16:17:56'),
(17, NULL, 'CV Writing Tips', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi a velit sed risus pulvinar faucibus. Nulla facilisi. Nullam vehicula nec ligula eu vulputate. Nunc id ultrices mi, ac tristique lectus. Suspendisse porta ultrices ultricies. Sed quis nisi vel magna maximus aliquam a vel nisl. Cras non rutrum diam. Nulla sed ipsum a felis posuere pharetra ut sit amet augue. Sed id nisl sodales, vulputate mi eu, viverra neque. Fusce fermentum, est ut accumsan accumsan, risus ante varius diam, non venenatis eros ligula fermentum leo. Etiam consectetur imperdiet volutpat. Donec ut pharetra nisi, eget pellentesque tortor. Integer eleifend dolor eu ex lobortis, ac gravida augue tristique. Proin placerat consectetur tincidunt. Nullam sollicitudin, neque eget iaculis ultricies, est justo pulvinar turpis, vulputate convallis leo orci at sapien.\n<br /><br />\nQuisque ac scelerisque libero, nec blandit neque. Nullam felis nisl, elementum eu sapien ut, convallis interdum felis. In turpis odio, fermentum non pulvinar gravida, posuere quis magna. Ut mollis eget neque at euismod. Interdum et malesuada fames ac ante ipsum primis in faucibus. Integer faucibus orci a pulvinar malesuada. Aenean at felis vitae lorem venenatis consequat. Nam non nunc euismod, consequat ligula non, tristique odio. Ut leo sapien, aliquet sed ultricies et, scelerisque quis nulla. Aenean non sapien maximus, convallis eros vitae, iaculis massa. In fringilla hendrerit nisi, eu pellentesque massa faucibus molestie. Etiam laoreet eros quis faucibus rutrum. Quisque eleifend purus justo, eget tempus quam interdum non.\n<br /><br />\nNullam enim ex, vulputate at ultricies bibendum, interdum sit amet tortor. Fusce semper augue ac ipsum ultricies interdum. Cras maximus faucibus sapien, et lacinia leo efficitur id. Nullam laoreet pulvinar nibh et ullamcorper. Etiam a lorem rhoncus, rutrum felis sed, blandit orci. Nulla vel tellus gravida, pretium neque a, fringilla lectus. Morbi et leo mi. Aliquam interdum ex ipsum. Vivamus eu ultrices ante, eget volutpat massa. Nulla nisi purus, sollicitudin euismod eleifend pulvinar, dictum rutrum lacus. Nam hendrerit sed arcu a pellentesque. Vestibulum maximus ligula tellus, a euismod dui feugiat et. Aliquam viverra blandit est nec ultricies.\n<br /><br />\nNullam et sem a dui accumsan ornare. Praesent faucibus ultricies orci. Maecenas hendrerit tincidunt rutrum. Phasellus eget libero eget ante interdum venenatis. Cras sodales finibus vulputate. Aenean aliquet velit eget felis pellentesque, et blandit ex facilisis. Vivamus sit amet euismod diam, at rhoncus ex. Nullam consectetur, erat ut maximus dignissim, ex eros pellentesque ex, at dictum odio dui in urna. Nulla rutrum nisi eget risus accumsan, sit amet iaculis risus interdum. Curabitur accumsan eu purus nec condimentum. Fusce pulvinar ex id sagittis sodales. Donec hendrerit scelerisque est, in viverra nibh lobortis et.\n<br /><br />\n<ul>\n<li>Quisque facilisis purus vel sem laoreet posuere.</li>\n<li>Proin eleifend velit ut elit sollicitudin scelerisque.</li>\n<li>Nulla aliquet urna in magna congue, ac hendrerit velit lacinia.</li>\n<li>Aliquam id urna ut lorem porta vulputate.</li>\n<li>Sed ultrices sem quis risus tincidunt, ut lacinia nunc aliquet.</li>\n<li>Phasellus in est suscipit, feugiat tortor ac, iaculis enim.</li>\n</ul>', 'cv_tips.html', 'active', '2016-03-12 16:19:17'),
(18, NULL, 'How to get Job', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi a velit sed risus pulvinar faucibus. Nulla facilisi. Nullam vehicula nec ligula eu vulputate. Nunc id ultrices mi, ac tristique lectus. Suspendisse porta ultrices ultricies. Sed quis nisi vel magna maximus aliquam a vel nisl. Cras non rutrum diam. Nulla sed ipsum a felis posuere pharetra ut sit amet augue. Sed id nisl sodales, vulputate mi eu, viverra neque. Fusce fermentum, est ut accumsan accumsan, risus ante varius diam, non venenatis eros ligula fermentum leo. Etiam consectetur imperdiet volutpat. Donec ut pharetra nisi, eget pellentesque tortor. Integer eleifend dolor eu ex lobortis, ac gravida augue tristique. Proin placerat consectetur tincidunt. Nullam sollicitudin, neque eget iaculis ultricies, est justo pulvinar turpis, vulputate convallis leo orci at sapien.<br />\n<br />\nQuisque ac scelerisque libero, nec blandit neque. Nullam felis nisl, elementum eu sapien ut, convallis interdum felis. In turpis odio, fermentum non pulvinar gravida, posuere quis magna. Ut mollis eget neque at euismod. Interdum et malesuada fames ac ante ipsum primis in faucibus. Integer faucibus orci a pulvinar malesuada. Aenean at felis vitae lorem venenatis consequat. Nam non nunc euismod, consequat ligula non, tristique odio. Ut leo sapien, aliquet sed ultricies et, scelerisque quis nulla. Aenean non sapien maximus, convallis eros vitae, iaculis massa. In fringilla hendrerit nisi, eu pellentesque massa faucibus molestie. Etiam laoreet eros quis faucibus rutrum. Quisque eleifend purus justo, eget tempus quam interdum non.<br />\n<br />\nNullam enim ex, vulputate at ultricies bibendum, interdum sit amet tortor. Fusce semper augue ac ipsum ultricies interdum. Cras maximus faucibus sapien, et lacinia leo efficitur id. Nullam laoreet pulvinar nibh et ullamcorper. Etiam a lorem rhoncus, rutrum felis sed, blandit orci. Nulla vel tellus gravida, pretium neque a, fringilla lectus. Morbi et leo mi. Aliquam interdum ex ipsum. Vivamus eu ultrices ante, eget volutpat massa. Nulla nisi purus, sollicitudin euismod eleifend pulvinar, dictum rutrum lacus. Nam hendrerit sed arcu a pellentesque. Vestibulum maximus ligula tellus, a euismod dui feugiat et. Aliquam viverra blandit est nec ultricies.<br />\n<br />\nNullam et sem a dui accumsan ornare. Praesent faucibus ultricies orci. Maecenas hendrerit tincidunt rutrum. Phasellus eget libero eget ante interdum venenatis. Cras sodales finibus vulputate. Aenean aliquet velit eget felis pellentesque, et blandit ex facilisis. Vivamus sit amet euismod diam, at rhoncus ex. Nullam consectetur, erat ut maximus dignissim, ex eros pellentesque ex, at dictum odio dui in urna. Nulla rutrum nisi eget risus accumsan, sit amet iaculis risus interdum. Curabitur accumsan eu purus nec condimentum. Fusce pulvinar ex id sagittis sodales. Donec hendrerit scelerisque est, in viverra nibh lobortis et.<br />\n<br />\nQuisque facilisis purus vel sem laoreet posuere.<br />\nProin eleifend velit ut elit sollicitudin scelerisque.<br />\nNulla aliquet urna in magna congue, ac hendrerit velit lacinia.<br />\nAliquam id urna ut lorem porta vulputate.<br />\nSed ultrices sem quis risus tincidunt, ut lacinia nunc aliquet.<br />\nPhasellus in est suscipit, feugiat tortor ac, iaculis enim.', 'how_to_get_job.html', 'active', '2016-03-12 16:21:26');

-- --------------------------------------------------------

--
-- Table structure for table `pp_companies`
--

CREATE TABLE `pp_companies` (
  `ID` int(11) NOT NULL,
  `company_name` varchar(155) DEFAULT NULL,
  `company_email` varchar(100) DEFAULT NULL,
  `company_ceo` varchar(60) DEFAULT NULL,
  `industry_ID` int(5) DEFAULT NULL,
  `ownership_type` enum('NGO','Private','Public') DEFAULT 'Private',
  `company_description` text DEFAULT NULL,
  `company_location` varchar(155) DEFAULT NULL,
  `no_of_offices` int(11) DEFAULT NULL,
  `company_website` varchar(155) DEFAULT NULL,
  `no_of_employees` varchar(15) DEFAULT NULL,
  `established_in` varchar(12) DEFAULT NULL,
  `company_type` varchar(60) DEFAULT NULL,
  `company_fax` varchar(30) DEFAULT NULL,
  `company_phone` varchar(30) DEFAULT NULL,
  `company_logo` varchar(155) DEFAULT NULL,
  `company_folder` varchar(155) DEFAULT NULL,
  `company_country` varchar(80) DEFAULT NULL,
  `sts` enum('blocked','pending','active') DEFAULT 'active',
  `company_city` varchar(80) DEFAULT NULL,
  `company_slug` varchar(155) DEFAULT NULL,
  `old_company_id` int(11) DEFAULT NULL,
  `old_employerlogin` varchar(100) DEFAULT NULL,
  `flag` varchar(5) DEFAULT NULL,
  `ownership_type` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pp_companies`
--

INSERT INTO `pp_companies` (`ID`, `company_name`, `company_email`, `company_ceo`, `industry_ID`, `ownership_type`, `company_description`, `company_location`, `no_of_offices`, `company_website`, `no_of_employees`, `established_in`, `company_type`, `company_fax`, `company_phone`, `company_logo`, `company_folder`, `company_country`, `sts`, `company_city`, `company_slug`, `old_company_id`, `old_employerlogin`, `flag`, `ownership_type`) VALUES
(88, 'ABCD', NULL, NULL, 22, 'Private', 'dfgdfgdf', 'dfgd', NULL, 'fdgdsfg.com', '1-10', NULL, NULL, NULL, '34543', 'JOBPORTAL-1599038448', NULL, NULL, 'active', NULL, 'abcd', NULL, NULL, NULL, 'Private'),
(89, 'Infotech', NULL, NULL, 22, 'Private', 'PHP is an open-source web development scripting language with high-grade customization capabilities that allows web developers to create highly scalable and flexible web solutions. It is extensively used to create dynamic and interactive web pages.\r\n\r\nHire PHP developers from The Brihaspati Infotech for attaining responsive, mobile-friendly, and profitable web solutions. Our PHP web development company holds vast experience while creating 1000\'s of websites and Apps, till date. We aim to deliver well-integrated websites and software that meets the holistic business needs of the client. The team consists of 50+ professional PHP experts who have expertise with core PHP, PHP based software development, eCommerce technologies, 3rd party integrations and delivering fully fledged web portals.\r\n\r\nAll of our PHP solutions are ‘Quality Tested’ to ensure an error-free orientation. Our PHP development company follow agile workflows to deliver best end product.\r\n\r\nGet in touch with our PHP development Agency today and avail 1 day free PHP development trial!', 'DHN', NULL, 'infotech.in', '601-1000', NULL, NULL, NULL, '165689', 'JOBPORTAL-1599041751.png', NULL, NULL, 'active', NULL, 'infotech', NULL, NULL, NULL, 'Private'),
(86, 'it solution', NULL, NULL, 5, 'Private', 'it company dhanbad ok', 'dhanbad', NULL, 'itsolutiondhn.com', '101-300', NULL, NULL, NULL, '55785651', 'JOBPORTAL-1598418436.png', NULL, NULL, 'active', NULL, 'it-solution', NULL, NULL, NULL, 'Public'),
(87, 'L&T', NULL, NULL, 1, 'Private', 'Headquartered in Mumbai, Larsen & Toubro Limited is one of the largest and most respected companies in India\'s private sector. With over 80 years of a strong, customer focused approach and a continuous quest for world-class quality, L&T has unmatched capabilities across Technology, Engineering, Construction, and Manufacturing, and maintains a leadership in all its major lines of business.', 'Dhanbad', NULL, 'https://www.larsentoubro.com/corporate/about-lt-group/', 'More than 2000', NULL, NULL, NULL, '8969436699', 'JOBPORTAL-1598440357.jpg', NULL, NULL, 'active', NULL, 'lt-1598503470', NULL, NULL, NULL, 'Private'),
(83, 'BPSSC', NULL, NULL, 1, 'Private', 'ihar Police Subordinate Service Commission has released new recruitment and in this recruitment, 2213 posts of Inspector and Attendant in which recruitment is done. Whichever qualified candidates want to apply in this recruitment of Bihar state and other states, in which recruitment on the post of Sub Inspector and Attendant. They can fill the online application, whose last date is 24 September 2020..', 'Bihar', NULL, 'https://apply-bpssc.com/BPSSCV2/applicationIndex', '1501-2000', NULL, NULL, NULL, '6287590493', 'JOBPORTAL-1597639269.png', NULL, NULL, 'active', NULL, 'bpssc', NULL, NULL, NULL, 'Government'),
(84, 'L&T', NULL, NULL, 1, 'Private', 'LTI (NSE: LTI) is a global technology consulting and digital solutions company helping more than 420 clients succeed in a converging world. With operations in 32 countries, we go the extra mile for our clients and accelerate their digital transformation with LTI’s Mosaic platform enabling their mobile, social, analytics, IoT, and cloud journeys.', 'Dhanbad', NULL, 'https://www.larsentoubro.com/', 'More than 2000', NULL, NULL, NULL, '8969436699', '841598432751.JPG', NULL, NULL, 'active', NULL, 'lt', NULL, NULL, NULL, 'Government'),
(85, 'jssc Exam', NULL, NULL, 1, 'Private', 'The Jharkhand Staff Selection Commission(J.S.S.C.) has been constituted by the Jharkhand Staff Selection Commission Act 2008 (Jharkhand Act 16, 2008) which was published vide Gazette notification no. 829 dt. 6 December 2008 of Jharkhand Government. The Act was amended by Jharkhand Act 03,2011 and Jharkhand Act 19,2011; published vide Gazette notification nos. 153 dt. 24 February 2011 and 687 dt. 11 October 2011 respectively.', 'Ranchi', NULL, 'jscc.nic.in', '301-600', NULL, NULL, NULL, '2545245', 'JOBPORTAL-1598332999.png', NULL, NULL, 'active', NULL, 'jssc-exam', NULL, NULL, NULL, 'Government'),
(4, 'Some IT company', NULL, NULL, 22, 'Private', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce venenatis arcu est. Phasellus vel dignissim tellus. Aenean fermentum fermentum convallis. Maecenas vitae ipsum sed risus viverra volutpat non ac sapien. Donec viverra massa at dolor imperdiet hendrerit. Nullam quis est vitae dui placerat posuere. Phasellus eget erat sit amet lacus semper consectetur. Sed a nisi nisi. Pellentesque hendrerit est id quam facilisis auctor a ut ante. Etiam metus arcu, sagittis vitae massa ac, faucibus tempus dolor. Sed et tempus ex. Aliquam interdum erat vel convallis tristique. Phasellus lectus eros, interdum ac sollicitudin vestibulum, scelerisque vitae ligula. Cras aliquam est id velit laoreet, et mattis massa ultrices. Ut aliquam mi nunc, et tempor neque malesuada in.', 'Lorem ipsum dolor sit amet, consectetur', NULL, 'www.companyurl.com', '1-10', NULL, NULL, NULL, '123456789', 'JOBPORTAL-1457693358.jpg', NULL, NULL, 'active', NULL, 'some-it-company', NULL, NULL, NULL, 'Private'),
(5, 'Abc IT Tech', NULL, NULL, 22, 'Private', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce venenatis arcu est. Phasellus vel dignissim tellus. Aenean fermentum fermentum convallis. Maecenas vitae ipsum sed risus viverra volutpat non ac sapien. Donec viverra massa at dolor imperdiet hendrerit. Nullam quis est vitae dui placerat posuere. Phasellus eget erat sit amet lacus semper consectetur. Sed a nisi nisi. Pellentesque hendrerit est id quam facilisis auctor a ut ante. Etiam metus arcu, sagittis vitae massa ac, faucibus tempus dolor. Sed et tempus ex. Aliquam interdum erat vel convallis tristique. Phasellus lectus eros, interdum ac sollicitudin vestibulum, scelerisque vitae ligula. Cras aliquam est id velit laoreet, et mattis massa ultrices. Ut aliquam mi nunc, et tempor neque malesuada in.', 'Lorem ipsum dolor sit amet', NULL, 'www.companyurl.com', '1-10', NULL, NULL, NULL, '123456789', 'JOBPORTAL-1457711170.jpg', NULL, NULL, 'active', NULL, 'abc-it-tech', NULL, NULL, NULL, 'Private'),
(6, 'Def It Company', NULL, NULL, 40, 'Private', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce venenatis arcu est. Phasellus vel dignissim tellus. Aenean fermentum fermentum convallis. Maecenas vitae ipsum sed risus viverra volutpat non ac sapien. Donec viverra massa at dolor imperdiet hendrerit. Nullam quis est vitae dui placerat posuere. Phasellus eget erat sit amet lacus semper consectetur. Sed a nisi nisi. Pellentesque hendrerit est id quam facilisis auctor a ut ante. Etiam metus arcu, sagittis vitae massa ac, faucibus tempus dolor. Sed et tempus ex. Aliquam interdum erat vel convallis tristique. Phasellus lectus eros, interdum ac sollicitudin vestibulum, scelerisque vitae ligula. Cras aliquam est id velit laoreet, et mattis massa ultrices. Ut aliquam mi nunc, et tempor neque malesuada in.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.', NULL, 'www.companyurl.com', '1-10', NULL, NULL, NULL, '123456789', 'JOBPORTAL-1457711477.jpg', NULL, NULL, 'active', NULL, 'def-it-company', NULL, NULL, NULL, 'Private'),
(7, 'Ghi Company', NULL, NULL, 10, 'Private', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce venenatis arcu est. Phasellus vel dignissim tellus. Aenean fermentum fermentum convallis. Maecenas vitae ipsum sed risus viverra volutpat non ac sapien. Donec viverra massa at dolor imperdiet hendrerit. Nullam quis est vitae dui placerat posuere. Phasellus eget erat sit amet lacus semper consectetur. Sed a nisi nisi. Pellentesque hendrerit est id quam facilisis auctor a ut ante. Etiam metus arcu, sagittis vitae massa ac, faucibus tempus dolor. Sed et tempus ex. Aliquam interdum erat vel convallis tristique. Phasellus lectus eros, interdum ac sollicitudin vestibulum, scelerisque vitae ligula. Cras aliquam est id velit laoreet, et mattis massa ultrices. Ut aliquam mi nunc, et tempor neque malesuada in.', 'Lorem ipsum dolor sit amet, consectetur', NULL, 'www.companyurl.com', '1-10', NULL, NULL, NULL, '123456789', 'JOBPORTAL-1457711897.jpg', NULL, NULL, 'active', NULL, 'ghi-company', NULL, NULL, NULL, 'Private'),
(8, 'Jkl Company', NULL, NULL, 7, 'Private', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce venenatis arcu est. Phasellus vel dignissim tellus. Aenean fermentum fermentum convallis. Maecenas vitae ipsum sed risus viverra volutpat non ac sapien. Donec viverra massa at dolor imperdiet hendrerit. Nullam quis est vitae dui placerat posuere. Phasellus eget erat sit amet lacus semper consectetur. Sed a nisi nisi. Pellentesque hendrerit est id quam facilisis auctor a ut ante. Etiam metus arcu, sagittis vitae massa ac, faucibus tempus dolor. Sed et tempus ex. Aliquam interdum erat vel convallis tristique. Phasellus lectus eros, interdum ac sollicitudin vestibulum, scelerisque vitae ligula. Cras aliquam est id velit laoreet, et mattis massa ultrices. Ut aliquam mi nunc, et tempor neque malesuada in.', 'Lorem ipsum dolor sit amet', NULL, 'www.companyurl.com', '1-10', NULL, NULL, NULL, '123456789', 'JOBPORTAL-1457712255.jpg', NULL, NULL, 'active', NULL, 'jkl-company', NULL, NULL, NULL, 'Private'),
(9, 'Mno Company', NULL, NULL, 22, 'Private', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce venenatis arcu est. Phasellus vel dignissim tellus. Aenean fermentum fermentum convallis. Maecenas vitae ipsum sed risus viverra volutpat non ac sapien. Donec viverra massa at dolor imperdiet hendrerit. Nullam quis est vitae dui placerat posuere. Phasellus eget erat sit amet lacus semper consectetur. Sed a nisi nisi. Pellentesque hendrerit est id quam facilisis auctor a ut ante. Etiam metus arcu, sagittis vitae massa ac, faucibus tempus dolor. Sed et tempus ex. Aliquam interdum erat vel convallis tristique. Phasellus lectus eros, interdum ac sollicitudin vestibulum, scelerisque vitae ligula. Cras aliquam est id velit laoreet, et mattis massa ultrices. Ut aliquam mi nunc, et tempor neque malesuada in.\n\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce venenatis arcu est. Phasellus vel dignissim tellus. Aenean fermentum fermentum convallis. Maecenas vitae ipsum sed risus viverra volutpat non ac sapien. Donec viverra massa at dolor imperdiet hendrerit. Nullam quis est vitae dui placerat posuere. Phasellus eget erat sit amet lacus semper consectetur. Sed a nisi nisi. Pellentesque hendrerit est id quam facilisis auctor a ut ante. Etiam metus arcu, sagittis vitae massa ac, faucibus tempus dolor. Sed et tempus ex. Aliquam interdum erat vel convallis tristique. Phasellus lectus eros, interdum ac sollicitudin vestibulum, scelerisque vitae ligula. Cras aliquam est id velit laoreet, et mattis massa ultrices. Ut aliquam mi nunc, et tempor neque malesuada in.', 'Lorem ipsum dolor sit amet', NULL, 'www.companyurl.com', '1-10', NULL, NULL, NULL, '12345679', 'JOBPORTAL-1457713172.jpg', NULL, NULL, 'active', NULL, 'mno-company', NULL, NULL, NULL, 'Private'),
(10, 'MNT Comapny', NULL, NULL, 22, 'Private', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce venenatis arcu est. Phasellus vel dignissim tellus. Aenean fermentum fermentum convallis. Maecenas vitae ipsum sed risus viverra volutpat non ac sapien. Donec viverra massa at dolor imperdiet hendrerit. Nullam quis est vitae dui placerat posuere. Phasellus eget erat sit amet lacus semper consectetur. Sed a nisi nisi. Pellentesque hendrerit est id quam facilisis auctor a ut ante. Etiam metus arcu, sagittis vitae massa ac, faucibus tempus dolor. Sed et tempus ex. Aliquam interdum erat vel convallis tristique. Phasellus lectus eros, interdum ac sollicitudin vestibulum, scelerisque vitae ligula. Cras aliquam est id velit laoreet, et mattis massa ultrices. Ut aliquam mi nunc, et tempor neque malesuada in.', 'Aenean fermentum fermentum convallis', NULL, 'www.companyurl.com', '101-300', NULL, NULL, NULL, '123456789', 'JOBPORTAL-1457713426.jpg', NULL, NULL, 'active', NULL, 'mnt-comapny', NULL, NULL, NULL, 'Private'),
(11, 'MNF Comapny', NULL, NULL, 16, 'Private', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce venenatis arcu est. Phasellus vel dignissim tellus. Aenean fermentum fermentum convallis. Maecenas vitae ipsum sed risus viverra volutpat non ac sapien. Donec viverra massa at dolor imperdiet hendrerit. Nullam quis est vitae dui placerat posuere. Phasellus eget erat sit amet lacus semper consectetur. Sed a nisi nisi. Pellentesque hendrerit est id quam facilisis auctor a ut ante. Etiam metus arcu, sagittis vitae massa ac, faucibus tempus dolor. Sed et tempus ex. Aliquam interdum erat vel convallis tristique. Phasellus lectus eros, interdum ac sollicitudin vestibulum, scelerisque vitae ligula. Cras aliquam est id velit laoreet, et mattis massa ultrices. Ut aliquam mi nunc, et tempor neque malesuada in.', 'Pellentesque hendrerit est id quam facilisis', NULL, 'www.companyurl.com', '51-100', NULL, NULL, NULL, '123456789', 'JOBPORTAL-1457713999.jpg', NULL, NULL, 'active', NULL, 'mnf-comapny', NULL, NULL, NULL, 'Private'),
(12, 'QWE Company', NULL, NULL, 18, 'Private', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi a velit sed risus pulvinar faucibus. Nulla facilisi. Nullam vehicula nec ligula eu vulputate. Nunc id ultrices mi, ac tristique lectus. Suspendisse porta ultrices ultricies. Sed quis nisi vel magna maximus aliquam a vel nisl. Cras non rutrum diam. Nulla sed ipsum a felis posuere pharetra ut sit amet augue. Sed id nisl sodales, vulputate mi eu, viverra neque. Fusce fermentum, est ut accumsan accumsan, risus ante varius diam, non venenatis eros ligula fermentum leo. Etiam consectetur imperdiet volutpat. Donec ut pharetra nisi, eget pellentesque tortor. Integer eleifend dolor eu ex lobortis, ac gravida augue tristique. Proin placerat consectetur tincidunt. Nullam sollicitudin, neque eget iaculis ultricies, est justo pulvinar turpis, vulputate convallis leo orci at sapien.\n\nQuisque ac scelerisque libero, nec blandit neque. Nullam felis nisl, elementum eu sapien ut, convallis interdum felis. In turpis odio, fermentum non pulvinar gravida, posuere quis magna. Ut mollis eget neque at euismod. Interdum et malesuada fames ac ante ipsum primis in faucibus. Integer faucibus orci a pulvinar malesuada. Aenean at felis vitae lorem venenatis consequat. Nam non nunc euismod, consequat ligula non, tristique odio. Ut leo sapien, aliquet sed ultricies et, scelerisque quis nulla. Aenean non sapien maximus, convallis eros vitae, iaculis massa. In fringilla hendrerit nisi, eu pellentesque massa faucibus molestie. Etiam laoreet eros quis faucibus rutrum. Quisque eleifend purus justo, eget tempus quam interdum non.', 'Quisque ac scelerisque libero, nec blandit neque', NULL, 'www.companyurl.com', '1-10', NULL, NULL, NULL, '123456789', 'JOBPORTAL-1457768561.jpg', NULL, NULL, 'active', NULL, 'qwe-company', NULL, NULL, NULL, 'Private'),
(13, 'ASD Company', NULL, NULL, 10, 'Private', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi a velit sed risus pulvinar faucibus. Nulla facilisi. Nullam vehicula nec ligula eu vulputate. Nunc id ultrices mi, ac tristique lectus. Suspendisse porta ultrices ultricies. Sed quis nisi vel magna maximus aliquam a vel nisl. Cras non rutrum diam. Nulla sed ipsum a felis posuere pharetra ut sit amet augue. Sed id nisl sodales, vulputate mi eu, viverra neque. Fusce fermentum, est ut accumsan accumsan, risus ante varius diam, non venenatis eros ligula fermentum leo. Etiam consectetur imperdiet volutpat. Donec ut pharetra nisi, eget pellentesque tortor. Integer eleifend dolor eu ex lobortis, ac gravida augue tristique. Proin placerat consectetur tincidunt. Nullam sollicitudin, neque eget iaculis ultricies, est justo pulvinar turpis, vulputate convallis leo orci at sapien.', 'Quisque ac scelerisque libero, nec blandit neque', NULL, 'www.companyurl.com', '1-10', NULL, NULL, NULL, '123456789', 'JOBPORTAL-1457768887.jpg', NULL, NULL, 'active', NULL, 'asd-company', NULL, NULL, NULL, 'Private'),
(14, 'XCV Company', NULL, NULL, 18, 'Private', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi a velit sed risus pulvinar faucibus. Nulla facilisi. Nullam vehicula nec ligula eu vulputate. Nunc id ultrices mi, ac tristique lectus. Suspendisse porta ultrices ultricies. Sed quis nisi vel magna maximus aliquam a vel nisl. Cras non rutrum diam. Nulla sed ipsum a felis posuere pharetra ut sit amet augue. Sed id nisl sodales, vulputate mi eu, viverra neque. Fusce fermentum, est ut accumsan accumsan, risus ante varius diam, non venenatis eros ligula fermentum leo. Etiam consectetur imperdiet volutpat. Donec ut pharetra nisi, eget pellentesque tortor. Integer eleifend dolor eu ex lobortis, ac gravida augue tristique. Proin placerat consectetur tincidunt. Nullam sollicitudin, neque eget iaculis ultricies, est justo pulvinar turpis, vulputate convallis leo orci at sapien.\n\nQuisque ac scelerisque libero, nec blandit neque. Nullam felis nisl, elementum eu sapien ut, convallis interdum felis. In turpis odio, fermentum non pulvinar gravida, posuere quis magna. Ut mollis eget neque at euismod. Interdum et malesuada fames ac ante ipsum primis in faucibus. Integer faucibus orci a pulvinar malesuada. Aenean at felis vitae lorem venenatis consequat. Nam non nunc euismod, consequat ligula non, tristique odio. Ut leo sapien, aliquet sed ultricies et, scelerisque quis nulla. Aenean non sapien maximus, convallis eros vitae, iaculis massa. In fringilla hendrerit nisi, eu pellentesque massa faucibus molestie. Etiam laoreet eros quis faucibus rutrum. Quisque eleifend purus justo, eget tempus quam interdum non.', 'Nullam enim ex, vulputate at ultricies bibendum', NULL, 'www.companyurl.com', '1-10', NULL, NULL, NULL, '123456789', 'JOBPORTAL-1457769102.jpg', NULL, NULL, 'active', NULL, 'xcv-company', NULL, NULL, NULL, 'Private'),
(15, 'Unique Solutions', NULL, NULL, 40, 'Private', 'asdlfjasdfj', 'Narvekar Galli', NULL, 'asdfsd', '1-10', NULL, NULL, NULL, '88888888888', 'JOBPORTAL-1459152055.jpg', NULL, NULL, 'active', NULL, 'unique-solutions', NULL, NULL, NULL, 'Private'),
(16, 'Kacykos', NULL, NULL, 5, 'Private', '234tgfdgh hd hd h dh dvh', 'ssssssssssssaaaaa', NULL, '423523525252', '11-50', NULL, NULL, NULL, '234242525', 'JOBPORTAL-1459158292.jpg', NULL, NULL, 'active', NULL, 'kacykos', NULL, NULL, NULL, 'Public'),
(23, 'Softrait Technologies', NULL, NULL, 22, 'Private', 'rfrfrf', 'frfrf', NULL, 'efrf', '1-10', NULL, NULL, NULL, 'rfrf', 'JOBPORTAL-1459449801.jpg', NULL, NULL, 'active', NULL, 'softrait-technologies', NULL, NULL, NULL, 'Private'),
(18, 'WM Informática', NULL, NULL, 40, 'Private', 'Esta aplicação web é concebida na sua forma actual como um sítio exigindo dinâmicas atualizações constantes tanto dos requerentes, bem como as empresas. No seu conjunto o objetivo do projeto é permitir que os candidatos a emprego a colocar os seus currículos e as empresas a publicar suas vagas.', 'Av. José Bezerra Sobrinho', NULL, 'www.wminfor.com', '11-50', NULL, NULL, NULL, '(81) 3676-1104', 'JOBPORTAL-1459191669.jpg', NULL, NULL, 'active', NULL, 'wm-informtica', NULL, NULL, NULL, 'Public'),
(22, 'dsfsdf', NULL, NULL, 22, 'Private', 'dgdfgd fgdfgdfgdfg dfgdfgdfg dfg dfg dfg dfg dfg', 'sdfsdf sdfsdf sdf sd fsdf fsdf sd fsd f', NULL, 'www.sdfsdf.com', '11-50', NULL, NULL, NULL, '654456456456', 'JOBPORTAL-1459333898.jpg', NULL, NULL, 'active', NULL, 'dsfsdf', NULL, NULL, NULL, 'Semi-Government'),
(20, 'lkinotech', NULL, NULL, 22, 'Private', 'no description', 'panipat', NULL, 'www.lkitech.com', '1-10', NULL, NULL, NULL, '1804020101', 'JOBPORTAL-1459235594.png', NULL, NULL, 'active', NULL, 'lkinotech', NULL, NULL, NULL, 'Private'),
(21, 'dfgsdfg', NULL, NULL, 5, 'Private', 'dfsd erfgerfer erferf', '3434 erfgerge', NULL, 'ass.com', '1-10', NULL, NULL, NULL, '5555555555', 'JOBPORTAL-1459241338.png', NULL, NULL, 'active', NULL, 'dfgsdfg', NULL, NULL, NULL, 'Public'),
(24, 'fdfdf', NULL, NULL, 5, 'Private', 'fdfdfdfdfdf', 'dfdf', NULL, 'dfdf', '51-100', NULL, NULL, NULL, 'fdfdfd', 'JOBPORTAL-1459502829.jpg', NULL, NULL, 'active', NULL, 'fdfdf', NULL, NULL, NULL, 'Private'),
(25, 'Master Tech', NULL, NULL, 10, 'Private', 'mmmmm', 'dha', NULL, 'www.mastertech.com.bd', '1-10', NULL, NULL, NULL, '878755', 'JOBPORTAL-1459553924.png', NULL, NULL, 'active', NULL, 'master-tech', NULL, NULL, NULL, 'Private'),
(26, 'TechHub', NULL, NULL, 3, 'Private', 'TechHub.com', 'Test Address', NULL, 'TechHub.com', '11-50', NULL, NULL, NULL, '001988734344', 'JOBPORTAL-1459583469.jpg', NULL, NULL, 'active', NULL, 'techhub', NULL, NULL, NULL, 'Public'),
(27, 'PEOPLES WEB INNOVATIONS', NULL, NULL, 22, 'Private', 'nothing just for test', 'india', NULL, 'www.wallofindia.com', '1-10', NULL, NULL, NULL, '677775888578', 'JOBPORTAL-1459593780.jpg', NULL, NULL, 'active', NULL, 'peoples-web-innovations', NULL, NULL, NULL, 'Private'),
(28, 'ksksk', NULL, NULL, 10, 'Private', 'jdjdjdj iziziz sjsjsj', 'dkdkdkdkdk', NULL, 'https://mail.google.com/mail/u/0/#inbox', '301-600', NULL, NULL, NULL, '2°2020202020', 'JOBPORTAL-1459634581.jpg', NULL, NULL, 'active', NULL, 'ksksk', NULL, NULL, NULL, 'Public'),
(29, 'sdfsafd', NULL, NULL, 3, 'Private', 'safasfd', 'sdfsdfas', NULL, 'dsffsafas', '51-100', NULL, NULL, NULL, '4343', 'JOBPORTAL-1459679795.jpg', NULL, NULL, 'active', NULL, 'sdfsafd', NULL, NULL, NULL, 'Public'),
(30, 'Motto', NULL, NULL, 40, 'Private', 'sjdfksjdfksjd nfksjdfnd', 'sdf sdf sdf s', NULL, 'http://mottoiletisim.com', '51-100', NULL, NULL, NULL, '5342029597', 'JOBPORTAL-1459784884.jpg', NULL, NULL, 'active', NULL, 'motto', NULL, NULL, NULL, 'Private'),
(31, 'Art Zeus', NULL, NULL, 16, 'Private', 'Teste', 'Teste', NULL, '21996265039', '11-50', NULL, NULL, NULL, 'Álan', 'JOBPORTAL-1459879029.jpg', NULL, NULL, 'active', NULL, 'art-zeus', NULL, NULL, NULL, 'Public'),
(32, 'smartdev', NULL, NULL, 5, 'Private', 'rsearas', 'rsaras', NULL, 'resar.ge', '1501-2000', NULL, NULL, NULL, 'r2313', 'JOBPORTAL-1459948962.png', NULL, NULL, 'active', NULL, 'smartdev', NULL, NULL, NULL, 'Public'),
(33, 'koko jk', NULL, NULL, 5, 'Private', 'ok', 'BP 253', NULL, 'ok', '1-10', NULL, NULL, NULL, '21548854', 'JOBPORTAL-1460013565.png', NULL, NULL, 'active', NULL, 'koko-jk', NULL, NULL, NULL, 'Private'),
(34, 'sdggsd', NULL, NULL, 5, 'Private', 'gsddgs dsggdsgdsd', 'sdgsdg', NULL, 'fsdsdsggds.pl', '1-10', NULL, NULL, NULL, '22424', 'JOBPORTAL-1460071669.png', NULL, NULL, 'active', NULL, 'sdggsd', NULL, NULL, NULL, 'Public'),
(35, 'dsfsfds', NULL, NULL, 10, 'Private', 'fdgdfgd', 'efvaesfesfwef', NULL, 'fdg.com', '1-10', NULL, NULL, NULL, '23424232', 'JOBPORTAL-1460132506.jpg', NULL, NULL, 'active', NULL, 'dsfsfds', NULL, NULL, NULL, 'Private'),
(36, 'Azienda Alpha', NULL, NULL, 22, 'Private', 'test', 'VIa Zurigo, Lugano', NULL, 'www.test.com', '1-10', NULL, NULL, NULL, '111', 'JOBPORTAL-1460229473.jpg', NULL, NULL, 'active', NULL, 'azienda-alpha', NULL, NULL, NULL, 'Private'),
(37, 'asdasdasd', NULL, NULL, 3, 'Private', 'asdasdasdasd', 'sdfsdfsdf', NULL, 'wfsdsdf.com', '1-10', NULL, NULL, NULL, '345345', 'JOBPORTAL-1460425365.png', NULL, NULL, 'active', NULL, 'asdasdasd', NULL, NULL, NULL, 'Private'),
(38, 'uptech', NULL, NULL, 40, 'Private', 'dasdasd', 'asdasdasd', NULL, 'http://asd.asd', '1-10', NULL, NULL, NULL, '4324234234', 'JOBPORTAL-1460530406.png', NULL, NULL, 'active', NULL, 'uptech', NULL, NULL, NULL, 'Private'),
(39, 'BILMA', NULL, NULL, 3, 'Private', 'qdqfd', 'Abdulnasir', NULL, 'hiiraan.com', '51-100', NULL, NULL, NULL, '02525', 'JOBPORTAL-1460625717.jpg', NULL, NULL, 'active', NULL, 'bilma', NULL, NULL, NULL, 'Public'),
(40, 'xyz', NULL, NULL, 22, 'Private', 'rrreytryy', 'dsdfsdf', NULL, 'www.xyz.com', '51-100', NULL, NULL, NULL, '543444', 'JOBPORTAL-1460700378.jpg', NULL, NULL, 'active', NULL, 'xyz', NULL, NULL, NULL, 'Private'),
(41, 'sparx', NULL, NULL, 22, 'Private', 'hello', 'delhi', NULL, 'spar', '1-10', NULL, NULL, NULL, '9874561230', 'JOBPORTAL-1460721976.jpeg', NULL, NULL, 'active', NULL, 'sparx', NULL, NULL, NULL, 'Public'),
(42, 'Gri Telekom', NULL, NULL, 3, 'Private', 'deneme', 'istanbul', NULL, 'http://www.gritelekom.com', '1-10', NULL, NULL, NULL, '905055050505', 'JOBPORTAL-1460804749.jpg', NULL, NULL, 'active', NULL, 'gri-telekom', NULL, NULL, NULL, 'Private'),
(43, 'Paxten', NULL, NULL, 22, 'Private', 'asadasdsd', 'asdasdas', NULL, 'www.paxten.in', '1-10', NULL, NULL, NULL, '99999999999', 'JOBPORTAL-1460975531.jpg', NULL, NULL, 'active', NULL, 'paxten', NULL, NULL, NULL, 'Private'),
(44, 'start designs', NULL, NULL, 16, 'Private', 'test', '8/344 vidhyadhar nagar', NULL, 'www.webismyworld789@gmail.com', '1-10', NULL, NULL, NULL, '08003366789', 'JOBPORTAL-1461332142.jpg', NULL, NULL, 'active', NULL, 'start-designs', NULL, NULL, NULL, 'Public'),
(45, 'tar? bili?im hizmetleri', NULL, NULL, 7, 'Private', 'Job portal is developed for creating an interactive job vacancy for candidates. This web application is to be conceived in its current form as a dynamic site-requiring constant updates both from the seekers as well as the companies. On the whole the objective of the project is to enable jobseekers to place their resumes and companies to publish their vacancies. It enables jobseekers to post their resume, search for jobs, view personal job listings. It will provide various companies to place their vacancy profile on the site and also have an option to search candidate resumes. Apart from this there will be an admin module for the customer to make changes to the database content. It consists of 4 modules:', '5714 37 göztepe', NULL, 'www.deneme.com', '301-600', NULL, NULL, NULL, '0212530152365', 'JOBPORTAL-1461495364.png', NULL, NULL, 'active', NULL, 'tar-biliim-hizmetleri', NULL, NULL, NULL, 'Private'),
(46, 'ewwfwe', NULL, NULL, 40, 'Private', 'sfddssda', 'wweewcs evrve', NULL, 'scdc.scs', '1-10', NULL, NULL, NULL, '561656810596', 'JOBPORTAL-1461611130.png', NULL, NULL, 'active', NULL, 'ewwfwe', NULL, NULL, NULL, 'Public'),
(47, 'fonseca', NULL, NULL, 3, 'Private', 'afafff', 'av joao', NULL, 'fonseca.com.br', '11-50', NULL, NULL, NULL, '21999754478', 'JOBPORTAL-1461764354.jpg', NULL, NULL, 'active', NULL, 'fonseca', NULL, NULL, NULL, 'Public'),
(48, 'avrupa reklam', NULL, NULL, 7, 'Private', 'asdfdsfa', 'Beyazevler', NULL, 'www.avrupareklam.com.tr', '1-10', NULL, NULL, NULL, '5325858854', 'JOBPORTAL-1461950231.jpg', NULL, NULL, 'active', NULL, 'avrupa-reklam', NULL, NULL, NULL, 'Private'),
(49, 'Handhome', NULL, NULL, 35, 'Private', 'Handhome.net là m?t n?n t?ng chia s? thông tin ki?n trúc, n?i th?t t?i Vi?t Nam', '52 Nguyen Thai H?c, Ph??ng ?i?n Biên, Qu?n Ba ?ình, Hà N?i', NULL, 'http://handhome.net', '1-10', NULL, NULL, NULL, '12345678', 'JOBPORTAL-1462009695.png', NULL, NULL, 'active', NULL, 'handhome', NULL, NULL, NULL, 'Public'),
(50, 'GBBS-IT', NULL, NULL, 5, 'Private', 'bonjour', 'lotissement El Salem 3 N° 4 Tahar bouchet Birkhadem Alger', NULL, 'www.gbbs-it.com', '1-10', NULL, NULL, NULL, '+213550193126', 'JOBPORTAL-1462047950.png', NULL, NULL, 'active', NULL, 'gbbsit', NULL, NULL, NULL, 'Private'),
(51, 'AMAM', NULL, NULL, 5, 'Private', 'khjg', 'Villa 42 pins maritime', NULL, 'GBBS-IT', '11-50', NULL, NULL, NULL, '+213550193126', 'JOBPORTAL-1462048253.png', NULL, NULL, 'active', NULL, 'amam', NULL, NULL, NULL, 'Private'),
(52, 'Confédération Algérienne du Patronat', NULL, NULL, 16, 'Private', 'test', 'Hôtel El Aurassi Niveau C Bureau n°7', NULL, 'www.marwendzc.com', '1-10', NULL, NULL, NULL, '670119423', 'JOBPORTAL-1462049084.png', NULL, NULL, 'active', NULL, 'confdration-algrienne-du-patronat', NULL, NULL, NULL, 'Public'),
(53, 'netzen', NULL, NULL, 7, 'Private', 'sadasdsd', 'içerenköy', NULL, 'www.company.com', '1-10', NULL, NULL, NULL, '5324475202', 'JOBPORTAL-1462141216.jpg', NULL, NULL, 'active', NULL, 'netzen', NULL, NULL, NULL, 'Private'),
(54, 'the testing company', NULL, NULL, 5, 'Private', 'adas askd jaskd askd jaks nfasdm,fsad fsdma fname fjsadb fsdfa basdf', 'dfgdfg', NULL, 'www.testincompany.com', '11-50', NULL, NULL, NULL, '564123231', 'JOBPORTAL-1462172312.png', NULL, NULL, 'active', NULL, 'the-testing-company', NULL, NULL, NULL, 'Public'),
(55, 'hkyhjgjgjhgkjh', NULL, NULL, 3, 'Private', 'gszdfszdfsdf', 'kjghkj,hkhjk', NULL, 'www.lhjkkjh.com', '1-10', NULL, NULL, NULL, '78768767567', 'JOBPORTAL-1462474003.jpg', NULL, NULL, 'active', NULL, 'hkyhjgjgjhgkjh', NULL, NULL, NULL, 'Private'),
(56, 'whitehouse dental practice', NULL, NULL, 3, 'Private', 'sdasdasdasdasd', '18 thirlmere drive', NULL, 'asdadad', '11-50', NULL, NULL, NULL, '7495996849', 'JOBPORTAL-1462541062.jpg', NULL, NULL, 'active', NULL, 'whitehouse-dental-practice', NULL, NULL, NULL, 'Private'),
(57, 'bobbbbbbbbbb', NULL, NULL, 3, 'Private', 'zzzzzzzzzzzzzzzzzzzzzzzz', 'zzzzzzzzzzzzzzzzzzzzzzzzzzzz', NULL, 'http://rtrtr.cc', '1-10', NULL, NULL, NULL, '11111111111111111111', 'JOBPORTAL-1462549881.jpg', NULL, NULL, 'active', NULL, 'bobbbbbbbbbb', NULL, NULL, NULL, 'Private'),
(58, 'teste test', NULL, NULL, 3, 'Private', 'teste test', 'teste test', NULL, 'teste.com', '11-50', NULL, NULL, NULL, '3456798789', 'JOBPORTAL-1462712382.png', NULL, NULL, 'active', NULL, 'teste-test', NULL, NULL, NULL, 'Private'),
(59, 'Test', NULL, NULL, 22, 'Private', 'IT', 'NY', NULL, 'codeareena.com', '1-10', NULL, NULL, NULL, '313444', 'JOBPORTAL-1462948682.png', NULL, NULL, 'active', NULL, 'test', NULL, NULL, NULL, 'Private'),
(60, 'In', NULL, NULL, 22, 'Private', 'dadwedew', 'India', NULL, 'ddsdwfefde', '1-10', NULL, NULL, NULL, '2222222222', 'JOBPORTAL-1463032113.jpg', NULL, NULL, 'active', NULL, 'in', NULL, NULL, NULL, 'Private'),
(61, 'cinestation', NULL, NULL, 22, 'Private', 'it sowftware', 'hyderabad', NULL, 'cinestation.in', '1-10', NULL, NULL, NULL, '8187030758', 'JOBPORTAL-1463114643.jpg', NULL, NULL, 'active', NULL, 'cinestation', NULL, NULL, NULL, 'Private'),
(62, 'Softgators Tech Pvt Ltd', NULL, NULL, 22, 'Private', 'asdasddasd', 'D4/26 Vashisth Park', NULL, 'softgators.com', '1-10', NULL, NULL, NULL, '9015845820', 'JOBPORTAL-1463156147.jpg', NULL, NULL, 'active', NULL, 'softgators-tech-pvt-ltd', NULL, NULL, NULL, 'Private'),
(63, 'TEST', NULL, NULL, 35, 'Private', 'SFSDFSDF SD FSD FSDF S', '3', NULL, 'www.testincompany.com', '11-50', NULL, NULL, NULL, '1118675198', 'JOBPORTAL-1463165183.jpg', NULL, NULL, 'active', NULL, 'test-1463165183', NULL, NULL, NULL, 'Private'),
(64, 'nib technology company', NULL, NULL, 3, 'Private', 'our company is our company is our company is our company is our company is our company is our company is our company is our company is our company is our company is our company is our company is our company is our company is our company is', 'addis ababa', NULL, 'www.websiste.com', '1-10', NULL, NULL, NULL, '+251911996922', 'JOBPORTAL-1463401978.png', NULL, NULL, 'active', NULL, 'nib-technology-company', NULL, NULL, NULL, 'Public'),
(65, 'sadasdsad', NULL, NULL, 3, 'Private', 'gfdgfdgdfg', 'sad', NULL, 'fdgfdgfd', '1-10', NULL, NULL, NULL, 'asdsad', 'JOBPORTAL-1463524284.jpg', NULL, NULL, 'active', NULL, 'sadasdsad', NULL, NULL, NULL, 'Public'),
(66, 'Temp Corp', NULL, NULL, 5, 'Private', 'Temporary company', 'Temporary Street, 1234', NULL, 'www.www.www', '1-10', NULL, NULL, NULL, '111111111', 'JOBPORTAL-1463599153.png', NULL, NULL, 'active', NULL, 'temp-corp', NULL, NULL, NULL, 'Private'),
(67, 'sksks', NULL, NULL, 5, 'Private', 'ksskskskks', 'kdkdkd', NULL, 'http://codeareena.com/jobportal/employer-signup', '1-10', NULL, NULL, NULL, 'ododod', 'JOBPORTAL-1463842511.jpg', NULL, NULL, 'active', NULL, 'sksks', NULL, NULL, NULL, 'Private'),
(68, 'Pakajs', NULL, NULL, 7, 'Private', 'Dsss', 'Snsnhssh', NULL, 'Web', '1-10', NULL, NULL, NULL, '01029883355', 'JOBPORTAL-1464008445.png', NULL, NULL, 'active', NULL, 'pakajs', NULL, NULL, NULL, 'Semi-Government'),
(69, 'ver', NULL, NULL, 35, 'Private', 'dfsdf sdfsdfas', 'ERZ?NCAN AÇIK CEZA EV?', NULL, 'www.sairyazar.com', '11-50', NULL, NULL, NULL, '5443333292', 'JOBPORTAL-1464183007.jpg', NULL, NULL, 'active', NULL, 'ver', NULL, NULL, NULL, 'Public'),
(70, 'ddddd', NULL, NULL, 22, 'Private', 'cxxcxcxcxxcxcx', 'fgfgfgf', NULL, 'www.vvv.com', '11-50', NULL, NULL, NULL, '123456789', 'JOBPORTAL-1464204378.jpg', NULL, NULL, 'active', NULL, 'ddddd', NULL, NULL, NULL, 'Private'),
(71, 'rkparmar', NULL, NULL, 3, 'Private', '2342', 'tresr', NULL, 'gjkhi', '1-10', NULL, NULL, NULL, '556969', 'JOBPORTAL-1464259083.jpg', NULL, NULL, 'active', NULL, 'rkparmar', NULL, NULL, NULL, 'Public'),
(72, 'ivaluelabs', NULL, NULL, 3, 'Private', 'fwqerfdsfasdfasdfasdfasdfasdfasdf', 'turrialba costa rica', NULL, 'www.leo.com', '1-10', NULL, NULL, NULL, '234523452345', 'JOBPORTAL-1464315440.png', NULL, NULL, 'active', NULL, 'ivaluelabs', NULL, NULL, NULL, 'Public'),
(73, 'ugjntechnoly', NULL, NULL, 22, 'Private', 'sasA', 'sasa', NULL, 'ugjntechnology.com', '51-100', NULL, NULL, NULL, '9971562879', 'JOBPORTAL-1464347546.png', NULL, NULL, 'active', NULL, 'ugjntechnoly', NULL, NULL, NULL, 'Public'),
(74, 'Onion Smart Solutions', NULL, NULL, 22, 'Private', 'Software Development Company', '369-00100 Nairobi', NULL, 'www.onion.co.ke', '101-300', NULL, NULL, NULL, '0718199017', 'JOBPORTAL-1464458862.png', NULL, NULL, 'active', NULL, 'onion-smart-solutions', NULL, NULL, NULL, 'Private'),
(75, 'fddsfsdf', NULL, NULL, 7, 'Private', 'dsfsdfsdf', 'dsfsdf', NULL, 'sdfsdf', '11-50', NULL, NULL, NULL, '066666666666', 'JOBPORTAL-1464470540.gif', NULL, NULL, 'active', NULL, 'fddsfsdf', NULL, NULL, NULL, 'Government'),
(76, 'rrrr', NULL, NULL, 5, 'Private', 'effsdfdsdgdfg', 'wedsafadsf', NULL, 'neuronswork.com/', '1-10', NULL, NULL, NULL, 'dfdfdf', 'JOBPORTAL-1464734965.jpg', NULL, NULL, 'active', NULL, 'rrrr', NULL, NULL, NULL, 'Government'),
(77, 'Patel', NULL, NULL, 18, 'Private', 'dsfsd', 'dsfsdf', NULL, 'www.google.com', '1-10', NULL, NULL, NULL, '4353453', 'JOBPORTAL-1464966601.png', NULL, NULL, 'active', NULL, 'patel', NULL, NULL, NULL, 'Private'),
(78, 'aaj mid', NULL, NULL, 16, 'Private', '05000', 'dubai', NULL, '05000', '11-50', NULL, NULL, NULL, '05000', 'JOBPORTAL-1465349326.jpg', NULL, NULL, 'active', NULL, 'aaj-mid', NULL, NULL, NULL, 'Public'),
(79, 'the koko', NULL, NULL, 16, 'Private', 'deals with web design of any sort and develpo apps for companies', '4B shopping mall', NULL, 'thekoko.com', '1-10', NULL, NULL, NULL, '01678905', 'JOBPORTAL-1465479729.jpg', NULL, NULL, 'active', NULL, 'the-koko', NULL, NULL, NULL, 'Private'),
(80, 'test', NULL, NULL, 35, 'Private', 'test', 'test', NULL, 'test.nl', '1-10', NULL, NULL, NULL, 'test', 'JOBPORTAL-1465913974.jpg', NULL, NULL, 'active', NULL, 'test-1465913974', NULL, NULL, NULL, 'Private'),
(82, 'HEC', NULL, NULL, 16, 'Private', 'very good', 'ranchi', NULL, 'HEC.nic.in', '51-100', NULL, NULL, NULL, '2565552', 'JOBPORTAL-1597380673.jpg', NULL, NULL, 'active', NULL, 'hec', NULL, NULL, NULL, 'Private'),
(90, 'getJob.com', NULL, NULL, 3, 'Private', 'ok', 'DHN', NULL, 'getjob.com', '1-10', NULL, NULL, NULL, '256+65', 'JOBPORTAL-1599048605.JPG', NULL, NULL, 'active', NULL, 'getjobcom', NULL, NULL, NULL, 'Private'),
(91, 'techemp', NULL, NULL, 40, 'Private', 'ok', 'dhn', NULL, 'techemp.in', '101-300', NULL, NULL, NULL, '2356555', 'JOBPORTAL-1599106236.JPG', NULL, NULL, 'active', NULL, 'techemp', NULL, NULL, NULL, 'Private'),
(92, 'demo', NULL, NULL, 5, 'Private', 'ghyttrdevgfdgfdgdg', 'dhn', NULL, 'demo.nic.in', '101-300', NULL, NULL, NULL, '25855865956', 'JOBPORTAL-1599115987.jpg', NULL, NULL, 'active', NULL, 'demo', NULL, NULL, NULL, 'Private'),
(93, 'VK Comm HUB', NULL, NULL, 1, 'Private', 'Products Sells Man and Products assembly Products Repaire and replacement Products', 'VK Comm HUB Dhanbad', NULL, 'vkcoomhub.com', '1-10', NULL, NULL, NULL, '0326-000000', 'JOBPORTAL-1599200270.jpg', NULL, NULL, 'active', NULL, 'vk-comm-hub', NULL, NULL, NULL, 'Private');

-- --------------------------------------------------------

--
-- Table structure for table `pp_countries`
--

CREATE TABLE `pp_countries` (
  `ID` int(11) NOT NULL,
  `country_name` varchar(150) NOT NULL DEFAULT '',
  `country_citizen` varchar(150) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pp_countries`
--

INSERT INTO `pp_countries` (`ID`, `country_name`, `country_citizen`) VALUES
(9, 'India', 'Indian');

-- --------------------------------------------------------

--
-- Table structure for table `pp_email_content`
--

CREATE TABLE `pp_email_content` (
  `ID` int(11) NOT NULL,
  `email_name` varchar(155) DEFAULT NULL,
  `from_name` varchar(155) DEFAULT NULL,
  `content` text DEFAULT NULL,
  `from_email` varchar(90) DEFAULT NULL,
  `subject` varchar(155) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pp_email_content`
--

INSERT INTO `pp_email_content` (`ID`, `email_name`, `from_name`, `content`, `from_email`, `subject`) VALUES
(1, 'Forgot Password', 'MNO Jobs', '<style type=\"text/css\">\n				.txt {\n						font-family: Arial, Helvetica, sans-serif;\n						font-size: 13px; color:#000000;\n					}\n				</style>\n<p class=\"txt\">Thank you  for contacting Member Support. Your account information is listed below: </p>\n<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"600\" class=\"txt\">\n  <tr>\n    <td width=\"17\" height=\"19\"><p>&nbsp;</p></td>\n    <td width=\"159\" height=\"25\" align=\"right\"><strong>Login Page:&nbsp;&nbsp;</strong></td>\n    <td width=\"424\" align=\"left\"><a href=\"{SITE_URL}/login\">{SITE_URL}/login</a></td>\n  </tr>\n  <tr>\n    <td height=\"19\">&nbsp;</td>\n    <td height=\"25\" align=\"right\"><strong>Your Username:&nbsp;&nbsp;</strong></td>\n    <td align=\"left\">{USERNAME}</td>\n  </tr>\n  <tr>\n    <td height=\"19\"><p>&nbsp;</p></td>\n    <td height=\"25\" align=\"right\"><strong>Your Password:&nbsp;&nbsp;</strong></td>\n    <td align=\"left\">{PASSWORD}</td>\n  </tr>\n</table>\n\n<p class=\"txt\">Thank you,</p>', 'service@jobportalbeta.com', 'Password Recovery'),
(2, 'Jobseeker Signup', 'Jobseeker Signup Successful', '<style type=\"text/css\">p {font-family: Arial, Helvetica, sans-serif; font-size: 13px; color:#000000;}</style>\n\n  <p>{JOBSEEKER_NAME}:</p>\n  <p>Thank you for joining us. Please note your profile details for future record.</p>\n  <p>Username: {USERNAME}<br>\n    Password: {PASSWORD}</p>\n  \n  <p>Regards</p>', 'service@jobportalbeta.com', 'Jobs website'),
(3, 'Employer signs up', 'Employer Signup Successful', '<style type=\"text/css\">p {font-family: Arial, Helvetica, sans-serif; font-size: 13px; color:#000000;}</style>\n\n  <p>{EMPLOYER_NAME}</p>\n  <p>Thank you for joining us. Please note your profile details for future record.</p>\n  <p>Username: {USERNAME}<br>\n    Password: {PASSWORD}</p>\n  <p>Regards</p>', 'service@jobportalbeta.com', 'Jobs website'),
(4, 'New job is posted by Employer', 'New Job Posted', '<style type=\"text/css\">p {font-family: Arial, Helvetica, sans-serif; font-size: 13px; color:#000000;}</style>\n\n  <p>{JOBSEEKER_NAME},</p>\n  <p>We would like to inform  that a new job has been posted on our website that may be of your interest.</p>\n  <p>Please visit the  following link to review and apply:</p>\n <p>{JOB_LINK}</p>\n  <p>Regards,</p>', 'service@jobportalbeta.com', 'New {JOB_CATEGORY}'),
(5, 'Apply Job', 'Job Application', 'new job', 'ravi.rdx9122@gmail.com', 'New Job'),
(6, 'Job Activation Email', 'Job Activated', 'new job in dhanbad', 'ravi.rdx9122@gmail.com', 'New job'),
(7, 'Send Message To Candidate', '{EMPLOYER_NAME}', '<style type=\"text/css\">p {font-family: Arial, Helvetica, sans-serif; font-size: 13px; color:#000000;}</style>\r\n  <p>Hi {JOBSEEKER_NAME}:</p>\r\n  <p>A new message has been posted for you by :  {COMPANY_NAME}.</p>\r\n  <p>Message:</p>\r\n  <p>{MESSAGE}</p>\r\n  <p>You may review this company by going to: {COMPANY_PROFILE_LINK} to company profile.</p>\r\n  \r\n  <p>Regards,</p>', '{EMPLOYER_EMAIL}', 'New message for you'),
(8, 'Scam Alert', '{JOBSEEKER_NAME}', 'bla bla bla', '{JOBSEEKER_EMAIL}', 'Company reported');

-- --------------------------------------------------------

--
-- Table structure for table `pp_employers`
--

CREATE TABLE `pp_employers` (
  `ID` int(11) NOT NULL,
  `company_ID` int(6) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `pass_code` varchar(100) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `mobile_phone` varchar(30) NOT NULL DEFAULT '',
  `gender` enum('female','male') DEFAULT NULL,
  `dated` date NOT NULL,
  `sts` enum('blocked','pending','active') NOT NULL DEFAULT 'active',
  `dob` date DEFAULT NULL,
  `home_phone` varchar(30) DEFAULT NULL,
  `verification_code` varchar(155) DEFAULT NULL,
  `first_login_date` datetime DEFAULT NULL,
  `last_login_date` datetime DEFAULT NULL,
  `ip_address` varchar(40) DEFAULT NULL,
  `old_emp_id` int(11) DEFAULT NULL,
  `flag` varchar(10) DEFAULT NULL,
  `present_address` varchar(155) DEFAULT NULL,
  `top_employer` enum('no','yes') DEFAULT 'no'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pp_employers`
--

INSERT INTO `pp_employers` (`ID`, `company_ID`, `email`, `pass_code`, `first_name`, `last_name`, `country`, `city`, `mobile_phone`, `gender`, `dated`, `sts`, `dob`, `home_phone`, `verification_code`, `first_login_date`, `last_login_date`, `ip_address`, `old_emp_id`, `flag`, `present_address`, `top_employer`) VALUES
(88, 88, 'abcdefg@gmail.com', 'cc03e747a6afbbcbf8be7668acfebee5', 'ABCD', NULL, 'India', 'dfh', '3434634636', NULL, '2020-09-02', 'active', NULL, '0', NULL, NULL, NULL, '::1', NULL, NULL, NULL, 'no'),
(84, 84, 'kundan123@demo.com', 'e10adc3949ba59abbe56e057f20f883e', 'kundan', NULL, 'india', 'Jhar', '8969436699', NULL, '0000-00-00', 'active', NULL, '0', NULL, '2020-08-20 16:34:43', '2020-08-27 11:53:31', '::1', NULL, NULL, NULL, 'no'),
(85, 85, 'jsscexam@demo.com', '670b14728ad9902aecba32e22fa4f6bd', 'student', NULL, 'india', 'jharkhand', '25642', NULL, '2020-08-25', 'active', NULL, '0', NULL, NULL, NULL, '::1', NULL, NULL, NULL, 'no'),
(86, 86, 'it_dhn@demo.com', '670b14728ad9902aecba32e22fa4f6bd', 'Student', NULL, 'india', 'jharkhand', '25452452', NULL, '0000-00-00', 'active', NULL, '0', NULL, '2020-08-26 11:14:37', '2020-08-26 12:48:32', '::1', NULL, NULL, NULL, 'no'),
(87, 87, 'ravi.rdx9122@gmail.com', 'e3ceb5881a0a1fdaad01296d7554868d', 'Larsen Toubro', NULL, 'india', 'jharkhand', '0000000000', NULL, '0000-00-00', 'active', NULL, '0', NULL, '2020-08-26 14:18:29', '2020-08-27 09:48:34', '::1', NULL, NULL, NULL, 'no'),
(89, 89, 'jobManager@gmail.com', '670b14728ad9902aecba32e22fa4f6bd', 'Manager', NULL, 'India', 'DHn', '1324614654', NULL, '2020-09-02', 'active', NULL, '0', NULL, '2020-09-02 15:57:42', '2020-09-02 17:53:37', '::1', NULL, NULL, NULL, 'no'),
(90, 90, 'job@gmail.com', '670b14728ad9902aecba32e22fa4f6bd', 'job', NULL, 'India', 'DHN', '32564', NULL, '2020-09-02', 'active', NULL, '0', NULL, '2020-09-03 09:30:02', '2020-09-03 14:58:40', '::1', NULL, NULL, NULL, 'no'),
(91, 91, 'employee@demo.com', 'ff92a240d11b05ebd392348c35f781b2', 'employee', NULL, 'India', 'dhn', '22225', NULL, '2020-09-03', 'active', NULL, '0', NULL, NULL, NULL, '::1', NULL, NULL, NULL, 'no'),
(92, 92, 'demo@demo.com', '670b14728ad9902aecba32e22fa4f6bd', 'demo', NULL, 'India', 'dhn', '255525', NULL, '2020-09-03', 'active', NULL, '0', NULL, NULL, NULL, '::1', NULL, NULL, NULL, 'no'),
(93, 93, 'rawani7677565704@gmail.com', '0ce7afd58dc24c0667178f227c6fbcaf', 'Vijay Kumar', NULL, 'India', 'Dhanbad', '7677565704', NULL, '2020-09-04', 'active', NULL, '0', NULL, NULL, NULL, '::1', NULL, NULL, NULL, 'no');

-- --------------------------------------------------------

--
-- Table structure for table `pp_favourite_candidates`
--

CREATE TABLE `pp_favourite_candidates` (
  `employer_id` int(11) NOT NULL,
  `seekerid` int(11) DEFAULT NULL,
  `employerlogin` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pp_favourite_companies`
--

CREATE TABLE `pp_favourite_companies` (
  `seekerid` int(11) NOT NULL,
  `companyid` int(11) DEFAULT NULL,
  `seekerlogin` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pp_institute`
--

CREATE TABLE `pp_institute` (
  `ID` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `sts` enum('blocked','active') DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pp_job_alert`
--

CREATE TABLE `pp_job_alert` (
  `ID` int(11) NOT NULL,
  `job_ID` int(11) DEFAULT NULL,
  `dated` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pp_job_alert_queue`
--

CREATE TABLE `pp_job_alert_queue` (
  `ID` int(11) NOT NULL,
  `seeker_ID` int(11) DEFAULT NULL,
  `job_ID` int(11) DEFAULT NULL,
  `dated` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pp_job_functional_areas`
--

CREATE TABLE `pp_job_functional_areas` (
  `ID` int(7) NOT NULL,
  `industry_ID` int(7) DEFAULT NULL,
  `functional_area` varchar(155) DEFAULT NULL,
  `sts` enum('suspended','active') DEFAULT 'active'
) ENGINE=MyISAM DEFAULT CHARSET=latin1 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Table structure for table `pp_job_industries`
--

CREATE TABLE `pp_job_industries` (
  `ID` int(11) NOT NULL,
  `industry_name` varchar(155) DEFAULT NULL,
  `slug` varchar(155) DEFAULT NULL,
  `sts` enum('suspended','active') DEFAULT 'active',
  `top_category` enum('no','yes') DEFAULT 'no'
) ENGINE=MyISAM DEFAULT CHARSET=latin1 PACK_KEYS=0;

--
-- Dumping data for table `pp_job_industries`
--

INSERT INTO `pp_job_industries` (`ID`, `industry_name`, `slug`, `sts`, `top_category`) VALUES
(3, 'Accounts', 'accounts', 'active', 'yes'),
(5, 'Advertising', 'advertising', 'active', 'yes'),
(7, 'Banking', 'banking', 'active', 'yes'),
(10, 'Customer Service', 'customer-service', 'active', 'yes'),
(16, 'Graphic / Web Design', 'graphic-web-design', 'active', 'yes'),
(18, 'HR / Industrial Relations', 'hr-industrial-relations', 'active', 'yes'),
(22, 'IT - Software', 'it-software', 'active', 'yes'),
(35, 'Teaching / Education', 'teaching-education', 'active', 'yes'),
(40, 'IT - Hardware', 'it-hardware', 'active', 'yes'),
(1, 'other', 'other', 'active', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `pp_job_seekers`
--

CREATE TABLE `pp_job_seekers` (
  `ID` int(11) NOT NULL,
  `first_name` varchar(30) DEFAULT NULL,
  `last_name` varchar(30) DEFAULT NULL,
  `email` varchar(155) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `present_address` varchar(255) DEFAULT NULL,
  `permanent_address` varchar(255) DEFAULT NULL,
  `dated` datetime NOT NULL,
  `country` varchar(50) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `gender` enum('female','male') DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `photo` varchar(100) DEFAULT NULL,
  `default_cv_id` int(11) NOT NULL,
  `mobile` varchar(30) DEFAULT NULL,
  `home_phone` varchar(25) DEFAULT NULL,
  `cnic` varchar(255) DEFAULT NULL,
  `nationality` varchar(50) DEFAULT NULL,
  `career_objective` text DEFAULT NULL,
  `sts` enum('active','blocked','pending') NOT NULL DEFAULT 'active',
  `verification_code` varchar(155) DEFAULT NULL,
  `first_login_date` datetime DEFAULT NULL,
  `last_login_date` datetime DEFAULT NULL,
  `slug` varchar(155) DEFAULT NULL,
  `ip_address` varchar(40) DEFAULT NULL,
  `old_id` int(11) DEFAULT NULL,
  `flag` varchar(10) DEFAULT NULL,
  `queue_email_sts` tinyint(1) DEFAULT NULL,
  `send_job_alert` enum('no','yes') DEFAULT 'yes',
  `ph` varchar(12) CHARACTER SET cp866 COLLATE cp866_nopad_bin DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pp_job_seekers`
--

INSERT INTO `pp_job_seekers` (`ID`, `first_name`, `last_name`, `email`, `password`, `present_address`, `permanent_address`, `dated`, `country`, `city`, `gender`, `category`, `dob`, `phone`, `photo`, `default_cv_id`, `mobile`, `home_phone`, `cnic`, `nationality`, `career_objective`, `sts`, `verification_code`, `first_login_date`, `last_login_date`, `slug`, `ip_address`, `old_id`, `flag`, `queue_email_sts`, `send_job_alert`, `ph`) VALUES
(244, 'dfgdfg', '', 'fghgf@fghj.com', 'e807f1fcf82d132f9bb018ca6738a19f', 'hfghdhh', NULL, '2020-09-07 16:16:34', 'India', 'sgssg', 'male', 'General', '1980-08-04', NULL, NULL, 0, '9876543210', 'sfgsdfg', NULL, 'iNDIAN', NULL, 'active', NULL, NULL, NULL, NULL, '::1', NULL, NULL, NULL, 'yes', '0'),
(220, 'test', NULL, 'test1@test.com', '670b14728ad9902aecba32e22fa4f6bd', 'Dhanbad', NULL, '2020-09-02 11:41:27', 'Jharkhand', 'Dhanbad', 'male', 'General', '1997-06-02', NULL, NULL, 0, '35948165989', '', NULL, 'IN', NULL, 'active', NULL, NULL, NULL, NULL, '::1', NULL, NULL, NULL, 'yes', '0'),
(221, 'test', '', 'test1@test.com', '670b14728ad9902aecba32e22fa4f6bd', 'Dhanbad', NULL, '2020-09-02 11:41:27', 'India', 'Dhanbad', 'male', 'General', '1997-06-02', NULL, NULL, 0, '35948165989', '', NULL, 'IN', NULL, 'active', NULL, NULL, NULL, NULL, '::1', NULL, NULL, NULL, 'yes', 'Yes'),
(222, 'test', NULL, 'testw@test.com', '670b14728ad9902aecba32e22fa4f6bd', 'jh', NULL, '2020-09-02 12:14:09', 'Jharkhand', 'dhn', 'female', 'Sc/St', '1999-02-19', NULL, NULL, 0, '2564654567878', '', NULL, 'IN', NULL, 'active', NULL, NULL, NULL, NULL, '::1', NULL, NULL, NULL, 'yes', '0'),
(223, 'test', NULL, 'testw@test.com', '670b14728ad9902aecba32e22fa4f6bd', 'jh', NULL, '2020-09-02 12:14:09', 'Jharkhand', 'dhn', 'female', 'Sc/St', '1999-02-19', NULL, NULL, 0, '2564654567878', '', NULL, 'IN', NULL, 'active', NULL, NULL, NULL, NULL, '::1', NULL, NULL, NULL, 'yes', '0'),
(224, 'demo', NULL, 'demo@demo.com', '670b14728ad9902aecba32e22fa4f6bd', 'DHN', NULL, '2020-09-02 12:34:23', 'Jharkhand', 'DHN', 'female', 'Obc', '1998-07-02', NULL, NULL, 0, '65461654685615', '', NULL, 'IN', NULL, 'active', NULL, '2020-09-03 14:58:00', '2020-09-03 14:58:00', NULL, '::1', NULL, NULL, NULL, 'yes', 'Yes'),
(225, 'employee', '', 'employee@demo.com', '670b14728ad9902aecba32e22fa4f6bd', 'dhn', NULL, '2020-09-02 12:47:58', 'India', 'dhn', 'female', 'General', '1999-05-06', NULL, NULL, 0, '8697968', '', NULL, 'IN', NULL, 'active', NULL, '2020-09-07 12:06:13', '2020-09-07 12:06:13', NULL, '::1', NULL, NULL, NULL, 'yes', 'Yes'),
(226, 'employee', NULL, 'employee@demo.com', '670b14728ad9902aecba32e22fa4f6bd', 'dhn', NULL, '2020-09-02 12:47:58', 'Jharkhand', 'dhn', 'female', 'Sc/St', '1999-05-06', NULL, NULL, 0, '8697968', '', NULL, 'IN', NULL, 'active', NULL, NULL, NULL, NULL, '::1', NULL, NULL, NULL, 'yes', 'Yes'),
(228, 'demo', NULL, 'demo2@demo.com', '670b14728ad9902aecba32e22fa4f6bd', 'dhn', NULL, '2020-09-02 14:10:35', 'Jharkhand', 'dhn', 'male', 'General', '1997-02-21', NULL, NULL, 0, '5585584744', '', NULL, 'IN', NULL, 'active', NULL, '2020-09-03 17:52:15', '2020-09-03 17:52:15', NULL, '::1', NULL, NULL, NULL, 'yes', 'Yes'),
(232, 'ravi kumar', '', 'ravicsir001@gmail.com', '670b14728ad9902aecba32e22fa4f6bd', 'DHN', NULL, '2020-09-04 10:07:41', 'India', 'dhn', 'male', 'EWS', '1996-09-19', NULL, 'ravi-kumar-JOBPORTAL-232.png', 0, '896945599', '', NULL, 'Indian', NULL, 'active', NULL, '2020-09-04 10:50:22', '2020-09-07 09:26:31', NULL, '::1', NULL, NULL, NULL, 'yes', 'Yes'),
(233, 'student', NULL, 'student12@gmail.com', '670b14728ad9902aecba32e22fa4f6bd', 'dhn', NULL, '2020-09-07 12:30:45', 'Jharkhand', 'dhn', 'male', 'General', '1995-02-20', NULL, NULL, 0, '655656', '65165', NULL, 'IN', NULL, 'active', NULL, '2020-09-07 12:47:49', '2020-09-07 12:47:49', NULL, '::1', NULL, NULL, NULL, 'yes', 'Yes'),
(234, 'gfgfgfhghgfgh', NULL, 'gkjhgisuh@gmail.com', '670b14728ad9902aecba32e22fa4f6bd', 'fkuuyyufukj', NULL, '2020-09-07 12:38:03', 'Jharkhand', 'gkg', 'male', 'General', '1997-02-14', NULL, NULL, 0, '5656566565661', '2654641654', NULL, 'ffffkf', NULL, 'active', NULL, NULL, NULL, NULL, '::1', NULL, NULL, NULL, 'yes', 'Yes'),
(235, 'MMMMM', NULL, 'JJJHHH@DEMP.COM', '670b14728ad9902aecba32e22fa4f6bd', 'MKJHV', NULL, '2020-09-07 12:52:01', 'Jharkhand', 'KLJHG', 'female', 'General', '1980-11-19', NULL, NULL, 0, '65546564564645', '', NULL, 'KJHG', NULL, 'active', NULL, NULL, NULL, NULL, '::1', NULL, NULL, NULL, 'yes', 'Yes'),
(236, 'lgj fh as', NULL, 'gfhjgfdh@gmail.com', '670b14728ad9902aecba32e22fa4f6bd', 'gjsoeirjsdrfjn', NULL, '2020-09-07 14:26:25', 'Jharkhand', 'gsg', 'male', 'General', '1980-12-30', NULL, NULL, 0, '28256565626', '', NULL, 'ga', NULL, 'active', NULL, NULL, NULL, NULL, '::1', NULL, NULL, NULL, 'yes', 'Yes'),
(237, 'fsfsd', NULL, 'dfredf@gmail.com', '670b14728ad9902aecba32e22fa4f6bd', 'dfghfd', NULL, '2020-09-07 14:33:47', 'Jharkhand', 'bgh', 'male', 'General', '1986-08-17', NULL, NULL, 0, '2535659666', '', NULL, 'in', NULL, 'active', NULL, '2020-09-07 14:36:15', '2020-09-07 14:36:15', NULL, '::1', NULL, NULL, NULL, 'yes', 'Yes'),
(238, 'Today', NULL, 'today@today.com', '1caba15387d8a7f98e31a21c797e258c', 'Current Address', NULL, '2020-09-07 15:17:43', 'Jharkhand', 'DHANBAD', 'male', 'General', '1990-01-01', NULL, NULL, 0, '12364564545', '', NULL, 'Indian', NULL, 'active', NULL, NULL, NULL, NULL, '::1', NULL, NULL, NULL, 'yes', 'Yes'),
(239, 'Today', NULL, 'today1@today1.com', '1caba15387d8a7f98e31a21c797e258c', 'Current Address', NULL, '2020-09-07 15:20:42', 'Jharkhand', 'DHANBAD', 'male', 'General', '1990-01-01', NULL, NULL, 0, '12364564545', '', NULL, 'Indian', NULL, 'active', NULL, NULL, NULL, NULL, '::1', NULL, NULL, NULL, 'yes', 'Yes'),
(240, 'jkhf', NULL, 'djfhgiuds@demo.com', '670b14728ad9902aecba32e22fa4f6bd', 'gf', NULL, '2020-09-07 16:00:36', 'Jharkhand', 'ttt', 'male', 'General', '1986-03-21', NULL, NULL, 0, '2555555', '', NULL, 'tt', NULL, 'active', NULL, NULL, NULL, NULL, '::1', NULL, NULL, NULL, 'yes', 'Yes'),
(241, 'jkhf', NULL, 'xdjfhgiuds@demo.com', '670b14728ad9902aecba32e22fa4f6bd', 'gf', NULL, '2020-09-07 16:04:51', 'Jharkhand', 'ttt', 'male', 'General', '1986-03-21', NULL, NULL, 0, '2555555', 'dfh', NULL, 'tt', NULL, 'active', NULL, NULL, NULL, NULL, '::1', NULL, NULL, NULL, 'yes', 'Yes'),
(242, 'jkhf', NULL, 'xdnnnnjfhgiuds@demo.com', '670b14728ad9902aecba32e22fa4f6bd', 'gf', NULL, '2020-09-07 16:08:31', 'Jharkhand', 'ttt', 'male', 'General', '1986-03-21', NULL, NULL, 0, '2555555', 'dfh', NULL, 'tt', NULL, 'active', NULL, NULL, NULL, NULL, '::1', NULL, NULL, NULL, 'yes', 'Yes'),
(243, 'jkhf', NULL, 'xdnnnGXFnjfhgiuds@demo.com', '670b14728ad9902aecba32e22fa4f6bd', 'gf', NULL, '2020-09-07 16:09:34', 'Jharkhand', 'ttt', 'male', 'General', '1986-03-21', NULL, NULL, 0, '2555555', 'dfh', NULL, 'tt', NULL, 'active', NULL, NULL, NULL, NULL, '::1', NULL, NULL, NULL, 'yes', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `pp_job_titles`
--

CREATE TABLE `pp_job_titles` (
  `ID` int(11) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pp_job_titles`
--

INSERT INTO `pp_job_titles` (`ID`, `value`, `text`) VALUES
(1, 'Web Designer', 'Web Designer'),
(2, 'Web Developer', 'Web Developer'),
(3, 'Graphic Designer', 'Graphic Designer'),
(4, 'Project Manager', 'Project Manager'),
(5, 'Network Administrator', 'Network Administrator'),
(6, 'Network Engineer', 'Network Engineer'),
(7, 'Software Engineer', 'Software Engineer'),
(8, 'System Administrator', 'System Administrator'),
(9, 'System Analyst', 'System Analyst');

-- --------------------------------------------------------

--
-- Table structure for table `pp_newsletter`
--

CREATE TABLE `pp_newsletter` (
  `ID` int(11) NOT NULL,
  `email_name` varchar(50) DEFAULT NULL,
  `from_name` varchar(60) DEFAULT NULL,
  `from_email` varchar(120) DEFAULT NULL,
  `email_subject` varchar(100) DEFAULT NULL,
  `email_body` text DEFAULT NULL,
  `email_interval` int(4) DEFAULT NULL,
  `status` enum('inactive','active') DEFAULT 'active',
  `dated` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pp_newsletter`
--

INSERT INTO `pp_newsletter` (`ID`, `email_name`, `from_name`, `from_email`, `email_subject`, `email_body`, `email_interval`, `status`, `dated`) VALUES
(1, NULL, 'ravi kumar', 'ravi.rdx9122@gmail.com', 'job', '<span style=\"color: rgb(45, 45, 45); font-family: &quot;Noto Sans&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 13.3333px;\">Data entry employment is a wide field. Sometimes referred to as a data entry operator, data entry specialist, data entry clerk or an information processing worker these are the common data entry duties and data entry skills for the job.</span><br style=\"-webkit-font-smoothing: antialiased; box-sizing: border-box; font-family: &quot;Noto Sans&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; color: rgb(45, 45, 45); font-size: 13.3333px;\" />\r\n<span style=\"color: rgb(45, 45, 45); font-family: &quot;Noto Sans&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 13.3333px;\">&quot;Back office executives&quot; are the backbone of the companies and organizations. They look after the duties that help and keep the company running. They deal with faxes, phone calls, and data entry tasks. One executive at least is appointed to perform human resource (HR) tasks. Back office is the place where back office executives work. Fresher and female also apply</span>', 4, 'active', '2020-07-22 09:27:20'),
(2, NULL, 'Ravi', 'ravi.rdx9122@gmail.com', 'jobs', 'Bro', 20, 'active', '2020-07-23 10:01:13');

-- --------------------------------------------------------

--
-- Table structure for table `pp_post_jobs`
--

CREATE TABLE `pp_post_jobs` (
  `ID` int(11) NOT NULL,
  `employer_ID` int(11) NOT NULL,
  `job_title` varchar(255) NOT NULL,
  `company_ID` int(11) NOT NULL,
  `industry_ID` int(11) NOT NULL,
  `pay` varchar(60) NOT NULL,
  `dated` date NOT NULL,
  `sts` enum('inactive','pending','blocked','active') NOT NULL DEFAULT 'pending',
  `is_featured` enum('no','yes') NOT NULL DEFAULT 'no',
  `country` varchar(100) NOT NULL,
  `last_date` date NOT NULL,
  `age_required` varchar(50) NOT NULL,
  `qualification` varchar(60) NOT NULL,
  `experience` varchar(50) NOT NULL,
  `city` varchar(100) NOT NULL,
  `category` varchar(20) NOT NULL,
  `job_mode` enum('Home Based','Part Time','Full Time') NOT NULL DEFAULT 'Full Time',
  `vacancies` int(3) NOT NULL,
  `job_description` longtext NOT NULL,
  `contact_person` varchar(100) NOT NULL,
  `contact_email` varchar(100) NOT NULL,
  `contact_phone` varchar(30) NOT NULL,
  `viewer_count` int(11) NOT NULL DEFAULT 0,
  `job_slug` varchar(255) DEFAULT NULL,
  `ip_address` varchar(40) DEFAULT NULL,
  `flag` varchar(10) DEFAULT NULL,
  `old_id` int(11) DEFAULT NULL,
  `required_skills` varchar(255) DEFAULT NULL,
  `required_cats` varchar(12) NOT NULL,
  `email_queued` tinyint(1) DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pp_post_jobs`
--

INSERT INTO `pp_post_jobs` (`ID`, `employer_ID`, `job_title`, `company_ID`, `industry_ID`, `pay`, `dated`, `sts`, `is_featured`, `country`, `last_date`, `age_required`, `qualification`, `experience`, `city`, `category`, `job_mode`, `vacancies`, `job_description`, `contact_person`, `contact_email`, `contact_phone`, `viewer_count`, `job_slug`, `ip_address`, `flag`, `old_id`, `required_skills`, `required_cats`, `email_queued`) VALUES
(1, 90, 'Web Developer', 90, 22, 'Trainee Stipend', '2003-09-20', 'active', 'no', 'India', '1970-01-01', '', 'BE/B.Tech', 'Fresh', 'Dhanbad', 'General,EWS,OBC,SC,S', 'Full Time', 2, 'A&nbsp;Web Developer&nbsp;is responsible for the coding, design and layout of a&nbsp;website&nbsp;according to a company&#39;s specifications. As the role takes into consideration user experience and function, a certain level of both graphic design and computer programming is necessary.', '98765463210', 'contact@gmail.com', '9876543210', 0, 'getjobcom-jobs-in-dhanbad-web-developer-1', '::1', NULL, NULL, 'web development, php', '', 0),
(2, 90, 'Software Developer', 90, 22, 'Trainee Stipend', '2003-09-20', 'active', 'no', 'India', '1970-01-01', '', 'BE/B.Tech', 'Fresh', 'Ranchi', 'General', 'Full Time', 1, 'A standard&nbsp;Software Developer job description&nbsp;should include, but not be limited to: Researching, designing, implementing and managing&nbsp;software&nbsp;programs. Testing and evaluating new programs. Identifying areas for modification in existing programs and subsequently developing these modifications.', '9876543210', 'contact@gmail.com', '9876543210', 0, 'getjobcom-jobs-in-ranchi-software-developer-2', '::1', NULL, NULL, 'software developer', '', 0),
(3, 93, 'Education', 93, 1, 'Trainee Stipend', '2004-09-20', 'active', 'no', 'India', '2018-09-20', '', 'Graduation', 'Fresh', 'Dhanbad', 'General,SC,ST', 'Full Time', 20, 'nice', '', '', '', 0, 'vk-comm-hub-jobs-in-dhanbad-education-3', '::1', NULL, NULL, 'ba, 12th, diploma', '', 0),
(4, 93, 'Animation', 93, 16, 'Trainee Stipend', '2004-09-20', 'active', 'no', 'India', '2025-09-20', '', 'Diploma', 'Fresh', 'Dhanbad', 'EWS,SC', 'Full Time', 20, 'ok', '', '', '', 0, 'vk-comm-hub-jobs-in-dhanbad-animation-4', '::1', NULL, NULL, 'diploma, animation', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `pp_prohibited_keywords`
--

CREATE TABLE `pp_prohibited_keywords` (
  `ID` int(11) NOT NULL,
  `keyword` varchar(150) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pp_prohibited_keywords`
--

INSERT INTO `pp_prohibited_keywords` (`ID`, `keyword`) VALUES
(8, 'idiot'),
(9, 'fuck'),
(10, 'bitch');

-- --------------------------------------------------------

--
-- Table structure for table `pp_qualifications`
--

CREATE TABLE `pp_qualifications` (
  `ID` int(5) NOT NULL,
  `val` varchar(25) DEFAULT NULL,
  `text` varchar(25) DEFAULT NULL,
  `display_order` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pp_qualifications`
--

INSERT INTO `pp_qualifications` (`ID`, `val`, `text`, `display_order`) VALUES
(1, 'BA', 'BA', NULL),
(2, 'BE/B.Tech', 'BE/B.Tech', NULL),
(3, 'BS', 'BS', NULL),
(4, 'CA', 'CA', NULL),
(5, 'Certification', 'Certification', NULL),
(6, 'Diploma', 'Diploma', NULL),
(7, 'HSSC', 'HSSC', NULL),
(8, 'MA', 'MA', NULL),
(9, 'MBA', 'MBA', NULL),
(10, 'MS', 'MS', NULL),
(11, 'PhD', 'PhD', NULL),
(12, 'SSC', 'SSC', NULL),
(13, 'ACMA', 'ACMA', NULL),
(14, 'MCS', 'MCS', NULL),
(15, 'Does not matter', 'Does not matter', NULL),
(16, 'Matric', 'Matric', NULL),
(17, '12th', '12th', NULL),
(19, 'MCA', 'MCA', NULL),
(20, 'Graduation', 'Graduation', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pp_salaries`
--

CREATE TABLE `pp_salaries` (
  `ID` int(5) NOT NULL,
  `val` varchar(25) DEFAULT NULL,
  `text` varchar(25) DEFAULT NULL,
  `display_order` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pp_salaries`
--

INSERT INTO `pp_salaries` (`ID`, `val`, `text`, `display_order`) VALUES
(1, 'Trainee Stipend', 'Trainee Stipend', 0),
(2, '5000rs-10000rs', '5-10', NULL),
(3, '11000rs-15000rs', '11-15', NULL),
(4, '16000-20000', '16-20', NULL),
(5, '21000-25000', '21-25', NULL),
(6, '26000-30000', '26-30', NULL),
(7, '31000-35000', '31-35', NULL),
(8, '36000-40000', '36-40', NULL),
(9, '41000-50000', '41-50', NULL),
(10, '51000-60000', '51-60', NULL),
(11, '61000rs-70000rs', '61-70', NULL),
(12, '71000-80000', '71-80', NULL),
(13, '81000rs-100000rs', '81-100', NULL),
(14, '100000-120000', '101-120', NULL),
(15, '120000-140000', '121-140', NULL),
(16, '140000-160000', '141-160', NULL),
(17, '160000-200000', '161-200', NULL),
(18, '200000-240000', '201-240', NULL),
(19, '240000-280000', '241-280', NULL),
(20, '281000-350000', '281-350', NULL),
(21, '350000-450000', '351-450', NULL),
(22, '450000 or above', '450 or above', NULL),
(23, 'Discuss', 'Discuss', NULL),
(24, 'Depends', 'Depends', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pp_scam`
--

CREATE TABLE `pp_scam` (
  `ID` int(11) NOT NULL,
  `user_ID` int(11) DEFAULT NULL,
  `job_ID` int(11) DEFAULT NULL,
  `reason` text DEFAULT NULL,
  `dated` datetime DEFAULT NULL,
  `ip_address` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pp_seeker_academic`
--

CREATE TABLE `pp_seeker_academic` (
  `ID` int(11) NOT NULL,
  `seeker_ID` int(11) DEFAULT NULL,
  `degree_level` varchar(30) DEFAULT NULL,
  `degree_title` varchar(100) DEFAULT NULL,
  `major` varchar(155) DEFAULT NULL,
  `institude` varchar(155) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `completion_year` int(5) DEFAULT NULL,
  `dated` datetime DEFAULT NULL,
  `flag` varchar(10) DEFAULT NULL,
  `old_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pp_seeker_academic`
--

INSERT INTO `pp_seeker_academic` (`ID`, `seeker_ID`, `degree_level`, `degree_title`, `major`, `institude`, `country`, `city`, `completion_year`, `dated`, `flag`, `old_id`) VALUES
(1, 10, NULL, 'BA', 'test', 'teste e ere', 'United States of America', 'New york', 2012, '2016-03-12 13:05:55', NULL, NULL),
(2, 18, NULL, 'BS', 'axaxsxsxsxsxs', 'xsxsxsxsxsxsxsxsx', 'Peru', 'Lima', 2015, '2016-03-28 20:03:39', NULL, NULL),
(3, 18, NULL, 'Certification', 'cscscdscdsdc', 'cdscdsdcscdsdcscsdcdcsd', 'Pakistan', 'sdcsdcsdcsd', 2020, '2016-03-28 20:05:34', NULL, NULL),
(4, 18, NULL, 'Diploma', 'csdcsdcsdcsdscsdcscdsdcsdc', 'cdscsdcsdcsdcsdcsdcsdcsdcsdc', 'Pakistan', 'csdcsdcscsdcsd', 2018, '2016-03-28 20:06:28', NULL, NULL),
(5, 15, NULL, 'Certification', '3º Ano Completo', 'EREM Tamandaré', 'Brazil', 'Tamandaré', 2014, '2016-03-28 22:24:18', NULL, NULL),
(6, 25, NULL, 'BS', 'ghj', 'fghjfgh', 'United States of America', 'fortson', 2007, '2016-03-29 13:43:47', NULL, NULL),
(7, 32, NULL, 'MBA', 'ded', 'eded', 'India', 'arabbia', 2020, '2016-03-31 23:38:10', NULL, NULL),
(8, 36, NULL, 'MBA', 'MKT', 'WUB', 'Bangladesh', 'dhaka', 2013, '2016-04-02 03:23:29', NULL, NULL),
(9, 59, NULL, 'MBA', 'computer application', 'add', 'India', 'mana', 2010, '2016-04-08 16:06:52', NULL, NULL),
(10, 74, NULL, 'BA', 'php', 'abc', 'India', 'mangalore', 2014, '2016-04-15 11:42:43', NULL, NULL),
(11, 75, NULL, 'BS', 'Computer', 'SV University', 'India', 'Bangalore', 2002, '2016-04-15 15:27:09', NULL, NULL),
(12, 95, NULL, 'PhD', 'test', 'test', 'Tahiti', 'test', 2021, '2016-05-04 18:51:07', NULL, NULL),
(13, 109, NULL, 'BA', '12', '21', 'Pakistan', '12', 2021, '2016-05-13 20:35:24', NULL, NULL),
(14, 132, NULL, 'Certification', 'sdas', 'asdsad', 'Peru', 'sadsad', 2000, '2016-06-01 03:45:47', NULL, NULL),
(15, 133, NULL, 'BS', 'ddd', 'sffs', 'Pakistan', 'Tgcvga', 2020, '2016-06-02 12:53:52', NULL, NULL),
(16, 137, NULL, 'BA', 'fdsa', 'fdas', 'Pakistan', 'fdsa', 2008, '2016-06-09 23:56:55', NULL, NULL),
(18, 139, NULL, 'CA', 'fdsa', 'fdsa', 'Norway', 'fdsa', 2006, '2016-06-11 22:02:33', NULL, NULL),
(19, 123, NULL, 'BA', 'Science', 'anc', 'Nigeria', 'adg', 2012, '2016-06-17 02:41:10', NULL, NULL),
(32, 169, NULL, 'Diploma', 'cse', 'gp dhanbad', 'India', 'dhanbad', 2013, '2020-08-20 15:30:01', NULL, NULL),
(34, 171, NULL, 'BE/B.Tech', 'cse', 'JNTU Hyderabad', 'India', 'Nalgonda', 2017, '2020-08-25 16:23:57', NULL, NULL),
(37, 172, NULL, 'BE/B.Tech', 'cse', 'JNTU Hyderabad', 'India', 'HYDERABAD', 2017, '2020-08-26 11:43:14', NULL, NULL),
(38, 219, NULL, 'BE/B.Tech', 'computer', 'BIT', 'India', 'Dhanbad', 2012, '2020-09-01 16:35:26', NULL, NULL),
(41, 232, NULL, 'CA', 'account', 'NIIT', 'India', 'dhn', 2018, '2020-09-04 11:12:41', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pp_seeker_additional_info`
--

CREATE TABLE `pp_seeker_additional_info` (
  `ID` int(11) NOT NULL,
  `seeker_ID` int(11) DEFAULT NULL,
  `languages` varchar(255) DEFAULT NULL COMMENT 'JSON data',
  `interest` varchar(155) DEFAULT NULL,
  `awards` varchar(100) DEFAULT NULL,
  `additional_qualities` varchar(155) DEFAULT NULL,
  `convicted_crime` enum('no','yes') DEFAULT 'no',
  `crime_details` text DEFAULT NULL,
  `summary` text DEFAULT NULL,
  `bad_habits` varchar(255) DEFAULT NULL,
  `salary` varchar(50) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pp_seeker_additional_info`
--

INSERT INTO `pp_seeker_additional_info` (`ID`, `seeker_ID`, `languages`, `interest`, `awards`, `additional_qualities`, `convicted_crime`, `crime_details`, `summary`, `bad_habits`, `salary`, `keywords`, `description`) VALUES
(1, 8, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(2, 9, NULL, NULL, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur massa nisl, porttitor id urna sag', NULL, 'no', NULL, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur massa nisl, porttitor id urna sagittis, mollis tristique diam. Donec augue nulla, tempus id egestas finibus, sodales a ligula. Suspendisse lacinia malesuada sapien nec pretium. Curabitur sed augue sed neque vulputate congue at pellentesque ante. Aliquam facilisis cursus eros, in laoreet risus luctus non. Aliquam tincidunt purus in urna molestie, eget aliquet lectus sollicitudin. Proin pretium tellus maximus dolor dapibus aliquet. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam sed bibendum nulla. Nulla ac magna placerat, tristique nisl a, consectetur lectus. Pellentesque quis enim semper, placerat augue vel, faucibus urna. Nullam ut odio volutpat, scelerisque mi ac, ornare libero.', NULL, NULL, NULL, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur massa nisl, porttitor id urna sagittis, mollis tristique diam. Donec augue nulla, tempus id egestas finibus, sodales a ligula. Suspendisse lacinia malesuada sapien nec pretium. Curabitur sed augue sed neque vulputate congue at pellentesque ante. Aliquam facilisis cursus eros, in laoreet risus luctus non. Aliquam tincidunt purus in urna molestie, eget aliquet lectus sollicitudin. Proin pretium tellus maximus dolor dapibus aliquet. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam sed bibendum nulla. Nulla ac magna placerat, tristique nisl a, consectetur lectus. Pellentesque quis enim semper, placerat augue vel, faucibus urna. Nullam ut odio volutpat, scelerisque mi ac, ornare libero.'),
(3, 10, NULL, NULL, 'Quisque ac scelerisque libero, nec blandit neque. Nullam felis nisl, elementum eu sapien ut, convall', NULL, 'no', NULL, 'Quisque ac scelerisque libero, nec blandit neque. Nullam felis nisl, elementum eu sapien ut, convallis interdum felis. In turpis odio, fermentum non pulvinar gravida, posuere quis magna. Ut mollis eget neque at euismod. Interdum et malesuada fames ac ante ipsum primis in faucibus. Integer faucibus orci a pulvinar malesuada. Aenean at felis vitae lorem venenatis consequat. Nam non nunc euismod, consequat ligula non, tristique odio. Ut leo sapien, aliquet sed ultricies et, scelerisque quis nulla. Aenean non sapien maximus, convallis eros vitae, iaculis massa. In fringilla hendrerit nisi, eu pellentesque massa faucibus molestie. Etiam laoreet eros quis faucibus rutrum. Quisque eleifend purus justo, eget tempus quam interdum non.', NULL, NULL, NULL, 'Quisque ac scelerisque libero, nec blandit neque. Nullam felis nisl, elementum eu sapien ut, convallis interdum felis. In turpis odio, fermentum non pulvinar gravida, posuere quis magna. Ut mollis eget neque at euismod. Interdum et malesuada fames ac ante ipsum primis in faucibus. Integer faucibus orci a pulvinar malesuada. Aenean at felis vitae lorem venenatis consequat. Nam non nunc euismod, consequat ligula non, tristique odio. Ut leo sapien, aliquet sed ultricies et, scelerisque quis nulla. Aenean non sapien maximus, convallis eros vitae, iaculis massa. In fringilla hendrerit nisi, eu pellentesque massa faucibus molestie. Etiam laoreet eros quis faucibus rutrum. Quisque eleifend purus justo, eget tempus quam interdum non.'),
(4, 11, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(5, 12, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(6, 13, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(7, 14, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(8, 15, NULL, NULL, 'Qualquer coisa que vim que seja bem vindo.', NULL, 'no', NULL, 'Eu geralmente trabalhei com computação e fotografias. O meu objetivo é dar o meu melhor a empresa, pois quando eu tento dar o meu melhor eu consigo, e pra conquistar as pessoas eu sou de mais.', NULL, NULL, NULL, 'Serviços Gerais'),
(9, 16, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(10, 17, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(11, 18, NULL, NULL, 'demo demo demo demo demo demo demo demode mdo', NULL, 'no', NULL, 'dededededed edededededededed dede', NULL, NULL, NULL, 'de demo demo demo demo demo demo dem'),
(12, 19, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(13, 20, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(14, 21, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(15, 22, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(16, 23, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(17, 24, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(18, 25, NULL, NULL, 'rthrthrt', NULL, 'no', NULL, 'df df df gd fg dfgdfgsdfrg', NULL, NULL, NULL, 'sfrtghdr'),
(19, 26, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(20, 27, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(21, 28, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(22, 29, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(23, 30, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(24, 32, NULL, NULL, NULL, NULL, 'no', NULL, 'adfsdfsdfsdfsdf', NULL, NULL, NULL, NULL),
(25, 33, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(26, 35, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(27, 36, NULL, NULL, NULL, NULL, 'no', NULL, 'My Name is Saiful', NULL, NULL, NULL, NULL),
(28, 37, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(29, 38, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(30, 41, NULL, NULL, NULL, NULL, 'no', NULL, 'kdkdkd kdkdkd ksksks zkzkzkz sksksksksks kdkdkd kdkdkd ksksks zkzkzkz sksksksksks kdkdkd kdkdkd ksksks zkzkzkz sksksksksks kdkdkd kdkdkd ksksks zkzkzkz sksksksksks vkdkdkd kdkdkd ksksks zkzkzkz sksksksksks kdkdkd kdkdkd ksksks zkzkzkz sksksksksks kdkdkd kdkdkd ksksks zkzkzkz sksksksksks kdkdkd kdkdkd ksksks zkzkzkz sksksksksks kdkdkd kdkdkd ksksks zkzkzkz sksksksksks vkdkdkd kdkdkd ksksks zkzkzkz sksksksksks kdkdkd kdkdkd ksksks zkzkzkz sksksksksks kdkdkd kdkdkd ksksks zkzkzkz sksksksksks kdkdkd kdkdkd ksksks zkzkzkz sksksksksks kdkdkd kdkdkd ksksks zkzkzkz sksksksksks vkdkdkd kdkdkd ksksks zkzkzkz sksksksksks', NULL, NULL, NULL, NULL),
(31, 42, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(32, 47, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(33, 48, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(34, 49, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(35, 50, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(36, 51, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(37, 52, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(38, 53, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(39, 54, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(40, 55, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(41, 56, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(42, 57, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(43, 58, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(44, 59, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(45, 60, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(46, 61, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(47, 65, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(48, 66, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(49, 67, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(50, 68, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(51, 69, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(52, 70, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(53, 71, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(54, 74, NULL, NULL, 'ghgfhg', NULL, 'no', NULL, 'fgfdfd', NULL, NULL, NULL, 'hghg'),
(55, 75, NULL, NULL, 'Best Employee of the Year', NULL, 'no', NULL, '1	Around 6+ Years of Experience in Manual testing (Printer Domain).\n2	Expertise in Software testing process.\n3	Proficient with Software Development Life cycle.\n4	Black Box testing, Integration Testing, System testing, Boundary testing and Regression testing process of a given software application for different software releases and builds.\n5	Development of test procedure, test cases and test reporting documents.\n6	Review Test Cases on Fixed Defects by Onsite Team on different freezes and different Products.', NULL, NULL, NULL, 'To be a part of an Organization which provides a high quality of work life through challenging opportunities, a meaningful career growth and professional development'),
(56, 76, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(57, 78, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(58, 79, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(59, 80, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(60, 81, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(61, 82, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(62, 83, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(63, 84, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(64, 85, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(65, 86, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(66, 87, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(67, 88, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(68, 89, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(69, 90, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(70, 91, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(71, 92, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(72, 93, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(73, 94, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(74, 95, NULL, NULL, NULL, NULL, 'no', NULL, 'test at test.com and testing test from test date to test updat and original test update.', NULL, NULL, NULL, NULL),
(75, 97, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(76, 98, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(77, 99, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(78, 100, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(79, 102, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(80, 103, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(81, 105, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(82, 106, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(83, 107, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(84, 108, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(85, 109, NULL, NULL, NULL, NULL, 'no', NULL, 'hello pro', NULL, NULL, NULL, NULL),
(86, 110, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(87, 111, NULL, NULL, NULL, NULL, 'no', NULL, 'You\'ve ventured too far out into the desert! Time to head back.\n\nWe couldn\'t find any results for your search. Use more generic words or double check your spelling.You\'ve ventured too far out into the desert! Time to head back.\n\nWe couldn\'t find any results for your search. Use more generic words or double check your spelling.You\'ve ventured too far out into the desert! Time to head back.\n\nWe couldn\'t find any results for your search. Use more generic words or double check your spelling.', NULL, NULL, NULL, NULL),
(88, 113, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(89, 114, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(90, 115, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(91, 116, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(92, 117, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(93, 118, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(94, 119, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(95, 121, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(96, 122, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(97, 123, NULL, NULL, NULL, NULL, 'no', NULL, 'sfsdfds fsd fsdf sdfsdfsdfsdfsdf', NULL, NULL, NULL, NULL),
(98, 124, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(99, 125, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(100, 126, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(101, 127, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(102, 128, NULL, NULL, 'vcvc', NULL, 'no', NULL, 'vbvbvbv', NULL, NULL, NULL, 'vcvc'),
(103, 129, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(104, 130, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(105, 132, NULL, NULL, 'dadasdas', NULL, 'no', NULL, 'sdasdsad', NULL, NULL, NULL, 'sdsadsadad'),
(106, 133, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(107, 134, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(108, 135, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(109, 136, NULL, NULL, 'was class prefect', NULL, 'no', NULL, 'advance welding and inspection pro', NULL, NULL, NULL, 'to help employers succeed'),
(110, 137, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(111, 138, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(112, 139, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(113, 142, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(114, 143, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(115, 144, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(116, 145, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(117, 146, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(118, 147, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(119, 148, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(120, 149, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(121, 151, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(122, 152, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(123, 153, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(124, 154, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(125, 155, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(126, 156, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(127, 157, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(128, 158, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(129, 159, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(130, 160, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(131, 163, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(132, 167, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(133, 168, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(134, 169, NULL, NULL, '1st', NULL, 'no', NULL, 'HHHH', NULL, NULL, NULL, 'job'),
(135, 170, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(136, 171, NULL, NULL, '1st prices of the team for team leading', NULL, 'no', NULL, 'i am a project assistant in csir cimfr dhanbad', NULL, NULL, NULL, 'ok'),
(137, 172, NULL, NULL, NULL, NULL, 'no', NULL, 'DHANBAD', NULL, NULL, NULL, NULL),
(138, 173, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(139, 174, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(140, 175, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(141, 176, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(142, 180, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(143, 182, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(144, 185, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(145, 186, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(146, 187, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(147, 188, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(148, 189, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(149, 190, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(150, 191, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(151, 192, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(152, NULL, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(153, NULL, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(154, NULL, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(155, NULL, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(156, 193, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(157, 194, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(158, 195, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(159, 196, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(160, 197, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(161, 198, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(162, 199, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(163, 200, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(164, 201, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(165, 202, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(166, 203, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(167, 204, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(168, 205, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(169, 206, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(170, 211, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(171, 212, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(172, 213, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(173, 214, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(174, 216, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(175, 217, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(176, 218, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(177, 219, NULL, NULL, 'team leader', NULL, 'no', NULL, 'hhh', NULL, NULL, NULL, 'i work at nareshit'),
(178, 220, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(179, 221, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(180, 222, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(181, 223, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(182, NULL, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(183, 224, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(184, 225, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(185, NULL, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(186, 229, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(187, 230, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(188, 231, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(189, 232, NULL, NULL, 'Achievements', NULL, 'no', NULL, 'FGHDSIUFGHISDFH SIUFH', NULL, NULL, NULL, 'Career Objective'),
(190, 233, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(191, 234, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(192, 235, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(193, 236, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(194, 237, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(195, 238, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(196, 239, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(197, 240, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(198, 241, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(199, 242, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(200, 243, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL),
(201, 244, NULL, NULL, 'Mango', NULL, 'no', NULL, 'fgh', NULL, NULL, NULL, 'Apple');

-- --------------------------------------------------------

--
-- Table structure for table `pp_seeker_applied_for_job`
--

CREATE TABLE `pp_seeker_applied_for_job` (
  `ID` int(11) NOT NULL,
  `seeker_ID` int(11) NOT NULL,
  `job_ID` int(11) NOT NULL,
  `cover_letter` text DEFAULT NULL,
  `expected_salary` varchar(20) DEFAULT NULL,
  `dated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ip_address` varchar(40) DEFAULT NULL,
  `employer_ID` int(11) DEFAULT NULL,
  `flag` varchar(10) DEFAULT NULL,
  `old_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pp_seeker_applied_for_job`
--

INSERT INTO `pp_seeker_applied_for_job` (`ID`, `seeker_ID`, `job_ID`, `cover_letter`, `expected_salary`, `dated`, `ip_address`, `employer_ID`, `flag`, `old_id`) VALUES
(1, 9, 8, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur massa nisl, porttitor id urna sagittis, mollis tristique diam. Donec augue nulla, tempus id egestas finibus, sodales a ligula. Suspendisse lacinia malesuada sapien nec pretium. Curabitur sed augue sed neque vulputate congue at pellentesque ante. Aliquam facilisis cursus eros, in laoreet risus luctus non. Aliquam tincidunt purus in urna molestie, eget aliquet lectus sollicitudin. Proin pretium tellus maximus dolor dapibus aliquet. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam sed bibendum nulla. Nulla ac magna placerat, tristique nisl a, consectetur lectus. Pellentesque quis enim semper, placerat augue vel, faucibus urna. Nullam ut odio volutpat, scelerisque mi ac, ornare libero.', 'Trainee Stipend', '2016-03-12 01:53:57', NULL, 7, NULL, NULL),
(2, 10, 12, 'Quisque ac scelerisque libero, nec blandit neque. Nullam felis nisl, elementum eu sapien ut, convallis interdum felis. In turpis odio, fermentum non pulvinar gravida, posuere quis magna. Ut mollis eget neque at euismod. Interdum et malesuada fames ac ante ipsum primis in faucibus. Integer faucibus orci a pulvinar malesuada. Aenean at felis vitae lorem venenatis consequat. Nam non nunc euismod, consequat ligula non, tristique odio. Ut leo sapien, aliquet sed ultricies et, scelerisque quis nulla. Aenean non sapien maximus, convallis eros vitae, iaculis massa. In fringilla hendrerit nisi, eu pellentesque massa faucibus molestie. Etiam laoreet eros quis faucibus rutrum. Quisque eleifend purus justo, eget tempus quam interdum non.', '21000-25000', '2016-03-12 13:06:43', NULL, 11, NULL, NULL),
(3, 10, 9, 'Quisque ac scelerisque libero, nec blandit neque. Nullam felis nisl, elementum eu sapien ut, convallis interdum felis. In turpis odio, fermentum non pulvinar gravida, posuere quis magna. Ut mollis eget neque at euismod. Interdum et malesuada fames ac ante ipsum primis in faucibus. Integer faucibus orci a pulvinar malesuada. Aenean at felis vitae lorem venenatis consequat. Nam non nunc euismod, consequat ligula non, tristique odio. Ut leo sapien, aliquet sed ultricies et, scelerisque quis nulla. Aenean non sapien maximus, convallis eros vitae, iaculis massa. In fringilla hendrerit nisi, eu pellentesque massa faucibus molestie. Etiam laoreet eros quis faucibus rutrum. Quisque eleifend purus justo, eget tempus quam interdum non.', 'Trainee Stipend', '2016-03-12 13:07:08', NULL, 8, NULL, NULL),
(4, 11, 9, 'Test', '5000-10000', '2016-03-28 14:14:16', NULL, 8, NULL, NULL),
(5, 11, 15, 'Account Officer', 'Trainee Stipend', '2016-03-28 14:14:39', NULL, 14, NULL, NULL),
(6, 11, 7, 'Account Officer', 'Trainee Stipend', '2016-03-28 14:15:03', NULL, 6, NULL, NULL),
(7, 12, 15, 'bcchchv', '5000-10000', '2016-03-28 14:47:58', NULL, 14, NULL, NULL),
(8, 15, 12, '1.500', 'Depends', '2016-03-28 18:51:23', NULL, 11, NULL, NULL),
(9, 17, 15, 'uuuuu', 'Trainee Stipend', '2016-03-28 19:37:26', NULL, 14, NULL, NULL),
(10, 18, 14, 'deed  deededxeddxedde', '16000-20000', '2016-03-28 20:07:46', NULL, 13, NULL, NULL),
(11, 15, 7, 'Designer', '5000-10000', '2016-03-28 23:51:55', NULL, 6, NULL, NULL),
(12, 19, 2, 'hjjhklklfgh', '11000-15000', '2016-03-29 00:10:22', NULL, 1, NULL, NULL),
(13, 21, 14, 'aaaaaaaaaaaa', '31000-35000', '2016-03-29 02:58:06', NULL, 13, NULL, NULL),
(14, 23, 10, 'Hello, this is my cover letter.', '5000-10000', '2016-03-29 05:54:46', NULL, 9, NULL, NULL),
(15, 25, 15, 'fgmfhjm', '11000-15000', '2016-03-29 13:44:49', NULL, 14, NULL, NULL),
(16, 26, 15, 'sadfsdfsdfffffffffffffffffffffffs', '5000-10000', '2016-03-29 22:41:52', NULL, 14, NULL, NULL),
(17, 30, 12, '234234234', '5000-10000', '2016-03-31 20:01:56', NULL, 11, NULL, NULL),
(18, 30, 15, '45345345', '281000-350000', '2016-03-31 20:16:28', NULL, 14, NULL, NULL),
(19, 32, 3, 'fsdfsdf', '5000-10000', '2016-03-31 23:39:09', NULL, 2, NULL, NULL),
(20, 36, 12, 'Hello', '5000-10000', '2016-04-02 03:25:01', NULL, 11, NULL, NULL),
(21, 38, 14, 'test', '11000-15000', '2016-04-02 12:12:38', NULL, 13, NULL, NULL),
(22, 41, 13, 'ksksks ksksksks skskks ksksks ksksksks skskks ksksks ksksksks skskks ksksks ksksksks skskks ksksks ksksksks skskks ksksks ksksksks skskksksksks ksksksks skskks ksksks ksksksks skskks ksksks ksksksks skskksksksks ksksksks skskks ksksks ksksksks skskks ksksks ksksksks skskksksksks ksksksks skskks ksksks ksksksks skskks ksksks ksksksks skskksksksks ksksksks skskks ksksks ksksksks skskks ksksks ksksksks skskksksksks ksksksks skskks ksksks ksksksks skskks ksksks ksksksks skskksksksks ksksksks skskks ksksks ksksksks skskks ksksks ksksksks skskks', '11000-15000', '2016-04-03 02:58:44', NULL, 12, NULL, NULL),
(23, 42, 15, 'aaaaa', '21000-25000', '2016-04-03 03:32:39', NULL, 14, NULL, NULL),
(24, 42, 6, 'a', 'Trainee Stipend', '2016-04-03 03:33:06', NULL, 5, NULL, NULL),
(25, 47, 14, 'rrrr', 'Trainee Stipend', '2016-04-03 03:45:58', NULL, 13, NULL, NULL),
(26, 48, 3, 'fsdfsdf', '26000-30000', '2016-04-03 15:33:36', NULL, 2, NULL, NULL),
(27, 36, 15, 'Thanks', '16000-20000', '2016-04-03 17:39:50', NULL, 14, NULL, NULL),
(28, 49, 15, 'benimben44627', '16000-20000', '2016-04-03 20:17:39', NULL, 14, NULL, NULL),
(29, 51, 15, 'Test Letter', '5000-10000', '2016-04-04 21:08:49', NULL, 14, NULL, NULL),
(30, 52, 15, 'fd', '5000-10000', '2016-04-05 13:58:50', NULL, 14, NULL, NULL),
(31, 55, 15, '3123', '11000-15000', '2016-04-06 18:23:31', NULL, 14, NULL, NULL),
(32, 59, 12, 'dhggggggggg', '26000-30000', '2016-04-08 16:07:49', NULL, 11, NULL, NULL),
(33, 58, 15, 'dfghdg', 'Trainee Stipend', '2016-04-08 16:23:41', NULL, 14, NULL, NULL),
(34, 61, 2, 'oooo', '5000-10000', '2016-04-10 00:23:33', NULL, 1, NULL, NULL),
(35, 65, 7, 'sfdadsa', '16000-20000', '2016-04-11 17:21:27', NULL, 6, NULL, NULL),
(36, 66, 15, 'SDS', '5000-10000', '2016-04-12 01:34:51', NULL, 14, NULL, NULL),
(37, 67, 12, 'hola', '11000-15000', '2016-04-12 10:35:49', NULL, 11, NULL, NULL),
(38, 69, 12, 'orem ipsum dolor sit amet, consectetur adipiscing elit. Fusce venenatis arcu est. Phasellus vel dignissim tellus. Aenean fermentum fermentum convallis. Maecenas vitae ipsum sed risus viverra volutpat non ac sapien. Donec viverra massa at dolor imperdiet hendrerit. Nullam quis est vitae dui placerat posuere. Phasellus eget erat sit amet lacus semper consectetur. Sed a nisi nisi. Pellentesque hendrerit est id quam facilisis au', '281000-350000', '2016-04-13 18:36:52', NULL, 11, NULL, NULL),
(39, 70, 3, 'Just testing', '71000-80000', '2016-04-13 21:43:07', NULL, 2, NULL, NULL),
(40, 70, 12, 'lol', '5000-10000', '2016-04-13 22:25:07', NULL, 11, NULL, NULL),
(41, 74, 2, 'fhgh', '16000-20000', '2016-04-15 11:44:15', NULL, 1, NULL, NULL),
(42, 75, 3, 'test', '36000-40000', '2016-04-15 15:29:26', NULL, 2, NULL, NULL),
(43, 76, 12, 'fghfghfghgfh', '5000-10000', '2016-04-15 17:37:18', NULL, 11, NULL, NULL),
(44, 78, 15, 'denemeee', '11000-15000', '2016-04-16 12:06:39', NULL, 14, NULL, NULL),
(45, 80, 4, 'test', '41000-50000', '2016-04-17 23:23:48', NULL, 3, NULL, NULL),
(46, 81, 3, 'ok', 'Trainee Stipend', '2016-04-18 15:36:42', NULL, 2, NULL, NULL),
(47, 81, 13, 'Test', '11000-15000', '2016-04-19 18:55:11', NULL, 12, NULL, NULL),
(48, 83, 13, 'rtytrryty', '31000-35000', '2016-04-20 08:56:47', NULL, 12, NULL, NULL),
(49, 84, 12, 'dfgdfgdfgd', '5000-10000', '2016-04-22 03:43:30', NULL, 11, NULL, NULL),
(51, 95, 14, 'test', '450000 or above', '2016-05-04 18:51:57', NULL, 13, NULL, NULL),
(52, 97, 15, 'as', '26000-30000', '2016-05-05 15:55:09', NULL, 14, NULL, NULL),
(53, 99, 15, 'wdeasdasd', '450000 or above', '2016-05-06 01:03:12', NULL, 14, NULL, NULL),
(54, 92, 12, 'hello', '5000-10000', '2016-05-07 17:30:16', NULL, 11, NULL, NULL),
(55, 102, 2, 'test', '350000-450000', '2016-05-07 22:41:28', NULL, 1, NULL, NULL),
(56, 105, 7, 'Test cover letter', '5000-10000', '2016-05-10 03:11:06', NULL, 6, NULL, NULL),
(57, 107, 14, 'nada', '11000-15000', '2016-05-12 07:35:53', NULL, 13, NULL, NULL),
(58, 108, 13, 'jhgbhbhj', '51000-60000', '2016-05-12 23:32:26', NULL, 12, NULL, NULL),
(59, 92, 15, 'hhh', '21000-25000', '2016-05-13 13:18:09', NULL, 14, NULL, NULL),
(60, 111, 2, 'this is testing....', 'Discuss', '2016-05-15 01:23:49', NULL, 1, NULL, NULL),
(61, 115, 15, 'testset', '11000-15000', '2016-05-16 17:18:04', NULL, 14, NULL, NULL),
(62, 116, 12, 'As Web designers plan, create and code web pages, using both non-technical and technical skills to produce websites that fit the customer\'s requirements.\nThey are involved in the technical and graphical aspects of pages, producing not just the look of the website, but determining how it works as well. Web designers might also be responsible for the maintenance of an existing site.\nThe term web developer is sometimes used interchangeably with web designer, but this can be confusing. Web developing is a more specialist role, focusing on the back-end development of a website and will incorporate, among other things, the creation of highly complex search functions.\nThe recent growth in touchscreen phones and tablet devices has dictated a new way of designing websites, with the web designer needing to ensure that web pages are responsive no matter what type of device a viewer is using. Therefore the need to test websites at different stages of design, on a variety of different devices, has become an important aspect of the job.', 'Trainee Stipend', '2016-05-16 17:25:54', NULL, 11, NULL, NULL),
(63, 117, 15, 'cascsacs', '5000-10000', '2016-05-16 22:52:56', NULL, 14, NULL, NULL),
(64, 41, 15, 'tes cover letter', 'Trainee Stipend', '2016-05-21 19:30:17', NULL, 14, NULL, NULL),
(65, 123, 14, '\'t\'g(', '5000-10000', '2016-05-22 12:41:41', NULL, 13, NULL, NULL),
(66, 123, 15, '(--g-g', '5000-10000', '2016-05-22 12:42:01', NULL, 14, NULL, NULL),
(68, 125, 15, 'sfsqafa', '5000-10000', '2016-05-24 17:36:38', NULL, 14, NULL, NULL),
(69, 126, 10, 'fff', '61000-70000', '2016-05-25 00:34:56', NULL, 9, NULL, NULL),
(70, 126, 4, 'rwy3', '16000-20000', '2016-05-25 00:37:00', NULL, 3, NULL, NULL),
(71, 41, 14, 'test', '5000-10000', '2016-05-25 15:18:35', NULL, 13, NULL, NULL),
(72, 128, 7, 'gfgfg', '11000-15000', '2016-05-26 00:23:22', NULL, 6, NULL, NULL),
(73, 129, 2, 'asd', '5000-10000', '2016-05-27 19:06:06', NULL, 1, NULL, NULL),
(74, 129, 1, 'php mysql c# flash', '11000-15000', '2016-05-27 19:06:59', NULL, 1, NULL, NULL),
(75, 130, 10, 'Hello', '16000-20000', '2016-05-28 23:03:02', NULL, 9, NULL, NULL),
(76, 132, 7, 'ssss', 'Trainee Stipend', '2016-06-01 03:42:04', NULL, 6, NULL, NULL),
(77, 132, 10, 'sadsadasd', '5000-10000', '2016-06-01 03:46:57', NULL, 9, NULL, NULL),
(78, 133, 14, 'gg', '36000-40000', '2016-06-02 13:03:45', NULL, 13, NULL, NULL),
(79, 133, 13, 'ggggggggg', 'Trainee Stipend', '2016-06-02 13:09:53', NULL, 12, NULL, NULL),
(80, 117, 7, 'qdwdwd', 'Trainee Stipend', '2016-06-02 16:07:45', NULL, 6, NULL, NULL),
(81, 92, 7, 'ggg', '11000-15000', '2016-06-03 12:43:32', NULL, 6, NULL, NULL),
(82, 142, 3, 'ufd', 'Trainee Stipend', '2016-06-15 14:13:27', NULL, 2, NULL, NULL),
(84, 168, 53, 'im ravi', '21000-25000', '2020-08-17 11:28:50', NULL, 83, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pp_seeker_experience`
--

CREATE TABLE `pp_seeker_experience` (
  `ID` int(11) NOT NULL,
  `seeker_ID` int(11) DEFAULT NULL,
  `job_title` varchar(155) DEFAULT NULL,
  `company_name` varchar(155) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `city` varchar(40) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `responsibilities` text DEFAULT NULL,
  `dated` datetime DEFAULT NULL,
  `flag` varchar(10) DEFAULT NULL,
  `old_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pp_seeker_experience`
--

INSERT INTO `pp_seeker_experience` (`ID`, `seeker_ID`, `job_title`, `company_name`, `start_date`, `end_date`, `city`, `country`, `responsibilities`, `dated`, `flag`, `old_id`) VALUES
(1, 9, 'test', 'testete', '2012-02-16', NULL, 'New york', 'United States of America', NULL, '2016-03-12 02:10:41', NULL, NULL),
(2, 15, 'Edmar Paz Fotográfias', 'Fotos & Vídeos', '1970-01-01', NULL, 'Tamandaré', 'Brazil', NULL, '2016-03-28 18:44:09', NULL, NULL),
(3, 18, 'dedededededde', 'deddededededdeded', '2016-03-28', '2016-03-31', 'lima', 'Bahamas', NULL, '2016-03-28 20:02:38', NULL, NULL),
(4, 15, 'Informática', 'Provedor Web', '2011-05-11', NULL, 'Tamandaré', 'Brazil', NULL, '2016-03-28 23:48:52', NULL, NULL),
(5, 15, 'Locutor', 'Rádio Estrela do Mar FM', '2013-06-05', NULL, 'Tamandaré', 'Brazil', NULL, '2016-03-28 23:49:34', NULL, NULL),
(6, 25, 'dfdf', 'fdfvd', '2016-03-09', NULL, 'columbus', 'United States of America', NULL, '2016-03-29 13:42:28', NULL, NULL),
(7, 25, 'zxcvzs', 'asfasdf', '2016-03-23', NULL, 'yfutu', 'United States of America', NULL, '2016-03-29 13:44:11', NULL, NULL),
(8, 32, 'Test123113', 'Softrait Technologies', '2016-04-01', '2016-04-01', 'palakkad', 'India', NULL, '2016-03-31 23:37:21', NULL, NULL),
(9, 36, 'Web Designer', 'Master Tech', '2016-04-11', NULL, 'Dhaka', 'Bangladesh', NULL, '2016-04-02 03:21:27', NULL, NULL),
(10, 36, 'Web Designer Sr', 'Master Tech Ltd', '2016-04-11', '2016-04-04', 'Dhaka', 'Bangladesh', NULL, '2016-04-02 03:22:09', NULL, NULL),
(11, 41, 'job 11', 'company  job', '2016-04-13', '2016-04-20', 'sksksks', 'Angola', NULL, '2016-04-03 02:55:23', NULL, NULL),
(12, 41, 'test', 's2m', '2016-02-02', '2016-04-13', 'casa', 'Antigua & Barbuda', NULL, '2016-04-03 02:55:50', NULL, NULL),
(13, 59, 'PHP developer', 'abc', '2015-06-02', NULL, 'mangalore', 'Afganistan', NULL, '2016-04-08 16:05:27', NULL, NULL),
(14, 74, 'gfdfg', 'fdtyytr', '2016-03-10', NULL, 'fdf', 'Croatia', NULL, '2016-04-15 11:41:43', NULL, NULL),
(15, 75, 'Sr Test Engineer', 'Aadithya Visuals Pvt Ltd', '2011-11-01', '2015-12-16', 'Bangalore', 'India', NULL, '2016-04-15 15:25:12', NULL, NULL),
(16, 75, 'Sr Test Engineer', 'Samsung India Software Operations', '2006-07-07', '2011-10-05', 'Bangalore', 'India', NULL, '2016-04-15 15:26:04', NULL, NULL),
(17, 92, 'test', 'the testing company', '2016-05-03', NULL, 'cairo', 'Egypt', NULL, '2016-05-02 11:56:32', NULL, NULL),
(18, 95, 'anything else', 'home', '2016-05-10', NULL, 'karachi', 'Afganistan', NULL, '2016-05-04 18:50:51', NULL, NULL),
(19, 109, 'dsfsd', 'dsfds', '2016-05-05', NULL, 'paris', 'Afganistan', NULL, '2016-05-13 20:28:58', NULL, NULL),
(20, 128, 'vbbvb', 'bvbvbv', '2016-05-03', '2016-05-01', 'bbb', 'Afganistan', NULL, '2016-05-26 00:22:05', NULL, NULL),
(21, 128, 'bvbvb', 'bbvbv', '2016-05-04', NULL, 'bvbvb', 'Afganistan', NULL, '2016-05-26 00:22:26', NULL, NULL),
(22, 132, 'dsadasd', 'sadsads', '2016-05-10', '2016-05-11', 'lima', 'Peru', NULL, '2016-06-01 03:45:22', NULL, NULL),
(23, 136, 'IT specialist', 'koko', '2016-06-01', '2016-07-25', 'wari', 'American Samoa', NULL, '2016-06-09 19:19:49', NULL, NULL),
(24, 142, 'dgh', 'dgh', '2016-06-15', NULL, 'dgh', 'Afganistan', NULL, '2016-06-15 14:09:28', NULL, NULL),
(25, 123, 'designer', 'avb', '2016-06-01', '2016-06-07', 'bairoot', 'Algeria', NULL, '2016-06-17 02:40:41', NULL, NULL),
(48, 172, 'IT', 'L&T', '2012-08-17', NULL, 'JHARKHAND', 'India', NULL, '2020-08-26 11:41:35', NULL, NULL),
(31, 169, 'cs', 'nit', '2015-04-14', '2019-04-13', 'hyd', 'India', NULL, '2020-08-20 15:33:38', NULL, NULL),
(53, 232, 'web Developer', 'NareshIt', '2016-09-09', NULL, 'Hyd', 'India', NULL, '2020-09-04 10:56:34', NULL, NULL),
(49, 172, 'hardware', 'L&T', '2017-08-03', NULL, 'hyd', 'India', NULL, '2020-08-26 14:05:45', NULL, NULL),
(50, 172, 'hardware', 'L&T', '2017-08-03', NULL, 'hyd', 'India', NULL, '2020-08-26 14:05:47', NULL, NULL),
(51, 219, 'java', 'infotech', '2014-06-11', NULL, 'dhanbad', 'India', NULL, '2020-09-01 16:34:41', NULL, NULL),
(52, 228, 'IT', 'IT DHN', '2020-09-09', NULL, 'DHN', 'India', NULL, '2020-09-03 17:53:01', NULL, NULL),
(42, 171, 'php developer', 'TECH MEHENDRA', '2016-08-18', '2020-08-13', 'HYDERABAD', 'India', NULL, '2020-08-25 16:42:57', NULL, NULL),
(43, 169, 'NJN', 'L&T', '2014-08-12', '2020-08-08', 'GTH', 'India', NULL, '2020-08-25 17:40:45', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pp_seeker_ph`
--

CREATE TABLE `pp_seeker_ph` (
  `ID` int(11) NOT NULL,
  `seeker_ID` int(11) DEFAULT NULL,
  `is_uploaded_ph` enum('no','yes') DEFAULT 'no',
  `file_name` varchar(155) DEFAULT NULL,
  `upload_name` varchar(40) DEFAULT NULL,
  `dated` datetime DEFAULT NULL,
  `is_default_ph` enum('no','yes') DEFAULT 'no'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pp_seeker_ph`
--

INSERT INTO `pp_seeker_ph` (`ID`, `seeker_ID`, `is_uploaded_ph`, `file_name`, `upload_name`, `dated`, `is_default_ph`) VALUES
(1, 186, 'yes', 'neha-186.docx', NULL, '2020-08-31 16:23:05', 'no'),
(29, 234, 'yes', 'gfgfgfhghgfgh-234.pdf', NULL, '2020-09-07 12:38:03', 'no'),
(3, 190, 'yes', 'rahul-190.docx', NULL, '2020-09-01 09:27:30', 'no'),
(4, 192, 'yes', 'new-account-192.docx', NULL, '2020-09-01 10:01:07', 'no'),
(5, NULL, 'yes', '', NULL, NULL, 'no'),
(6, NULL, 'yes', '', NULL, NULL, 'no'),
(7, NULL, 'yes', '', NULL, NULL, 'no'),
(8, NULL, 'yes', '', NULL, NULL, 'no'),
(9, 194, 'yes', 'account-194.docx', NULL, '2020-09-01 11:44:05', 'no'),
(10, 196, 'yes', 'santosh-196.docx', NULL, '2020-09-01 11:52:35', 'no'),
(11, 198, 'yes', 'demo-198.docx', NULL, '2020-09-01 12:01:40', 'no'),
(12, 200, 'yes', 'demo-200.docx', NULL, '2020-09-01 12:12:40', 'no'),
(13, 202, 'yes', 'jeet-202.docx', NULL, '2020-09-01 12:51:28', 'no'),
(14, 204, 'yes', 'test-204.docx', NULL, '2020-09-01 14:21:00', 'no'),
(15, 205, 'yes', 'test-205.pdf', NULL, '2020-09-01 14:50:04', 'no'),
(16, 212, 'yes', 'test-212.pdf', NULL, '2020-09-01 15:10:08', 'no'),
(17, 214, 'yes', 'test-214.pdf', NULL, '2020-09-01 15:15:00', 'no'),
(18, 216, 'yes', 'ravi-216.pdf', NULL, '2020-09-01 15:19:52', 'no'),
(19, 219, 'yes', 'hello-219.pdf', NULL, '2020-09-01 15:23:17', 'no'),
(20, 221, 'yes', 'test-221.pdf', NULL, '2020-09-02 11:41:27', 'no'),
(21, 223, 'yes', 'test-223.pdf', NULL, '2020-09-02 12:14:09', 'no'),
(22, NULL, 'yes', 'demo-.pdf', NULL, '2020-09-02 12:34:23', 'no'),
(28, 233, 'yes', 'student-233.pdf', NULL, '2020-09-07 12:30:45', 'no'),
(24, 229, 'yes', 'demo-229.pdf', NULL, '2020-09-02 14:10:35', 'no'),
(25, 230, 'yes', 'dfbd-230.pdf', NULL, '2020-09-02 14:28:43', 'no'),
(26, 231, 'yes', 'abcdefg-231.pdf', NULL, '2020-09-02 14:37:19', 'no'),
(30, 235, 'yes', 'mmmmm-235.pdf', NULL, '2020-09-07 12:52:01', 'no'),
(31, 236, 'yes', 'lgj-fh-as-236.pdf', NULL, '2020-09-07 14:26:25', 'no'),
(33, 238, 'yes', 'today-238.pdf', NULL, '2020-09-07 15:17:43', 'no'),
(34, 239, 'yes', 'today-239.pdf', NULL, '2020-09-07 15:20:42', 'no'),
(40, 237, 'yes', 'fsfsd-JOBPORTAL-2371599474518.pdf', NULL, '2020-09-07 15:58:38', 'no'),
(41, 240, 'yes', 'jkhf-240.pdf', NULL, '2020-09-07 16:00:36', 'no'),
(42, 241, 'yes', 'jkhf-241.pdf', NULL, '2020-09-07 16:04:51', 'no'),
(43, 242, 'yes', 'jkhf-242.pdf', NULL, '2020-09-07 16:08:31', 'no'),
(44, 243, 'yes', 'jkhf-243.pdf', NULL, '2020-09-07 16:09:34', 'no'),
(45, 244, 'yes', 'dfgdfg-244.pdf', NULL, '2020-09-07 16:16:34', 'no'),
(46, 244, 'yes', 'dfgdfg-JOBPORTAL-2441599475614.pdf', NULL, '2020-09-07 16:16:54', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `pp_seeker_resumes`
--

CREATE TABLE `pp_seeker_resumes` (
  `ID` int(11) NOT NULL,
  `seeker_ID` int(11) DEFAULT NULL,
  `is_uploaded_resume` enum('no','yes') DEFAULT 'no',
  `file_name` varchar(155) DEFAULT NULL,
  `resume_name` varchar(40) DEFAULT NULL,
  `dated` datetime DEFAULT NULL,
  `is_default_resume` enum('no','yes') DEFAULT 'no'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pp_seeker_resumes`
--

INSERT INTO `pp_seeker_resumes` (`ID`, `seeker_ID`, `is_uploaded_resume`, `file_name`, `resume_name`, `dated`, `is_default_resume`) VALUES
(1, 8, 'yes', 'test-test-8.docx', NULL, '2016-03-12 01:44:43', 'no'),
(2, 9, 'yes', 'michel-jen-9.docx', NULL, '2016-03-12 01:51:56', 'no'),
(4, 10, 'yes', 'jhony-man-JOBPORTAL-101457770070.docx', NULL, '2016-03-12 13:07:50', 'no'),
(5, 11, 'yes', 'kganxx-11.docx', NULL, '2016-03-28 14:11:09', 'no'),
(6, 12, 'yes', 'kacykos-12.jpg', NULL, '2016-03-28 14:46:29', 'no'),
(7, 13, 'yes', 'ajay-13.txt', NULL, '2016-03-28 17:40:38', 'no'),
(8, 14, 'yes', 'peter-sturm-14.pdf', NULL, '2016-03-28 18:18:22', 'no'),
(9, 15, 'yes', 'marcos-15.docx', NULL, '2016-03-28 18:33:36', 'no'),
(10, 16, 'yes', 'brunaoxp-16.jpg', NULL, '2016-03-28 19:11:36', 'no'),
(11, 17, 'yes', 'hotel-mira-serra-17.pdf', NULL, '2016-03-28 19:36:29', 'no'),
(12, 18, 'yes', 'pedro-lianzres-18.pdf', NULL, '2016-03-28 20:00:45', 'no'),
(13, 19, 'yes', 'shamim-19.docx', NULL, '2016-03-29 00:05:47', 'no'),
(14, 20, 'yes', 'laakz-laakz-20.jpg', NULL, '2016-03-29 00:59:45', 'no'),
(15, 21, 'yes', 'haris-21.jpg', NULL, '2016-03-29 02:56:12', 'no'),
(16, 22, 'yes', 'asasas-22.doc', NULL, '2016-03-29 03:13:18', 'no'),
(17, 23, 'yes', 'joe-employee-23.docx', NULL, '2016-03-29 05:51:44', 'no'),
(18, 24, 'yes', 'deneme-24.jpg', NULL, '2016-03-29 09:51:00', 'no'),
(19, 25, 'yes', 'asd-asd-a-25.pdf', NULL, '2016-03-29 13:41:11', 'no'),
(20, 26, 'yes', 'leandro-26.pdf', NULL, '2016-03-29 22:40:47', 'no'),
(21, 27, 'yes', 'gianfranco-devico-27.txt', NULL, '2016-03-31 06:04:32', 'no'),
(22, 28, 'yes', 'Özcan-odaba?o?lu-28.jpg', NULL, '2016-03-31 11:22:24', 'no'),
(23, 29, 'yes', 'anshu-29.pdf', NULL, '2016-03-31 11:39:58', 'no'),
(24, 30, 'yes', 'nong-tien-thanh-30.jpg', NULL, '2016-03-31 19:56:04', 'no'),
(25, 32, 'yes', 'abdu-ravoof-k-32.pdf', NULL, '2016-03-31 23:35:26', 'no'),
(26, 33, 'yes', 'web-creative-33.doc', NULL, '2016-04-01 01:25:56', 'no'),
(27, 35, 'yes', 'v-babu-babu-35.doc', NULL, '2016-04-01 17:55:19', 'no'),
(28, 36, 'yes', 'saiful-36.docx', NULL, '2016-04-02 03:18:45', 'no'),
(29, 37, 'yes', 'oswaldo-cabrera-37.jpg', NULL, '2016-04-02 05:49:14', 'no'),
(30, 38, 'yes', 'mark-38.jpg', NULL, '2016-04-02 12:11:59', 'no'),
(31, 41, 'yes', 'hicham-rassif-41.txt', NULL, '2016-04-03 02:52:58', 'no'),
(32, 42, 'yes', 'sebastian-42.pdf', NULL, '2016-04-03 03:31:29', 'no'),
(33, 47, 'yes', 'test-user-47.doc', NULL, '2016-04-03 03:44:55', 'no'),
(34, 48, 'yes', 'dfasdfas-48.docx', NULL, '2016-04-03 15:31:36', 'no'),
(35, 49, 'yes', 'paderborn-49.jpg', NULL, '2016-04-03 20:11:00', 'no'),
(36, 50, 'yes', 'rabson-takavarasha-50.PDF', NULL, '2016-04-04 13:55:06', 'no'),
(37, 51, 'yes', 'shrikant-51.docx', NULL, '2016-04-04 21:08:00', 'no'),
(38, 52, 'yes', 'gombosvren-52.docx', NULL, '2016-04-05 13:58:14', 'no'),
(39, 53, 'yes', 'mateo-agudelo-53.pdf', NULL, '2016-04-06 08:12:16', 'no'),
(40, 54, 'yes', 'sayed-ahmed-54.jpg', NULL, '2016-04-06 12:04:09', 'no'),
(41, 55, 'yes', 'nikoloz-natssarishvili-55.doc', NULL, '2016-04-06 18:21:12', 'no'),
(42, 56, 'yes', 'al-emran-56.pdf', NULL, '2016-04-07 02:08:30', 'no'),
(44, 58, 'yes', 'festus-iipito-58.pdf', NULL, '2016-04-08 16:00:17', 'no'),
(45, 59, 'yes', 'kom-59.jpg', NULL, '2016-04-08 16:02:20', 'no'),
(46, 60, 'yes', 'jay-alame-60.jpg', NULL, '2016-04-08 23:10:17', 'no'),
(47, 61, 'yes', 'mio-nome-completo-61.docx', NULL, '2016-04-10 00:22:17', 'no'),
(48, 65, 'yes', 'noufal-65.jpg', NULL, '2016-04-11 17:19:58', 'no'),
(49, 66, 'yes', 'ayhan-ergun-66.jpg', NULL, '2016-04-12 01:34:18', 'no'),
(50, 67, 'yes', 'fernando-67.jpg', NULL, '2016-04-12 10:34:48', 'no'),
(51, 68, 'yes', 'kharl-68.docx', NULL, '2016-04-12 14:48:43', 'no'),
(52, 69, 'yes', 'fullname-69.jpg', NULL, '2016-04-13 18:34:11', 'no'),
(53, 70, 'yes', 'habib-jiwan-70.docx', NULL, '2016-04-13 21:39:42', 'no'),
(54, 71, 'yes', 'salih-karadeniz-71.pdf', NULL, '2016-04-14 15:13:54', 'no'),
(55, 74, 'yes', 'std-74.pdf', NULL, '2016-04-15 11:39:43', 'no'),
(56, 75, 'yes', 'arjun-kumar-75.doc', NULL, '2016-04-15 15:22:03', 'no'),
(57, 76, 'yes', 'nagendra-yadav-76.doc', NULL, '2016-04-15 17:31:45', 'no'),
(58, 78, 'yes', 'deneme-deneme-78.txt', NULL, '2016-04-16 12:05:57', 'no'),
(59, 79, 'yes', 'md-mahabub-alam-rubel-79.pdf', NULL, '2016-04-17 08:31:40', 'no'),
(60, 80, 'yes', 'alex-kehr-80.pdf', NULL, '2016-04-17 23:21:48', 'no'),
(61, 81, 'yes', 'gouse-81.docx', NULL, '2016-04-18 15:25:15', 'no'),
(62, 82, 'yes', 'doan-82.pdf', NULL, '2016-04-19 12:05:13', 'no'),
(63, 83, 'yes', 'tiago-83.doc', NULL, '2016-04-20 08:54:24', 'no'),
(64, 84, 'yes', 'vehbi-84.jpg', NULL, '2016-04-22 03:42:30', 'no'),
(65, 85, 'yes', 'vipin-agarwal-85.docx', NULL, '2016-04-22 18:40:13', 'no'),
(66, 86, 'yes', 'sergey-86.docx', NULL, '2016-04-23 03:06:40', 'no'),
(67, 87, 'yes', 'deli-atoia-abuda-87.pdf', NULL, '2016-04-24 22:26:40', 'no'),
(69, 89, 'yes', 'max-azarcon-89.pdf', NULL, '2016-04-26 01:55:22', 'no'),
(70, 90, 'yes', 'abcd-zyx-90.jpg', NULL, '2016-04-28 15:03:00', 'no'),
(71, 91, 'yes', 'stanislas-bellot-91.pdf', NULL, '2016-05-02 06:45:03', 'no'),
(72, 92, 'yes', 'testat-92.pdf', NULL, '2016-05-02 11:53:02', 'no'),
(73, 93, 'yes', 'qartes-93.jpg', NULL, '2016-05-03 05:54:41', 'no'),
(74, 94, 'yes', 'sayali-94.jpg', NULL, '2016-05-03 10:35:10', 'no'),
(75, 95, 'yes', 'john-doe-95.pdf', NULL, '2016-05-04 16:39:17', 'no'),
(76, 97, 'yes', 'jj-97.docx', NULL, '2016-05-05 15:54:34', 'no'),
(77, 98, 'yes', 'dtest-er-98.docx', NULL, '2016-05-05 22:55:28', 'no'),
(78, 99, 'yes', 'kevin-de-la-horra-99.txt', NULL, '2016-05-06 01:02:08', 'no'),
(79, 100, 'yes', 'sean-brogan-100.jpg', NULL, '2016-05-06 18:22:12', 'no'),
(80, 102, 'yes', 'u-alan-102.pdf', NULL, '2016-05-07 22:39:29', 'no'),
(81, 103, 'yes', 'markettom-103.docx', NULL, '2016-05-08 11:15:53', 'no'),
(82, 105, 'yes', 'teste-105.doc', NULL, '2016-05-10 03:10:00', 'no'),
(83, 106, 'yes', 'alex-muurphy-106.doc', NULL, '2016-05-11 11:34:48', 'no'),
(84, 107, 'yes', 'gandhi-107.pdf', NULL, '2016-05-12 07:34:23', 'no'),
(85, 108, 'yes', 'dddddddddddddddd-108.doc', NULL, '2016-05-12 23:30:55', 'no'),
(86, 109, 'yes', 'jean-val-109.docx', NULL, '2016-05-13 20:24:42', 'no'),
(87, 110, 'yes', 'raakesh-kumar-110.pdf', NULL, '2016-05-13 21:13:28', 'no'),
(88, 111, 'yes', 'shaik-jilani-111.doc', NULL, '2016-05-15 01:20:46', 'no'),
(89, 113, 'yes', 'kevin-de-la-horra-113.docx', NULL, '2016-05-15 16:32:39', 'no'),
(90, 114, 'yes', 'asmaa-114.jpg', NULL, '2016-05-16 17:04:46', 'no'),
(91, 115, 'yes', 'test-115.jpg', NULL, '2016-05-16 17:17:43', 'no'),
(92, 116, 'yes', 'biniyam-116.doc', NULL, '2016-05-16 17:23:56', 'no'),
(93, 117, 'yes', 'abraiz-khan-117.docx', NULL, '2016-05-16 22:51:31', 'no'),
(94, 118, 'yes', 'razor-118.jpg', NULL, '2016-05-17 03:29:40', 'no'),
(95, 119, 'yes', 'anibal-centurion-119.jpg', NULL, '2016-05-17 17:30:52', 'no'),
(96, 121, 'yes', 'patryk-kloz-121.docx', NULL, '2016-05-17 18:31:32', 'no'),
(97, 121, 'yes', 'patryk-kloz-JOBPORTAL-1211463491984.docx', NULL, '2016-05-17 18:33:04', 'no'),
(98, 122, 'yes', 'job-seeker-122.txt', NULL, '2016-05-19 00:31:47', 'no'),
(99, 123, 'yes', 'admin-123.pdf', NULL, '2016-05-22 12:39:40', 'no'),
(100, 124, 'yes', 'parkkichul-124.jpg', NULL, '2016-05-23 15:42:06', 'no'),
(101, 125, 'yes', 'ina-125.pdf', NULL, '2016-05-24 17:36:03', 'no'),
(102, 126, 'yes', 'pepito-piguave-126.doc', NULL, '2016-05-25 00:32:51', 'no'),
(103, 127, 'yes', 'shiva-127.docx', NULL, '2016-05-25 17:07:07', 'no'),
(105, 129, 'yes', 'altan-kastalmis-129.jpg', NULL, '2016-05-27 19:04:49', 'no'),
(106, 130, 'yes', 'gray-jumba-130.docx', NULL, '2016-05-28 22:59:08', 'no'),
(107, 132, 'yes', 'neyber-becerra-zapata-132.pdf', NULL, '2016-06-01 03:39:36', 'no'),
(108, 133, 'yes', 'full-name-133.doc', NULL, '2016-06-02 12:50:25', 'no'),
(109, 134, 'yes', 'lvcmuz-134.docx', NULL, '2016-06-08 15:05:33', 'no'),
(110, 135, 'yes', 'zgh-135.docx', NULL, '2016-06-08 23:40:44', 'no'),
(111, 136, 'yes', 'larry-barry-136.docx', NULL, '2016-06-09 18:59:07', 'no'),
(112, 137, 'yes', 'casper-ved-137.jpg', NULL, '2016-06-09 23:55:17', 'no'),
(113, 138, 'yes', 'praveen-dokania-138.doc', NULL, '2016-06-10 23:41:39', 'no'),
(114, 139, 'yes', 'fdsa-139.jpg', NULL, '2016-06-11 22:01:01', 'no'),
(115, 142, 'yes', 'qsaf-142.pdf', NULL, '2016-06-15 14:06:39', 'no'),
(116, 143, 'yes', 'ansh-anand-143.docx', NULL, '2020-07-27 16:47:50', 'no'),
(117, 144, 'yes', 'app-144.pdf', NULL, '2020-07-29 12:08:41', 'no'),
(118, 145, 'yes', 'dfg-145.pdf', NULL, '2020-07-29 12:32:04', 'no'),
(119, 146, 'yes', 'file-146.pdf', NULL, '2020-07-29 12:34:23', 'no'),
(120, 147, 'yes', 'gggg-147.pdf', NULL, '2020-07-29 12:42:15', 'no'),
(121, 148, 'yes', 'ffffff-148.pdf', NULL, '2020-07-29 13:00:32', 'no'),
(122, 149, 'yes', 'ravi-kumar-149.pdf', NULL, '2020-07-29 14:14:48', 'no'),
(123, 151, 'yes', 'dsfghjk-151.pdf', NULL, '2020-07-29 14:20:31', 'no'),
(124, 152, 'yes', 'ra-152.pdf', NULL, '2020-07-29 14:28:40', 'no'),
(125, 153, 'yes', 'app-153.pdf', NULL, '2020-07-29 14:35:11', 'no'),
(126, 154, 'yes', 'ravi-154.pdf', NULL, '2020-07-29 14:45:05', 'no'),
(127, 155, 'yes', 'hexak-155.docx', NULL, '2020-07-29 15:48:01', 'no'),
(128, 156, 'yes', 'sachin-156.pdf', NULL, '2020-07-29 17:46:22', 'no'),
(129, 157, 'yes', 'despoo-157.pdf', NULL, '2020-07-30 10:41:47', 'no'),
(130, 158, 'yes', 'apppp-158.docx', NULL, '2020-07-31 14:21:35', 'no'),
(131, 159, 'yes', 'sita-159.docx', NULL, '2020-08-03 16:30:57', 'no'),
(132, 160, 'yes', 'rita-raw-160.docx', NULL, '2020-08-03 17:13:04', 'no'),
(133, 162, 'yes', 'mskj-162.docx', NULL, '2020-08-10 14:46:41', 'no'),
(134, 163, 'yes', 'mskj-163.docx', NULL, '2020-08-10 14:48:31', 'no'),
(135, 167, 'yes', 'liza-167.docx', NULL, '2020-08-11 12:28:09', 'no'),
(136, 168, 'yes', 'desrfr-168.docx', NULL, '2020-08-11 15:48:59', 'no'),
(138, 170, 'yes', 'sachin-170.docx', NULL, '2020-08-20 12:27:14', 'no'),
(139, 171, 'yes', 'ravi-kumar-171.docx', NULL, '2020-08-25 15:55:38', 'no'),
(140, 169, 'yes', 'vijay-rawani-JOBPORTAL-1691598357395.docx', NULL, '2020-08-25 17:39:55', 'no'),
(141, 172, 'yes', 'ravi-kumar-172.docx', NULL, '2020-08-26 11:40:37', 'no'),
(144, 174, 'yes', 'employee-174.docx', NULL, '2020-08-28 10:11:13', 'no'),
(143, 173, 'yes', 'rajopedia-JOBPORTAL-1731598528577.pdf', NULL, '2020-08-27 17:12:57', 'no'),
(146, 175, 'yes', 'employee-JOBPORTAL-1751598612780.pdf', NULL, '2020-08-28 16:36:20', 'no'),
(147, 175, 'yes', 'employee-JOBPORTAL-1751598612821.pdf', NULL, '2020-08-28 16:37:01', 'no'),
(148, 175, 'yes', 'employee-JOBPORTAL-1751598612843.pdf', NULL, '2020-08-28 16:37:23', 'no'),
(161, 176, 'yes', 'dev-JOBPORTAL-1761598859120.docx', NULL, '2020-08-31 13:02:00', 'no'),
(154, 177, 'yes', 'demo-177.docx', NULL, '2020-08-31 12:30:28', 'no'),
(155, 179, 'yes', 'demo-179.docx', NULL, '2020-08-31 12:32:30', 'no'),
(156, 180, 'yes', 'demo-JOBPORTAL-1801598857632.docx', NULL, '2020-08-31 12:37:12', 'no'),
(158, 181, 'yes', 'demo-181.docx', NULL, '2020-08-31 12:56:52', 'no'),
(159, 182, 'yes', 'demo-JOBPORTAL-1821598859021.docx', NULL, '2020-08-31 13:00:21', 'no'),
(162, 183, 'yes', 'neha-183.docx', NULL, '2020-08-31 16:20:22', 'no'),
(163, 184, 'yes', 'neha-184.docx', NULL, '2020-08-31 16:21:47', 'no'),
(164, 185, 'yes', 'neha-185.docx', NULL, '2020-08-31 16:23:05', 'no'),
(165, 186, 'yes', 'neha-JOBPORTAL-1861598871368.docx', NULL, '2020-08-31 16:26:08', 'no'),
(166, 187, 'yes', 'test-187.docx', NULL, '2020-08-31 16:28:40', 'no'),
(168, 188, 'yes', 'test-JOBPORTAL-1881598872588.docx', NULL, '2020-08-31 16:46:28', 'no'),
(169, 189, 'yes', 'rahul-189.docx', NULL, '2020-09-01 09:27:30', 'no'),
(170, 191, 'yes', 'new-account-191.docx', NULL, '2020-09-01 10:01:07', 'no'),
(171, 193, 'yes', 'account-193.docx', NULL, '2020-09-01 11:44:05', 'no'),
(172, 194, 'yes', 'account-JOBPORTAL-1941598941022.pdf', NULL, '2020-09-01 11:47:02', 'no'),
(173, 195, 'yes', 'santosh-195.docx', NULL, '2020-09-01 11:52:35', 'no'),
(176, 197, 'yes', 'demo-197.docx', NULL, '2020-09-01 12:01:40', 'no'),
(175, 196, 'yes', 'santosh-JOBPORTAL-1961598941718.pdf', NULL, '2020-09-01 11:58:38', 'no'),
(177, 198, 'yes', 'demo-JOBPORTAL-1981598942155.pdf', NULL, '2020-09-01 12:05:55', 'no'),
(178, 199, 'yes', 'demo-199.docx', NULL, '2020-09-01 12:12:40', 'no'),
(179, 201, 'yes', 'jeet-201.docx', NULL, '2020-09-01 12:51:28', 'no'),
(180, 202, 'yes', 'jeet-JOBPORTAL-2021598949722.pdf', NULL, '2020-09-01 14:12:02', 'no'),
(181, 203, 'yes', 'test-203.docx', NULL, '2020-09-01 14:21:00', 'no'),
(182, 204, 'yes', 'test-JOBPORTAL-2041598951938.pdf', NULL, '2020-09-01 14:48:58', 'no'),
(183, 206, 'yes', 'test-205.docx', NULL, '2020-09-01 14:50:04', 'no'),
(184, 211, 'yes', 'test-211.docx', NULL, '2020-09-01 15:10:08', 'no'),
(185, 212, 'yes', 'test-JOBPORTAL-2121598953392.docx', NULL, '2020-09-01 15:13:12', 'no'),
(186, 213, 'yes', 'test-213.docx', NULL, '2020-09-01 15:15:00', 'no'),
(187, 214, 'yes', 'test-JOBPORTAL-2141598953542.docx', NULL, '2020-09-01 15:15:42', 'no'),
(188, 217, 'yes', 'ravi-217.docx', NULL, '2020-09-01 15:19:52', 'no'),
(189, 218, 'yes', 'hello-218.docx', NULL, '2020-09-01 15:23:17', 'no'),
(190, 219, 'yes', 'hello-JOBPORTAL-2191598954025.docx', NULL, '2020-09-01 15:23:45', 'no'),
(191, 220, 'yes', 'test-220.docx', NULL, '2020-09-02 11:41:27', 'no'),
(192, 222, 'yes', 'test-222.docx', NULL, '2020-09-02 12:14:09', 'no'),
(193, 223, 'yes', 'test-JOBPORTAL-2231599029226.docx', NULL, '2020-09-02 12:17:06', 'no'),
(194, 224, 'yes', 'demo-224.docx', NULL, '2020-09-02 12:34:23', 'no'),
(195, 229, 'yes', 'demo-JOBPORTAL-2291599036068.docx', NULL, '2020-09-02 14:11:08', 'no'),
(196, 231, 'yes', 'abcdefg-231.docx', NULL, '2020-09-02 14:37:19', 'no'),
(221, 225, 'yes', 'employee-JOBPORTAL-2251599461351.pdf', NULL, '2020-09-07 12:19:11', 'no'),
(222, 233, 'yes', 'student-233.docx', NULL, '2020-09-07 12:30:45', 'no'),
(223, 234, 'yes', 'gfgfgfhghgfgh-234.docx', NULL, '2020-09-07 12:38:03', 'no'),
(224, 235, 'yes', 'mmmmm-235.pdf', NULL, '2020-09-07 12:52:01', 'no'),
(225, 236, 'yes', 'lgj-fh-as-236.docx', NULL, '2020-09-07 14:26:25', 'no'),
(230, 239, 'yes', 'today-239.pdf', NULL, '2020-09-07 15:20:42', 'no'),
(229, 238, 'yes', 'today-238.pdf', NULL, '2020-09-07 15:17:43', 'no'),
(231, 237, 'yes', 'fsfsd-JOBPORTAL-2371599473986.pdf', NULL, '2020-09-07 15:49:46', 'no'),
(233, 240, 'yes', 'jkhf-240.docx', NULL, '2020-09-07 16:00:36', 'no'),
(234, 241, 'yes', 'jkhf-241.pdf', NULL, '2020-09-07 16:04:51', 'no'),
(235, 242, 'yes', 'jkhf-242.docx', NULL, '2020-09-07 16:08:31', 'no'),
(236, 243, 'yes', 'jkhf-243.docx', NULL, '2020-09-07 16:09:34', 'no'),
(237, 244, 'yes', 'dfgdfg-244.pdf', NULL, '2020-09-07 16:16:34', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `pp_seeker_skills`
--

CREATE TABLE `pp_seeker_skills` (
  `ID` int(11) NOT NULL,
  `seeker_ID` int(11) DEFAULT NULL,
  `skill_name` varchar(155) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pp_seeker_skills`
--

INSERT INTO `pp_seeker_skills` (`ID`, `seeker_ID`, `skill_name`) VALUES
(1, 8, 'php'),
(2, 8, 'java'),
(3, 8, 'javascript'),
(4, 9, 'html'),
(5, 9, 'css'),
(6, 9, 'photoshop'),
(7, 9, 'illustrator'),
(8, 9, 'js'),
(9, 9, 'jquery'),
(10, 10, 'html'),
(11, 10, 'css'),
(12, 10, 'js'),
(13, 11, 'css'),
(14, 11, 'photoshop'),
(15, 11, 'designer'),
(16, 12, 'prawojazdy c'),
(17, 12, 'dobry zawodowo'),
(18, 12, 'xdddd d ddd'),
(19, 14, 'nothing'),
(20, 14, 'more'),
(21, 14, 'nix'),
(22, 15, 'computação'),
(23, 15, 'formatação'),
(24, 15, 'administração'),
(25, 16, '54'),
(26, 16, 'hiioyui'),
(27, 16, 'etyt'),
(28, 17, 'illustrator'),
(29, 18, 'demo'),
(30, 18, 'dedede'),
(31, 18, 'dedededede'),
(32, 15, 'illustrator'),
(33, 15, 'indesign'),
(34, 15, 'marketting'),
(35, 15, 'ms office'),
(36, 19, 'php'),
(37, 19, 'ms office'),
(38, 19, 'jquery'),
(40, 21, 'wordpress, html, css, php'),
(41, 21, 'wordpress'),
(42, 21, 'html'),
(43, 21, 'css'),
(44, 21, 'photoshop'),
(45, 22, 'asasasas'),
(46, 22, 'wewewew'),
(47, 22, 'wewewewe'),
(48, 23, 'medicl assistant'),
(49, 23, 'front office'),
(50, 23, 'back office'),
(51, 24, 'cvcddsr'),
(52, 24, 'gddss'),
(53, 25, 'web'),
(54, 25, 'food'),
(55, 25, 'ass'),
(56, 25, 'boobs'),
(57, 26, ', desenvolvedor, administrador, obras'),
(58, 28, 'php'),
(59, 28, 'illustrator'),
(60, 28, 'java'),
(61, 29, 'test, test, test'),
(62, 29, 'test'),
(63, 29, 'testtest'),
(64, 29, 'ok'),
(65, 30, 'wordpress'),
(66, 30, 'html'),
(67, 30, 'php'),
(68, 32, 'php'),
(69, 32, 'photoshop'),
(70, 32, 'java'),
(71, 33, 'php'),
(72, 33, 'html'),
(73, 33, 'css'),
(74, 35, 'test'),
(75, 35, 'testttt'),
(76, 36, 'web design'),
(77, 36, 'web development'),
(78, 36, 'trainer'),
(79, 37, 'html'),
(80, 37, 'css'),
(81, 37, 'sx'),
(82, 37, 'cmmi'),
(83, 38, 'php'),
(84, 41, 'develpper'),
(85, 41, 'djjdjdj'),
(86, 41, 'dskdkdk'),
(87, 42, 'excel'),
(88, 42, 'illustrator'),
(89, 42, 'informática'),
(90, 47, 'drgdqg'),
(91, 47, 'rqdghqesh'),
(92, 47, 'gshtsrrfghrts'),
(93, 48, 'photoshop'),
(94, 48, 'dd'),
(95, 48, 'illustrator'),
(96, 49, 'illustrator'),
(97, 49, 'java'),
(98, 49, 'html'),
(99, 51, '.net'),
(100, 52, 'web developer'),
(101, 53, 'php, html, css'),
(102, 53, 'php'),
(103, 53, 'css'),
(104, 54, 'indesign'),
(105, 54, 'marketting'),
(106, 55, 'php delvoper'),
(107, 55, 'ss'),
(108, 55, 'dd'),
(109, 57, 'php'),
(110, 57, 'jquery'),
(111, 57, 'informática'),
(112, 59, 'php'),
(113, 59, '.net'),
(114, 59, 'php developer'),
(115, 58, 'fghfgh'),
(116, 58, 'dghd'),
(117, 58, 'dghdgh'),
(118, 58, 'dghdfgh'),
(119, 60, 'design'),
(120, 60, 'skill'),
(121, 60, 'skill2'),
(122, 60, 'skill3'),
(123, 61, 'php'),
(124, 61, 'mysql'),
(125, 61, 'photoshop'),
(126, 65, 'illustrator'),
(127, 65, 'indesign'),
(128, 65, 'cms'),
(129, 65, 'online marketing'),
(130, 65, 'crestone'),
(131, 65, 'extron'),
(132, 66, 'dsd'),
(133, 66, 'ads'),
(134, 66, 'asd'),
(135, 67, 'desarrolo de software'),
(136, 67, 'diseño grafico'),
(137, 67, 'web'),
(138, 68, 'web design'),
(139, 69, 'photoshop'),
(140, 69, 'dreamweaver'),
(141, 69, 'html5'),
(142, 69, 'css3'),
(143, 70, 'programmer, designer, writer'),
(144, 70, 'programmer'),
(145, 70, 'designer'),
(146, 70, 'writer'),
(147, 74, 'php'),
(148, 74, 'css'),
(149, 74, 'html'),
(150, 75, 'embedded testing'),
(151, 75, 'printer domain'),
(152, 75, 'testing'),
(157, 76, 'html'),
(155, 76, 'js'),
(158, 76, 'css'),
(159, 78, 'php developer'),
(160, 78, 'php'),
(161, 78, 'mysql'),
(162, 79, 'html'),
(163, 79, 'css'),
(164, 80, 'marketting'),
(165, 80, 'php'),
(166, 80, 'html'),
(167, 81, 'java'),
(168, 81, 'jquery'),
(169, 81, 'informática'),
(170, 84, 'atley-t'),
(171, 85, 'web design, seo, development'),
(172, 85, 'seo'),
(173, 85, 'web design'),
(174, 86, 'php'),
(175, 86, 'word'),
(176, 86, 'ffgh'),
(177, 87, 'php'),
(178, 87, 'photoshop'),
(179, 87, 'java'),
(180, 88, 'accountant, cost accountant, acca'),
(181, 88, 'fw'),
(182, 88, 'qwefw'),
(183, 89, 'html'),
(184, 89, 'css'),
(185, 89, 'jquery'),
(186, 90, 'php;mysql'),
(187, 90, 'qqq'),
(188, 90, 'ert'),
(189, 91, 'informaticien'),
(190, 91, 'administrateur de réseau'),
(191, 91, 'developpeur'),
(192, 92, 'test'),
(193, 92, 'cctv'),
(194, 92, 'software'),
(195, 92, 'hello'),
(196, 92, 'hr'),
(197, 93, 'marketting'),
(198, 93, 'informática'),
(199, 93, 'jquery'),
(200, 93, 'mysql'),
(201, 94, 'designer'),
(202, 94, 'website developer'),
(203, 94, 'photoshop'),
(204, 95, 'yes'),
(205, 95, 'no'),
(206, 95, 'marketting'),
(207, 97, 'aaa'),
(208, 97, 'dd'),
(209, 99, 'adasd'),
(210, 99, 'asdfasdf'),
(211, 99, 'sdfgdsfg'),
(212, 100, 'sfsff'),
(213, 100, 'sdfdf'),
(214, 100, 'dfsff'),
(215, 102, 'web'),
(216, 102, 'php coder, desing'),
(217, 102, 'desin'),
(218, 103, 'php developer, php coder, php programmer, website developer, word press, java script, js, ajax etc'),
(219, 105, 'indesign'),
(220, 105, 'html'),
(221, 105, 'photoshop'),
(222, 106, 'php'),
(223, 107, 'vendedor'),
(224, 107, 'cabesero'),
(225, 107, 'tematico'),
(226, 108, 'mmmm'),
(227, 108, 'bjhbjmh'),
(228, 108, 'nkjknkjn'),
(229, 109, 'seo'),
(230, 109, 'sem'),
(231, 109, 'kick boxing'),
(232, 110, 'php,python,java'),
(233, 110, 'php'),
(234, 110, 'python'),
(235, 110, 'java'),
(236, 111, 'php'),
(237, 111, 'java'),
(238, 111, '.net'),
(239, 111, 'oracle'),
(240, 113, 'programmer'),
(241, 113, 'desings'),
(242, 113, 'informatic'),
(243, 114, 'word press'),
(244, 115, 'test'),
(245, 116, 'php developer'),
(246, 116, 'php programmer'),
(247, 116, 'website developer'),
(248, 116, 'chartered accountant'),
(249, 117, 'php,'),
(250, 117, 'wordpress'),
(251, 117, 'html'),
(252, 121, 'html'),
(255, 121, 'testing2'),
(254, 121, 'testing'),
(256, 121, 'java'),
(257, 122, 'php'),
(258, 122, '.net'),
(259, 122, 'java'),
(260, 123, 'indesign'),
(305, 123, 'photoshop'),
(262, 124, 'sdfdsf'),
(263, 124, 'dsfdsf'),
(264, 124, 'dsfdsfff'),
(265, 124, 'ssdfdsfddddddd'),
(266, 125, 'no'),
(267, 125, 'dsg'),
(268, 126, 'html5'),
(269, 126, 'css3'),
(270, 126, 'php'),
(271, 127, 'java'),
(272, 128, 'php'),
(273, 128, 'java'),
(274, 128, 'indesign'),
(275, 129, 'cv'),
(276, 129, 'computer'),
(277, 129, 'php'),
(278, 130, 'banker'),
(279, 130, 'accountant'),
(280, 130, 'analyst'),
(281, 132, 'marketting'),
(282, 132, 'indesign'),
(283, 132, 'photoshop'),
(284, 132, 'php'),
(285, 133, 'jkjkjkjkj'),
(286, 133, 'hjhjhj'),
(287, 133, 'kkkk'),
(289, 135, 'illustrator'),
(290, 136, 'indesing'),
(291, 136, 'photoshop'),
(292, 136, 'graphic designer'),
(293, 137, 'html'),
(294, 137, 'css'),
(295, 137, 'java'),
(296, 138, 'html'),
(297, 138, 'php'),
(298, 138, 'css'),
(299, 139, 'html'),
(300, 139, 'css'),
(301, 139, 'java'),
(302, 142, 'xml'),
(303, 142, 'java'),
(304, 142, 'php'),
(306, 123, 'css'),
(307, 114, 'java'),
(308, 114, 'php'),
(309, 143, 'dca'),
(310, 143, '.net'),
(311, 143, 'ms office'),
(312, 144, 'mvaz'),
(313, 144, 'oji'),
(314, 144, 'gfdg'),
(315, 145, 'help'),
(316, 145, 'me'),
(317, 145, 'hye'),
(318, 146, 'c'),
(319, 146, 'c3'),
(320, 146, 'c33'),
(321, 147, 'gtgg'),
(322, 147, 'gdfgg'),
(323, 147, 'dgdfgdfg'),
(324, 148, 'jhjhj'),
(325, 148, 'fghfghfhg'),
(326, 148, 'fghfgdhdg'),
(327, 149, '455'),
(328, 149, 'hgfhh'),
(329, 149, 'hfgh'),
(330, 152, 'dsfsdgfgfdg'),
(331, 152, 'dfgdfgdfggfd'),
(332, 152, 'dfgfdhgfdh'),
(333, 153, 'asdfg'),
(334, 153, 'dsgfdsgd'),
(335, 153, 'fdhgfdh'),
(336, 154, 'hhhhh'),
(337, 154, 'hhhhhhh'),
(338, 154, 'hhhhhhhhhhh'),
(339, 155, 'qa'),
(340, 155, 'testing'),
(341, 155, 'ut'),
(342, 156, 'gt'),
(343, 156, 'cs'),
(344, 156, 'it'),
(345, 157, 'fvf'),
(346, 157, 'vfx'),
(347, 157, 'animation'),
(348, 158, 'vv'),
(349, 158, 'bb'),
(350, 158, 'bbh'),
(351, 159, 'jjjjjj'),
(352, 159, 'jjjjjjjjjj'),
(353, 159, 'jkkkkkkkk'),
(354, 160, 'ghjjfjfjf'),
(355, 160, 'gjgjghjfgj'),
(356, 160, 'urturjgf'),
(357, 163, 'hdhdhdh'),
(358, 163, 'dfhfhhf'),
(359, 163, 'jhhc'),
(360, 167, 'gfhdsf'),
(361, 167, 'bfg'),
(362, 167, 'dfg'),
(363, 168, 'vgcd'),
(364, 168, 'dc'),
(365, 168, 'ds'),
(370, 170, 'php'),
(374, 169, 'b-tech/be'),
(373, 169, 'ba'),
(371, 170, 'c'),
(372, 170, 'c++'),
(375, 169, 'mca'),
(376, 169, 'c'),
(377, 169, 'c++'),
(378, 169, 'html'),
(379, 169, 'css'),
(380, 171, 'java'),
(381, 171, 'php'),
(382, 171, 'c'),
(383, 172, 'java'),
(384, 172, 'php'),
(385, 172, 'css'),
(386, 172, 'html'),
(387, 173, 'php'),
(388, 173, 'mysql'),
(389, 173, 'nosql'),
(390, 174, '.net'),
(391, 174, 'js'),
(392, 174, 'ajex'),
(393, 175, 'java'),
(394, 175, 'c'),
(395, 175, 'c++'),
(396, 176, 'ba'),
(397, 176, 'bac'),
(398, 176, 'bca'),
(399, 180, 'aa'),
(400, 180, 'ss'),
(401, 180, 'dd'),
(402, 182, 'a'),
(403, 182, 'd'),
(404, 182, 'v'),
(405, 186, 'g'),
(406, 186, 'h'),
(407, 186, 'j'),
(408, 188, 'c'),
(409, 188, 'css'),
(410, 188, 'java'),
(411, 187, 'a'),
(412, 187, 'd'),
(413, 187, 'f'),
(414, 190, 'd'),
(415, 190, 'g'),
(416, 190, 'b'),
(417, 192, 'r'),
(418, 192, 't'),
(419, 192, 'y'),
(420, 192, 'indesign'),
(421, 192, 'html'),
(422, 192, 'css'),
(423, 191, 'java'),
(424, 191, 'c++'),
(425, 191, '12th'),
(426, 189, 'c++'),
(427, 189, 'c'),
(428, 189, 'css'),
(429, 194, 'java'),
(430, 194, 'web'),
(431, 194, 'ms office'),
(432, 196, 'web'),
(433, 196, 'ms office'),
(434, 196, 'mysql'),
(435, 198, 'java'),
(436, 198, 'marketting'),
(437, 198, 'ms office'),
(438, 197, 'web'),
(439, 197, 'graduation'),
(440, 197, 'ms office'),
(441, 200, 'web'),
(442, 200, 'c++'),
(443, 200, 'graduation'),
(444, 202, 'c++'),
(445, 202, 'css'),
(446, 202, 'html'),
(447, 204, 'css'),
(448, 204, 'html'),
(449, 204, 'python'),
(450, 206, 'web'),
(451, 206, 'ms office'),
(452, 206, 'mysql'),
(453, 212, 'graduation'),
(454, 212, 'web'),
(455, 212, 'ms office'),
(456, 214, 'java'),
(457, 214, 'jquery'),
(458, 214, 'graduation'),
(459, 217, 'php'),
(460, 217, 'js'),
(461, 217, 'css'),
(462, 219, 'js'),
(463, 219, 'jquery'),
(464, 219, 'php'),
(465, 221, 'c'),
(466, 221, 'css'),
(467, 221, 'java'),
(468, 223, 'web'),
(469, 223, 'ms office'),
(470, 223, 'mysql'),
(471, 229, 'web'),
(472, 229, 'marketting'),
(473, 229, 'ms office'),
(474, 230, 'sdgf'),
(475, 230, 'ssdg'),
(476, 230, 'sdg'),
(477, 231, 'ddfg'),
(478, 231, 'dfg'),
(479, 231, 'dfgfdgfd'),
(480, 224, 'v'),
(481, 224, 'b'),
(482, 224, 'n'),
(483, 228, 'g'),
(484, 228, 'b'),
(485, 228, 'n'),
(486, 232, 'software developer'),
(487, 232, 'web'),
(488, 232, 'it'),
(489, 225, 'gg'),
(490, 225, 'general'),
(491, 225, 'jquery'),
(492, 225, 'graduation'),
(493, 233, ';kjiu'),
(494, 233, 'lkbuhb'),
(495, 233, 'ohjni'),
(496, 237, 'vf'),
(497, 237, 'nb'),
(498, 237, 'mb'),
(499, 244, 'dy'),
(500, 244, 'dfh'),
(501, 244, 'dfhdf');

-- --------------------------------------------------------

--
-- Table structure for table `pp_sessions`
--

CREATE TABLE `pp_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `user_data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pp_sessions`
--

INSERT INTO `pp_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('60916497f1cfa6952063696d74869116', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.135 Safari/537.36', 1599479612, 'a:13:{s:7:\"cptcode\";s:4:\"X3FD\";s:10:\"user_email\";s:14:\"fghgf@fghj.com\";s:10:\"first_name\";s:6:\"dfgdfg\";s:20:\"back_from_user_login\";s:0:\"\";s:7:\"user_id\";i:244;s:4:\"slug\";s:0:\"\";s:9:\"last_name\";s:0:\"\";s:13:\"is_user_login\";b:1;s:13:\"is_job_seeker\";b:1;s:11:\"is_employer\";b:0;s:8:\"admin_id\";s:1:\"1\";s:4:\"name\";s:5:\"admin\";s:14:\"is_admin_login\";b:1;}'),
('a481608435cd820bb7b40d60249410a2', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', 1599478047, 'a:3:{s:9:\"user_data\";s:0:\"\";s:20:\"back_from_user_login\";s:24:\"candidate/465f3d62596259\";s:7:\"cptcode\";s:4:\"X3FD\";}'),
('d6f45ad31f98cc02eadeaf18741e0f63', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', 1599478346, '');

-- --------------------------------------------------------

--
-- Table structure for table `pp_settings`
--

CREATE TABLE `pp_settings` (
  `ID` int(11) NOT NULL,
  `emails_per_hour` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pp_settings`
--

INSERT INTO `pp_settings` (`ID`, `emails_per_hour`) VALUES
(1, 300);

-- --------------------------------------------------------

--
-- Table structure for table `pp_skills`
--

CREATE TABLE `pp_skills` (
  `ID` int(11) NOT NULL,
  `skill_name` varchar(40) DEFAULT NULL,
  `industry_ID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pp_skills`
--

INSERT INTO `pp_skills` (`ID`, `skill_name`, `industry_ID`) VALUES
(1, 'html', NULL),
(2, 'php', NULL),
(3, 'js', NULL),
(4, '.net', NULL),
(5, 'css', NULL),
(6, 'jquery', NULL),
(7, 'java', NULL),
(8, 'photoshop', NULL),
(9, 'illustrator', NULL),
(10, 'Indesign', NULL),
(11, 'mysql', NULL),
(12, 'Ms Office', NULL),
(13, 'Marketting', NULL),
(14, 'informática', NULL),
(15, 'web', NULL),
(16, 'indesing', NULL),
(17, 'developer', NULL),
(18, 'graduation', NULL),
(19, 'b-tech/be', NULL),
(20, 'mca', NULL),
(21, '12th', NULL),
(22, 'c', NULL),
(23, 'c++', NULL),
(24, 'v', NULL),
(25, 'general', NULL),
(26, 'sc', NULL),
(27, 'st', NULL),
(28, 'af', NULL),
(29, 'software developer', NULL),
(30, 'web development', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pp_stories`
--

CREATE TABLE `pp_stories` (
  `ID` int(11) NOT NULL,
  `seeker_ID` int(11) NOT NULL,
  `is_featured` enum('yes','no') DEFAULT 'no',
  `sts` enum('active','inactive') DEFAULT 'inactive',
  `title` varchar(250) DEFAULT NULL,
  `story` text DEFAULT NULL,
  `dated` datetime DEFAULT NULL,
  `ip_address` varchar(40) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_gallery`
--

CREATE TABLE `tbl_gallery` (
  `ID` int(11) NOT NULL,
  `image_caption` varchar(150) DEFAULT NULL,
  `image_name` varchar(155) DEFAULT NULL,
  `dated` datetime DEFAULT NULL,
  `sts` enum('inactive','active') DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_gallery`
--

INSERT INTO `tbl_gallery` (`ID`, `image_caption`, `image_name`, `dated`, `sts`) VALUES
(1, 'Test', 'portfolio-2.jpg', '2015-09-05 18:16:41', 'active'),
(2, '', 'portfolio-1.jpg', '2015-09-05 21:17:59', 'active'),
(3, '', 'portfolio-3.jpg', '2015-09-05 21:22:19', 'active'),
(4, '', 'portfolio-6.jpg', '2015-09-05 21:22:29', 'active'),
(5, '', 'portfolio-7.jpg', '2015-09-05 21:22:38', 'active'),
(6, '', 'portfolio-8.jpg', '2015-09-05 21:22:53', 'active'),
(7, '', 'portfolio-9.jpg', '2015-09-05 21:23:05', 'active'),
(8, 'Walk with the Queen... But be careful!', 'portfolio-10.jpg', '2015-09-05 21:23:16', 'inactive'),
(9, 'Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla.', 'portfolio-11.jpg', '2015-09-05 21:23:24', 'active'),
(10, 'Beatuiful Bubble', 'portfolio-12.jpg', '2015-09-05 21:23:32', 'active');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pp_admin`
--
ALTER TABLE `pp_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pp_ad_codes`
--
ALTER TABLE `pp_ad_codes`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `pp_category`
--
ALTER TABLE `pp_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pp_cities`
--
ALTER TABLE `pp_cities`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `pp_cms`
--
ALTER TABLE `pp_cms`
  ADD PRIMARY KEY (`pageID`);

--
-- Indexes for table `pp_cms_previous`
--
ALTER TABLE `pp_cms_previous`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `pp_companies`
--
ALTER TABLE `pp_companies`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `pp_countries`
--
ALTER TABLE `pp_countries`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `pp_email_content`
--
ALTER TABLE `pp_email_content`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `pp_employers`
--
ALTER TABLE `pp_employers`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `pp_favourite_candidates`
--
ALTER TABLE `pp_favourite_candidates`
  ADD PRIMARY KEY (`employer_id`);

--
-- Indexes for table `pp_favourite_companies`
--
ALTER TABLE `pp_favourite_companies`
  ADD PRIMARY KEY (`seekerid`);

--
-- Indexes for table `pp_institute`
--
ALTER TABLE `pp_institute`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `pp_job_alert`
--
ALTER TABLE `pp_job_alert`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `pp_job_alert_queue`
--
ALTER TABLE `pp_job_alert_queue`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `pp_job_functional_areas`
--
ALTER TABLE `pp_job_functional_areas`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `pp_job_industries`
--
ALTER TABLE `pp_job_industries`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `pp_job_seekers`
--
ALTER TABLE `pp_job_seekers`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `pp_job_titles`
--
ALTER TABLE `pp_job_titles`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `pp_newsletter`
--
ALTER TABLE `pp_newsletter`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `pp_post_jobs`
--
ALTER TABLE `pp_post_jobs`
  ADD PRIMARY KEY (`ID`);
ALTER TABLE `pp_post_jobs` ADD FULLTEXT KEY `job_search` (`job_title`,`job_description`);

--
-- Indexes for table `pp_prohibited_keywords`
--
ALTER TABLE `pp_prohibited_keywords`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `pp_qualifications`
--
ALTER TABLE `pp_qualifications`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `pp_salaries`
--
ALTER TABLE `pp_salaries`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `pp_scam`
--
ALTER TABLE `pp_scam`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `pp_seeker_academic`
--
ALTER TABLE `pp_seeker_academic`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `pp_seeker_additional_info`
--
ALTER TABLE `pp_seeker_additional_info`
  ADD PRIMARY KEY (`ID`);
ALTER TABLE `pp_seeker_additional_info` ADD FULLTEXT KEY `resume_search` (`summary`,`keywords`);

--
-- Indexes for table `pp_seeker_applied_for_job`
--
ALTER TABLE `pp_seeker_applied_for_job`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `pp_seeker_experience`
--
ALTER TABLE `pp_seeker_experience`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `pp_seeker_ph`
--
ALTER TABLE `pp_seeker_ph`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `pp_seeker_resumes`
--
ALTER TABLE `pp_seeker_resumes`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `pp_seeker_skills`
--
ALTER TABLE `pp_seeker_skills`
  ADD PRIMARY KEY (`ID`);
ALTER TABLE `pp_seeker_skills` ADD FULLTEXT KEY `js_skill_search` (`skill_name`);

--
-- Indexes for table `pp_sessions`
--
ALTER TABLE `pp_sessions`
  ADD PRIMARY KEY (`session_id`),
  ADD KEY `last_activity_idx` (`last_activity`);

--
-- Indexes for table `pp_settings`
--
ALTER TABLE `pp_settings`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `pp_skills`
--
ALTER TABLE `pp_skills`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `pp_stories`
--
ALTER TABLE `pp_stories`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_gallery`
--
ALTER TABLE `tbl_gallery`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pp_admin`
--
ALTER TABLE `pp_admin`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `pp_ad_codes`
--
ALTER TABLE `pp_ad_codes`
  MODIFY `ID` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pp_category`
--
ALTER TABLE `pp_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `pp_cities`
--
ALTER TABLE `pp_cities`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `pp_cms`
--
ALTER TABLE `pp_cms`
  MODIFY `pageID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `pp_cms_previous`
--
ALTER TABLE `pp_cms_previous`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `pp_companies`
--
ALTER TABLE `pp_companies`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;

--
-- AUTO_INCREMENT for table `pp_countries`
--
ALTER TABLE `pp_countries`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `pp_email_content`
--
ALTER TABLE `pp_email_content`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `pp_employers`
--
ALTER TABLE `pp_employers`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;

--
-- AUTO_INCREMENT for table `pp_favourite_candidates`
--
ALTER TABLE `pp_favourite_candidates`
  MODIFY `employer_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pp_favourite_companies`
--
ALTER TABLE `pp_favourite_companies`
  MODIFY `seekerid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pp_institute`
--
ALTER TABLE `pp_institute`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pp_job_alert`
--
ALTER TABLE `pp_job_alert`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pp_job_alert_queue`
--
ALTER TABLE `pp_job_alert_queue`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pp_job_functional_areas`
--
ALTER TABLE `pp_job_functional_areas`
  MODIFY `ID` int(7) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pp_job_industries`
--
ALTER TABLE `pp_job_industries`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `pp_job_seekers`
--
ALTER TABLE `pp_job_seekers`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=245;

--
-- AUTO_INCREMENT for table `pp_job_titles`
--
ALTER TABLE `pp_job_titles`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `pp_newsletter`
--
ALTER TABLE `pp_newsletter`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pp_post_jobs`
--
ALTER TABLE `pp_post_jobs`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `pp_prohibited_keywords`
--
ALTER TABLE `pp_prohibited_keywords`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `pp_qualifications`
--
ALTER TABLE `pp_qualifications`
  MODIFY `ID` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `pp_salaries`
--
ALTER TABLE `pp_salaries`
  MODIFY `ID` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `pp_scam`
--
ALTER TABLE `pp_scam`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pp_seeker_academic`
--
ALTER TABLE `pp_seeker_academic`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `pp_seeker_additional_info`
--
ALTER TABLE `pp_seeker_additional_info`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=202;

--
-- AUTO_INCREMENT for table `pp_seeker_applied_for_job`
--
ALTER TABLE `pp_seeker_applied_for_job`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;

--
-- AUTO_INCREMENT for table `pp_seeker_experience`
--
ALTER TABLE `pp_seeker_experience`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `pp_seeker_ph`
--
ALTER TABLE `pp_seeker_ph`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `pp_seeker_resumes`
--
ALTER TABLE `pp_seeker_resumes`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=238;

--
-- AUTO_INCREMENT for table `pp_seeker_skills`
--
ALTER TABLE `pp_seeker_skills`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=502;

--
-- AUTO_INCREMENT for table `pp_settings`
--
ALTER TABLE `pp_settings`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pp_skills`
--
ALTER TABLE `pp_skills`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `pp_stories`
--
ALTER TABLE `pp_stories`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_gallery`
--
ALTER TABLE `tbl_gallery`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
